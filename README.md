# Arnold Pricebook Creator

A Clojure program designed to facilitate creating Arnold Group price
lists. The objective is to store the Arnold product line information data in
a database and allow that information to be output into custom templates.


## Usage

FIXME

## License

Copyright © 2014 Jeffrey Stoffers

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
