(ns database.import
  (:use korma.core
        [korma.db :only (defdb)])
  (:require [database.schema :as schema]
            [database.core :as db]
            [database.items :as items]
            [migratus.core :as migrate]
            [clojure.java.io :as io]))

(defdb db schema/db-spec)

(defn import-data
  [coll]
  (doseq [i coll]
    (println i)
    (doall (db/insert-product (merge {:owner "Arnold Desk"} i)))))

(defn data-import-script []
  
  ;;Import All the Items
  (import-data items/items)
  
  ;;Try to associate the images with the correct products
  (doseq [stnu (select db/style_view)]
    (let [location "resources/images/"]
      (when (.exists (io/as-file (str location (:number stnu) ".svg")))
        (update db/product
                (where {:id (:product_id stnu)})
                (set-fields {:image (slurp (str location 
                                                (:number stnu)
                                                ".svg"))})))))
  
  (exec-raw "REFRESH MATERIALIZED VIEW product_view")
  (exec-raw "REFRESH MATERIALIZED VIEW option_view"))

(defn nope
  []
  (exec-raw (str "DROP TABLE category, "
                 "item, "
                 "line, "
                 "option, "
                 "owner, "
                 "price, "
                 "product, "
                 "product_option, "
                 "schema_migrations, "
                 "size, "
                 "spec, "
                 "style, "
                 "variant, "
                 "variant_option, "
                 "context_templates, "
                 "users CASCADE;"))
  (migrate/migrate {:store :database
                    :migration-dir "migrations"
                    :db {:classname "com.postgresql.jdbc.Driver"
                         :subprotocol "postgresql"
                         :subname "//localhost/arnold_pricebook"
                         :user "postgres"
                         :password "keet2004"}}))

(defn export-image-categories
  []
  
  (.mkdir (java.io.File. "resources/export-images"))
  
  (doseq [ln (select db/line)]
    (.mkdir (java.io.File. (str "resources/export-images/" (:name ln))))
    (doseq [cg (select db/category)]
      (.mkdir (java.io.File. (str "resources/export-images/"
                                  (:name ln) "/" (:name cg))))))
  
  (doseq [im (select db/product (fields :line :image :category :name))]
    (spit (str "resources/export-images/"
               (db/id->name db/line (:line im))
               "/"
               (db/id->name db/category (:category im))
               "/"
               (.replaceAll (:name im) "[^0-9A-Za-z]" "")
               ".svg")
          (:image im))))
