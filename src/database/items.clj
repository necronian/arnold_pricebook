(ns database.items)

(def items [{:line "Solar"
             :category "Desk"
             :name "Table Desk"
             :description "Center pencil drawer"
             :item [{:number "D6045" :total_width 60 :total_depth 30 :kneespace 57 :price 1949 :weight 202}
                    {:number "D6042" :total_width 66 :total_depth 26 :kneespace 63 :price 1914 :weight 192}
                    {:number "D6043" :total_width 66 :total_depth 30 :kneespace 63 :price 1949 :weight 213}
                    {:number "D6049" :total_width 66 :total_depth 36 :kneespace 63 :price 1986 :weight 244}
                    {:number "D6039" :total_width 72 :total_depth 20 :kneespace 68.5 :price 1722 :weight 170}
                    {:number "D6046" :total_width 72 :total_depth 36 :kneespace 68.5 :price 2029 :weight 256}
                    {:number "D6041" :total_width 84 :total_depth 36 :kneespace 77.5 :price 2549 :weight 281}]
             :option[{:name "013"} {:name "033a"} {:name "034a"} {:name "D16"}]}
            
            {:name "Double Pedestal Desk"
             :line "Solar"
             :category "Desk"
             :description "3/4 pedestals with box/file drawers and pullout writing shelves. Personal storage shelf in kneespace."
             :item [{:number "D6302" :total_width 60 :total_depth 30 :kneespace 25 :price 3261 :weight 366}
                    {:number "D6300" :total_width 66 :total_depth 26 :kneespace 25 :price 3226 :weight 356}
                    {:number "D6002" :total_width 66 :total_depth 30 :kneespace 25 :price 3295 :weight 377}
                    {:number "D6001" :total_width 66 :total_depth 36 :kneespace 25 :price 3301 :weight 420}
                    {:number "D6000" :total_width 72 :total_depth 36 :kneespace 31 :price 3520 :weight 432}
                    {:number "D6003" :total_width 84 :total_depth 36 :kneespace 43 :price 3890 :weight 457}]
             :option[{:name "D15"} {:name "D16"} {:name "D64"} {:name "020"} {:name "013"} {:name "033a"} {:name "D14"}]}

            {:name "Single Pedestal Desk with Bullet End"
             :line "Solar"
             :category "Desk"
             :description "Full pedestal with box/box/file drawrers and pullout writing shelf. Specify Bullet End left or right."
             :item[{:number "D6703" :total_width 66 :total_depth 30 :kneespace 28 :price 4148 :weight 295}
                   {:number "D6701" :total_width 66 :total_depth 36 :kneespace 28 :price 4198 :weight 319}
                   {:number "D6702" :total_width 72 :total_depth 30 :kneespace 34 :price 4237 :weight 320}
                   {:number "D6700" :total_width 72 :total_depth 36 :kneespace 34 :price 4284 :weight 345}]
             :option[{:name "033a"} {:name "034a"} {:name "081"} {:name "D14"} {:name "D16"} {:name "020"} {:name "013"}]}

            {:name "Single Pedestal Desk Expanded Bullet End"
             :line "Solar"
             :category "Desk"
             :description "Full pedestal with box/box/file drawers and pullout writing shelf. Specify: Bullet End left or right"
             :item [{:number "D6719" :total_width 84 :total_depth 30 :kneespace 46 :price 6058 :weight 347}
                    {:number "D6720" :total_width 84 :total_depth 36 :kneespace 46 :price 6216 :weight 375}
                    {:number "D6721" :total_width 96 :total_depth 36 :kneespace 58 :price 6581 :weight 393}]
             :option[{:name "020"} {:name "013"} {:name "033a"} {:name "034a"} {:name "081"} {:name "D14"} {:name "D16"}]}

            {:line "Solar"
             :category "Desk"
             :name "Presidential Double Pedestal Desk"
             :description "3/4 pedestals with box/file drawers and pullout writing shelf. Center pencil drawer. Personal storage shelf in kneespace."
             :item [{:number "D6506" :total_width 76 :total_depth 36 :kneespace 28 :price 5614 :weight 442}
                    {:number "D6505" :total_width 84 :total_depth 36 :kneespace 30 :price 5637 :weight 457}]
             :option[{:name "013"} {:name "033a"} {:name "034a"} {:name "D14"} {:name "D16"} {:name "D62"}]}

                        {:line "Solar"
             :category "Desk"
             :name "Presidential Double Pedestal Desk"
             :description "Full pedestal with box/box/file drawrers and pullout writing shelf. Centerdrawer. Personal storage shelf in kneespace."
             :item [{:number "D6554" :total_width 76 :total_depth 36 :kneespace 28 :price 6369 :weight 483}
                    {:number "D6553" :total_width 84 :total_depth 36 :kneespace 30 :price 6389 :weight 498}]
             :option[{:name "013"} {:name "033a"} {:name "034a"} {:name "D14"} {:name "D16"}]}

                        {:line "Solar"
             :category "Desk"
             :name "Double Pedestal Desks Full pedestals"
             :description "with box/box/file drawers and pull-out writing shelves. Personal storage shelf in kneespace. Available in models shown below."
             :variant [{:name "Rectangular Top with Recessed Modesty Panel"
                        :item [{:number "D6357" :total_width 60 :total_depth 30 :kneespace 25 :price 3988 :weight 419}
                               {:number "D6355" :total_width 66 :total_depth 26 :kneespace 25 :price 3957 :weight 409}
                               {:number "D6057" :total_width 66 :total_depth 30 :kneespace 25 :price 4117 :weight 430}
                               {:number "D6056" :total_width 66 :total_depth 36 :kneespace 25 :price 4130 :weight 461}
                               {:number "D6055" :total_width 72 :total_depth 36 :kneespace 31 :price 4169 :weight 473}
                               {:number "D6058" :total_width 84 :total_depth 36 :kneespace 43 :price 4791 :weight 498}]}
                       {:name "Rectangular Top with Breakfront Modesty Panel"
                        :item [{:number "D6006" :total_width 60 :total_depth 30 :kneespace 25 :price 4459 :weight 429}
                               {:number "D6007" :total_width 66 :total_depth 30 :kneespace 25 :price 4586 :weight 440}
                               {:number "D6008" :total_width 72 :total_depth 30 :kneespace 31 :price 4595 :weight 473}
                               {:number "D6010" :total_width 72 :total_depth 36 :kneespace 31 :price 4638 :weight 483}
                               {:number "D6011" :total_width 84 :total_depth 36 :kneespace 43 :price 5261 :weight 508}]}
                       {:name "Bowed Top with Recessed Modesty Panel"
                        :item [{:number "D6012" :total_width 60 :total_depth 36 :total_height 30 :kneespace 25 :price 4865 :weight 429}
                               {:number "D6013" :total_width 66 :total_depth 36 :total_height 30 :kneespace 25 :price 4994 :weight 440}
                               {:number "D6014" :total_width 72 :total_depth 36 :total_height 30 :kneespace 31 :price 5030 :weight 473}
                               {:number "D6016" :total_width 72 :total_depth 42 :total_height 36 :kneespace 31 :price 5046 :weight 483}
                               {:number "D6017" :total_width 84 :total_depth 42 :total_height 36 :kneespace 43 :price 5669 :weight 508}]}
                       {:name "Bowed Top with Breakfront Modesty Panel"
                        :item [{:number "D6022" :total_width 60 :total_depth 36 :total_height 30 :kneespace 25 :price 5336 :weight 439}
                               {:number "D6023" :total_width 66 :total_depth 36 :total_height 30 :kneespace 25 :price 5466 :weight 450}
                               {:number "D6025" :total_width 72 :total_depth 36 :total_height 30 :kneespace 31 :price 5474 :weight 483}
                               {:number "D6026" :total_width 72 :total_depth 42 :total_height 36 :kneespace 31 :price 5517 :weight 493}
                               {:number "D6027" :total_width 84 :total_depth 42 :total_height 36 :kneespace 43 :price 6183 :weight 518}]}
                       {:name "Bowed Top with Recessed Bowed Modesty Panel"
                        :item [{:number "D6018" :total_width 60 :total_depth 36 :total_height 30 :kneespace 25 :price 5140 :weight 429}
                               {:number "D6019" :total_width 66 :total_depth 36 :total_height 30 :kneespace 25 :price 5270 :weight 440}
                               {:number "D6020" :total_width 72 :total_depth 36 :total_height 30 :kneespace 31 :price 5278 :weight 473}
                               {:number "D6021" :total_width 72 :total_depth 42 :total_height 36 :kneespace 31 :price 5321 :weight 483}
                               {:number "D6063" :total_width 84 :total_depth 42 :total_height 36 :kneespace 43 :price 5944 :weight 508}]}]
             :option[{:name "D14"} {:name "D15"} {:name "D16"} {:name "020"} {:name "013"} {:name "033a"}]}

            {:line "Solar"
             :category "Desk"
             :name "Single Pedestal Desks Full Pedestal"
             :description "with box/box/file drawers and pull-out writing shelf. Personal storage shelf in knee-space. Available in models shown below"
             :variant [{:name "Rectangular Top with Recessed Modesty Panel"
                        :description "For use alone, or with returns on page 9"
                        :item [{:number "D6173" :total_width 60 :total_depth 30 :kneespace 41.25 :price 3113 :weight 321}
                               {:number "D6171" :total_width 66 :total_depth 26 :kneespace 44.25 :price 3078 :weight 302}
                               {:number "D6174" :total_width 66 :total_depth 30 :kneespace 44.25 :price 3183 :weight 332}
                               {:number "D6176" :total_width 66 :total_depth 36 :kneespace 44.25 :price 3208 :weight 349}
                               {:number "D6177" :total_width 72 :total_depth 36 :kneespace 50.25 :price 3273 :weight 362}
                               {:number "D6178" :total_width 84 :total_depth 36 :kneespace 62.25 :price 3658 :weight 387}]}
                       {:name "Rectangular Top with Breakfront Modesty Panel"
                        :description "For use alone, or with returns on page 9"
                        :item [{:number "D6028" :total_width 60 :total_depth 30 :kneespace 41.25 :price 3583 :weight 331}
                               {:number "D6029" :total_width 66 :total_depth 30 :kneespace 44.25 :price 3654 :weight 342}
                               {:number "D6033" :total_width 72 :total_depth 30 :kneespace 50.25 :price 3711 :weight 362}
                               {:number "D6034" :total_width 72 :total_depth 36 :kneespace 50.25 :price 3744 :weight 372}
                               {:number "D6035" :total_width 84 :total_depth 36 :kneespace 62.25 :price 4129 :weight 397}]}
                       {:name "Bowed Top with Recessed Modesty Panel"
                        :description "For use alone, or with returns on page 9"
                        :item [{:number "D6036" :total_width 60 :total_depth 36 :total_height 30 :kneespace 41.25 :price 3991 :weight 331}
                               {:number "D6037" :total_width 66 :total_depth 36 :total_height 30 :kneespace 44.25 :price 4063 :weight 342}
                               {:number "D6038" :total_width 72 :total_depth 36 :total_height 30 :kneespace 50.25 :price 4119 :weight 362}
                               {:number "D6040" :total_width 72 :total_depth 42 :total_height 36 :kneespace 50.25 :price 4152 :weight 372}
                               {:number "D6044" :total_width 84 :total_depth 42 :total_height 36 :kneespace 62.25 :price 4629 :weight 397}]}
                       {:name "Bowed Top with Breakfront Modesty Panel"
                        :description "For use alone, or with returns on page 9"
                        :item [{:number "D6053" :total_width 60 :total_depth 36 :total_height 30 :kneespace 41.25 :price 4462 :weight 341}
                               {:number "D6059" :total_width 66 :total_depth 36 :total_height 30 :kneespace 44.25 :price 4532 :weight 352}
                               {:number "D6060" :total_width 72 :total_depth 36 :total_height 30 :kneespace 50.25 :price 4591 :weight 372}
                               {:number "D6061" :total_width 72 :total_depth 42 :total_height 36 :kneespace 50.25 :price 4623 :weight 382}
                               {:number "D6062" :total_width 84 :total_depth 42 :total_height 36 :kneespace 62.25 :price 5006 :weight 407}]}
                       {:name "Bowed Top with Recessed Bowed Modesty Panel"
                        :description "For use alone, or with returns on page 9"
                        :item [{:number "D6047" :total_width 60 :total_depth 36 :total_height 30 :kneespace 41.25 :price 4252 :weight 331}
                               {:number "D6048" :total_width 66 :total_depth 36 :total_height 30 :kneespace 44.25 :price 4336 :weight 342}
                               {:number "D6050" :total_width 72 :total_depth 36 :total_height 30 :kneespace 50.25 :price 4395 :weight 362}
                               {:number "D6051" :total_width 72 :total_depth 42 :total_height 36 :kneespace 50.25 :price 4427 :weight 372}
                               {:number "D6052" :total_width 84 :total_depth 42 :total_height 36 :kneespace 62.25 :price 4810 :weight 397}]}]
             :option[{:name "020"} {:name "013"} {:name "033a"} {:name "D14"} {:name "D15"} {:name "D16"}]}

            {:line "Solar"
             :category "Desk"
             :name "Return Desks"
             :description "Select one desk from below or page 249, and one return from the facing page. Specify: left or right return"
             :variant [{:name "Presidential Single Pedestal Desk"
                        :description "Full pedestal with box/box/file drawers and pullout writing shelf. Center drawer. Personal storage shelf."
                        :item [{:number "D6179" :total_width 76 :total_depth 36 :kneespace 59 :price 5586 :weight 387}]
                        :option[{:name "013"} {:name "033a"} {:name "D14"} {:name "D16"}]}
                       {:name "Single Pedestal Desk"
                        :description "3/4 pedestal with box/file drawers and pullout writing shelf. Personal storage shelf in kneespace."
                        :item [{:number "D6166" :total_width 60 :total_depth 30 :kneespace 41.25 :price 2574 :weight 298}
                               {:number "D6165" :total_width 66 :total_depth 26 :kneespace 44.25 :price 2538 :weight 277}
                               {:number "D6167" :total_width 66 :total_depth 30 :kneespace 44.25 :price 2650 :weight 302}
                               {:number "D6168" :total_width 66 :total_depth 36 :kneespace 44.25 :price 2692 :weight 314}
                               {:number "D6169" :total_width 72 :total_depth 36 :kneespace 50.25 :price 2782 :weight 325}
                               {:number "D6170" :total_width 84 :total_depth 36 :kneespace 62.25 :price 3315 :weight 336}]
                        :option[{:name "020"} {:name "013"} {:name "033a"} {:name "D14"} {:name "D15"} {:name "D16"} {:name "D64"}]}
                       {:name "Bullet End Desk"
                        :item [{:number "D6180" :total_width 66 :total_depth 30 :kneespace 47 :price 2729 :weight 165}
                               {:number "D6181" :total_width 66 :total_depth 36 :kneespace 47 :price 2771 :weight 196}
                               {:number "D6182" :total_width 72 :total_depth 30 :kneespace 53 :price 2818 :weight 183}
                               {:number "D6183" :total_width 72 :total_depth 36 :kneespace 53 :price 2857 :weight 208}]
                        :option[{:name "020"} {:name "013"} {:name "033a"} {:name "081"} {:name "D16"}]}
                       {:name "Expanded Bullet End Desk"
                        :item [{:number "D6184" :total_width 84 :total_depth 30 :kneespace 65 :price 3089 :weight 206}
                               {:number "D6185" :total_width 84 :total_depth 36 :kneespace 65 :price 3183 :weight 237}
                               {:number "D6188" :total_width 96 :total_depth 36 :kneespace 77 :price 3406 :weight 261}]
                        :option[{:name "020"} {:name "013"} {:name "033a"} {:name "D16"}]}]}

            {:line "Solar"
             :category "Return"
             :name "Desk Returns"
             :description "Select one desk from page 249 or 250, and one return from below. Specify: left or right return"
             :variant [{:name "Secretarial Return"
                        :description "3/4 pedestal with stationery cabinet."
                        :item [{:number "D6189" :total_width 42 :total_depth 20 :kneespace 28 :price 1523 :weight 146}
                               {:number "D6190" :total_width 42 :total_depth 24 :kneespace 28 :price 1858 :weight 165}
                               {:number "D6191" :total_width 48 :total_depth 20 :kneespace 34.5 :price 1675 :weight 158}
                               {:number "D6192" :total_width 48 :total_depth 24 :kneespace 34.5 :price 2008 :weight 178}]}
                       {:name "Executive or Secretarial Return"
                        :description "3/4 pedestal with box/file drawers. Specify: Executive or secretarial height"
                        :item [{:number "D6193" :total_width 42 :total_depth 20 :kneespace 21.75 :price 1654 :weight 188}
                               {:number "D6194" :total_width 42 :total_depth 24 :kneespace 21.75 :price 2008 :weight 205}
                               {:number "D6195" :total_width 48 :total_depth 20 :kneespace 27.75 :price 1824 :weight 200}
                               {:number "D6196" :total_width 48 :total_depth 24 :kneespace 27.75 :price 2182 :weight 219}
                               {:number "D6197" :total_width 60 :total_depth 20 :kneespace 39.75 :price 1918 :weight 224}
                               {:number "D6198" :total_width 60 :total_depth 24 :kneespace 39.75 :price 2267 :weight 245}]}
                       {:name "Secretarial Return"
                        :description "Full pedestal with box/box/file drawers."
                        :item [{:number "D6199" :total_width 42 :total_depth 20 :kneespace 21.75 :price 1978 :weight 201}
                               {:number "D6201" :total_width 42 :total_depth 24 :kneespace 21.75 :price 2311 :weight 220}
                               {:number "D6203" :total_width 48 :total_depth 20 :kneespace 27.75 :price 2129 :weight 213}
                               {:number "D6204" :total_width 48 :total_depth 24 :kneespace 27.75 :price 2462 :weight 233}
                               {:number "D6206" :total_width 60 :total_depth 20 :kneespace 39.75 :price 2164 :weight 237}
                               {:number "D6208" :total_width 60 :total_depth 24 :kneespace 39.75 :price 2497 :weight 259}]}
                       {:name "Executive Return"
                        :description "Full pedestal with box/box/file drawers"
                        :item [{:number "D6209" :total_width 42 :total_depth 20 :kneespace 21.75 :price 1978 :weight 206}
                               {:number "D6211" :total_width 42 :total_depth 24 :kneespace 21.75 :price 2311 :weight 226}
                               {:number "D6213" :total_width 48 :total_depth 20 :kneespace 27.75 :price 2129 :weight 219}
                               {:number "D6214" :total_width 48 :total_depth 24 :kneespace 27.75 :price 2462 :weight 239}
                               {:number "D6216" :total_width 60 :total_depth 20 :kneespace 39.75 :price 2164 :weight 243}
                               {:number "D6218" :total_width 60 :total_depth 24 :kneespace 39.75 :price 2497 :weight 265}]}]
             :option[{:name "039"} {:name "033a"} {:name "034a"} {:name "030"} {:name "032a"} {:name "031"} {:name "D16"} {:name "026"}]}

            {:line "Solar"
             :category "Return"
             :name "Bullet End Return"
             :item [{:number "D6732" :total_width 72 :total_depth 36 :kneespace 53 :price 2995 :weight 155}
                    {:number "D6731" :total_width 84 :total_depth 36 :kneespace 65 :price 3408 :weight 171}
                    {:number "D6730" :total_width 84 :total_depth 40 :kneespace 65 :price 3750 :weight 185}]
             :option[{:name "020"} {:name "013"} {:name "D16"}]}

            {:line "Solar"
             :category "Desk"
             :name "Computer Desk"
             :description "2 full box/box/file pedestals, CRT keyboard   drawer and grommet."
             :style [{:number "D6101"
                      :price 5504
                      :weight 492
                      :item [{:part "Return" :total_width 48 :total_depth 20}
                             {:part "Corner" :total_width 41 :total_depth 41}
                             {:part "Return" :total_width 48 :total_depth 20}]}
                     {:number "D6100"
                      :price 5823
                      :weight 526
                      :item [{:part "Return" :total_width 60 :total_depth 20}
                             {:part "Corner" :total_width 41 :total_depth 41}
                             {:part "Return" :total_width 60 :total_depth 20}]}]
             :option[{:name "039"} {:name "034a"} {:name "036"} {:name "D16"}]}

            {:line "Solar"
             :category "Desk"
             :name "Computer Desk"
             :description "4 full pedestals w/6 file & 4 box drawers. CRT keyb'd. drawer, grommet."
             :style [{:number "D6104"
                      :price 7665
                      :weight 540
                      :item [{:part "Return" :total_width 48 :total_depth 20}
                             {:part "Corner" :total_width 41 :total_depth 41}
                             {:part "Return" :total_width 48 :total_depth 20}]}
                     {:number "D6102"
                      :price 7985
                      :weight 574
                      :item [{:part "Return" :total_width 60 :total_depth 20}
                             {:part "Corner" :total_width 41 :total_depth 41}
                             {:part "Return" :total_width 60 :total_depth 20}]}]
             :option[{:name "039"} {:name "034a"} {:name "036"} {:name "D16"}]}

            {:line "Solar"
             :category "Workstation"
             :name "Executive U Workstation"
             :description "Box/box/file pedestal in  desk; two box/box/file pedestals in credenza."
             :style [{:number "D6422"
                      :price 7222
                      :weight 712
                      :item [{:part "Desk" :total_width 66 :total_depth 30}
                             {:part "Bridge" :total_width 48 :total_depth 20}
                             {:part "Credenza" :total_width 66 :total_depth 20}]}
                     {:number "D6421"
                      :price 7488 
                      :weight 747
                      :item [{:part "Desk" :total_width 72 :total_depth 36}
                             {:part "Bridge" :total_width 48 :total_depth 20}
                             {:part "Credenza" :total_width 72 :total_depth 20}]}
                     {:number "D6420"
                      :price 8296
                      :weight 808
                      :item [{:part "Desk" :total_width 84 :total_depth 36}
                             {:part "Bridge" :total_width 60 :total_depth 20}
                             {:part "Credenza" :total_width 81 :total_depth 20}]}]
             :option[{:name "020"} {:name "013"} {:name "D65"} {:name "012"} {:name "039"} {:name "033a"} {:name "030"} {:name "032a"} {:name "031"} {:name "D14"} {:name "D15"} {:name "D16"} {:name "D33"} {:name "026"}]}

            {:line "Solar"
             :category "Workstation"
             :name "Executive U Computer Workstation"
             :description "Box/file pedestal in desk  B/B/F ped. in credenza.  CRT keyboard drawer."
             :style [{:number "D6571"
                      :price 7934
                      :weight 662
                      :item [{:part "Desk" :total_width 72 :total_depth 36}
                             {:part "Bridge" :total_width 48 :total_depth 20}
                             {:part "Credenza" :total_width 72 :total_depth 20 :bow 36}]}
                     {:number "D6572"
                      :price 8199
                      :weight 702
                      :item [{:part "Desk" :total_width 84 :total_depth 36}
                             {:part "Bridge" :total_width 60 :total_depth 20}
                             {:part "Credenza" :total_width 81 :total_depth 20 :bow 36}]}]
             :option[{:name "020"} {:name "013"} {:name "D65"} {:name "012"} {:name "039"} {:name "033a"} {:name "030"} {:name "032a"} {:name "031"} {:name "D14"} {:name "D15"} {:name "D16"} {:name "D33"} {:name "026"}]}

            {:line "Solar"
             :category "Workstation"
             :name "Bullet End U Workstation"
             :description "with two box/box/file pedestals."
             :style [{:number "D6762" :price 7636 :weight 535
                      :item [{:part "Desk" :total_width 66 :total_depth 30} 
                             {:part "Bridge" :total_width 48 :total_depth 20}
                             {:part "Credenza" :total_width 66 :total_depth 20}]}
                     {:number "D6761" :price 7897 :weight 593
                      :item [{:part "Desk" :total_width 72 :total_depth 36} 
                             {:part "Bridge" :total_width 48 :total_depth 20}
                             {:part "Credenza" :total_width 72 :total_depth 20}]}
                     {:number "D6760" :price 8138 :weight 646
                      :item [{:part "Desk" :total_width 84 :total_depth 36} 
                             {:part "Bridge" :total_width 60 :total_depth 20}
                             {:part "Credenza" :total_width 81 :total_depth 20}]}]
             :option[{:name "020"} {:name "013"} {:name "D65"} {:name "010"} {:name "039"} {:name "033a"} {:name "030"} {:name "031"} {:name "032a"} {:name "081"} {:name "012"} {:name "D16"} {:name "026"}]}

            {:line "Solar"
             :category "Workstation"
             :name "Bullet End Computer U Workstation"
             :description "2 cabinet doors, 2 box drawers, 1 file. Keyboard drawer, grommet, paper slot."
             :style [{:number "D6774" :price 8881 :weight 672
                      :item [{:part "Desk" :total_width 84 :total_depth 36}
                             {:part "Bridge" :total_width 36 :total_depth 20}
                             {:part "Corner" :total_width 41 :total_depth 41}
                             {:part "Credenza" :total_width 51 :total_depth 20}]}]
             :option[{:name "020"} {:name "013"} {:name "D65"} {:name "010"} {:name "039"} {:name "033a"} {:name "030"} {:name "031"} {:name "032a"} {:name "081"} {:name "012"} {:name "D16"} {:name "026"}]}
            {:line "Solar"
             :category "Workstation"
             :name "Bullet End Computer U Workstation"
             :description "2 cabinet doors, 4 box drawers, 2 files. Grommet and keyboard drawer."
             :style [{:number "D6775" :price 9325 :weight 743
                      :item [{:part "Desk" :total_width 84 :total_depth 36}
                             {:part "Bridge" :total_width 36 :total_depth 20}
                             {:part "Corner" :total_width 41 :total_depth 41}
                             {:part "Credenza" :total_width 70 :total_depth 20}]}]
             :option[{:name "020"} {:name "013"} {:name "D65"} {:name "010"} {:name "039"} {:name "033a"} {:name "030"} {:name "031"} {:name "032a"} {:name "081"} {:name "012"} {:name "D16"} {:name "026"}]}

            {:line "Solar"
             :category "Workstation"
             :name "Executive U Workstation  with Curved Top Desk"
             :description "2 box/box/file pedestals and center drawer in desk. 2 box/box/file pedestals in credenza."
             :style [{:number "D6550" :price 10111 :weight 790
                      :item [{:part "Desk" :total_width 76 :total_depth 36}
                             {:part "Bridge" :total_width 48 :total_depth 20}
                             {:part "Credenza" :total_width 76 :total_depth 20}]}]
             :option[{:name "013"} {:name "039"} {:name "033a"} {:name "030"} {:name "032a"} {:name "012"} {:name "D14"} {:name "D16"}]}

            {:line "Solar"
             :category "Table"
             :name "Computer Table"
             :description "with pull-out keyboard drawer and grommet."
             :item [{:number "D6130" :total_width 41 :total_depth 20 :price 1641 :weight 130}]
             :option[{:name "034a"} {:name "039"} {:name "D16"}]}

            {:line "Solar"
             :category "Table"
             :name "Computer Table"
             :description "with split level top. Grommet."
             :item [{:number "D6140" :total_width 41 :total_depth 30 :price 2024 :weight 170}]
             :option[{:name "034a"} {:name "039"} {:name "D16"}]}

            {:line "Solar"
             :category "Table"
             :name "Mobile Printer Stand"
             :description "with casters and paper feed grommet."
             :item [{:number "D6150" :total_width 41 :total_depth 30 :price 1850 :weight 166}]
             :option[{:name "033a"} {:name "034a"} {:name "039"} {:name "D16"}]}

            {:line "Solar"
             :category "Table"
             :name "Mobile Typing Table"
             :description "with casters."
             :item [{:number "D6090" :total_width 41 :total_depth 20 :price 1141 :weight 140}
                    {:number "D6998" :total_width 41 :total_depth 24 :price 1385 :weight 152}]
             :option[{:name "033a"} {:name "034a"} {:name "D16"}]}

            {:line "Solar"
             :category "Part"
             :name "Add-On Pedestals"
             :description "3/4 height. 19.25\" high. F - Box/File G - Box/box/box H - Hinged Door Cabinet with adjust. shelf"
             :item [{:number "D6990" :total_width 18.75 :total_depth 17.25 :price 841 :weight 56}
                    {:number "D6991" :total_width 18.75 :total_depth 17.25 :price 923 :weight 62}
                    {:number "D6992" :total_width 18.75 :total_depth 17.25 :price 665 :weight 38}]
             :option[{:name "012"} {:name "011"}]}

            {:line "Solar"
             :category "Part"
             :name "Add-On Pedestals"
             :description "Full height. 27.5\" high. A - Box/box/file B - Two file drawers D - Four box drawers E - Hinged Door Cabinet with adjust. shelf"
             :item [{:number "D6993" :total_width 18.75 :total_depth 17.25 :price 1086 :weight 73}
                    {:number "D6994" :total_width 18.75 :total_depth 17.25 :price 1007 :weight 66}
                    {:number "D6995" :total_width 18.75 :total_depth 17.25 :price 1173 :weight 78}
                    {:number "D6996" :total_width 18.75 :total_depth 17.25 :price 804 :weight 47}]
             :option[{:name "012"} {:name "011"}]}

            {:line "Solar"
             :category "Part"
             :name "Add-On Pedestals"
             :description "Lateral file drawers. 27.5\" high."
             :item [{:number "D6997" :total_width 33.75 :total_depth 17.25 :price 1464 :weight 84}]
             :option[{:name "012"} {:name "022"}]}

            {:line "Solar"
             :category "Credenza"
             :name "Cabinet Credenza"
             :description "2 doors; full height."
             :item [{:number "D6117" :total_width 30 :total_depth 20 :price 1957 :weight 162}
                    {:number "D6118" :total_width 36 :total_depth 20 :price 2022 :weight 178}
                    {:number "D6119" :total_width 42 :total_depth 20 :price 2506 :weight 194}
                    {:number "D6123" :total_width 48 :total_depth 20 :price 2776 :weight 210}]
             :option[{:name "010"} {:name "D16"}]}

            {:line "Solar"
             :category "Credenza"
             :name "Cabinet Credenza"
             :description "4 doors; full height."
             :item [{:number "D6431" :total_width 62 :total_depth 20 :price 3124 :weight 247}
                    {:number "D6432" :total_width 72 :total_depth 20 :price 3257 :weight 273}]
             :option[{:name "010"} {:name "D16"}]}

            {:line "Solar"
             :category "File"
             :name "File"
             :description "2 file drawers; full height."
             :item [{:number "D6004" :total_width 21.75 :total_depth 20 :price 1633 :weight 151}
                    {:number "D6009" :total_width 21.75 :total_depth 26 :price 1994 :weight 177}]
             :option[{:name "012"} {:name "D16"}]}

            {:line "Solar"
             :category "File"
             :name "File"
             :description "4 file drawers; full height."
             :item [{:number "D6287" :total_width 41.5 :total_depth 20 :price 2903 :weight 255}]
             :option[{:name "012"} {:name "D16"}]}

            {:line "Solar"
             :category "Credenza"
             :name "File Credenza"
             :description "2 box/5 file drawers. Full height."
             :item [{:number "D6086" :total_width 61 :total_depth 20 :price 3784 :weight 325}]
             :option[{:name "012"} {:name "D16"}]}

            {:line "Solar"
             :category "Credenza"
             :name "File Credenza"
             :description "8 box/4 file drawers. Full height"
             :item [{:number "D6580" :total_width 81 :total_depth 20 :price 4908 :weight 405}]
             :option[{:name "012"} {:name "D16"}]}

            {:line "Solar"
             :category "Credenza"
             :name "Combination Credenza"
             :description "2 doors; 4 box/ 2 file drawers. Full height. Note: 60\" unit has only one door."
             :item [{:number "D6115" :total_width 60 :total_depth 20 :price 3898 :weight 309}
                    {:number "D6114" :total_width 66 :total_depth 20 :price 3958 :weight 317}
                    {:number "D6085" :total_width 72 :total_depth 20 :price 4018 :weight 326}]
             :option[{:name "012"} {:name "010"} {:name "D16"}]}

            {:line "Solar"
             :category "File"
             :name "Lateral File"
             :description "2 drawers 29\" high."
             :item [{:number "D60822" :total_width 37 :total_depth 20 :price 2099 :weight 214}]
             :option[{:name "012"} {:name "022"} {:name "D16"}]}

            {:line "Solar"
             :category "File"
             :name "Lateral File"
             :description "3 drawers. 41\" high."
             :item [{:number "D6084" :total_width 37 :total_depth 20 :price 2904 :weight 255}]
             :option[{:name "012"} {:name "022"} {:name "D16"}]}

            {:line "Solar"
             :category "File"
             :name "Lateral File"
             :description "4 drawers. 53.5\" high."
             :item [{:number "D6054" :total_width 37 :total_depth 20 :price 4210 :weight 321}]
             :option[{:name "012"} {:name "022"} {:name "D16"}]}

            {:line "Solar"
             :category "File"
             :name "Lateral File"
             :description "4 drawers. 29\" high."
             :item [{:number "D60824" :total_width 72 :total_depth 20 :price 3319 :weight 329}]
             :option[{:name "012"} {:name "022"} {:name "D16"}]}

            {:line "Solar"
             :category "File"
             :name "Lateral File"
             :description "6 drawers. 53.5\" high."
             :item [{:number "D6186" :total_width 72 :total_depth 20 :price 5347 :weight 394}]
             :option[{:name "012"} {:name "022"} {:name "D16"}]}

            {:line "Solar"
             :category "Credenza"
             :name "Refrigerator Credenza"
             :description "2 cubic foot capacity refrigerator."
             :item [{:number "D6221" :total_width 39 :total_depth 20 :price 4657 :weight 235}]
             :option[{:name "D16"}]}

            {:line "Solar"
             :category "Credenza"
             :name "Writing Credenza"
             :description "3/4 pedestals with box/file drawers"
             :item [{:number "D603261" :total_width 61 :total_depth 20 :kneespace 26.5 :price 2484 :weight 238}
                    {:number "D603272" :total_width 72 :total_depth 20 :kneespace 30.75 :price 2564 :weight 252}
                    {:number "D603281" :total_width 81 :total_depth 20 :kneespace 40.5 :price 2926 :weight 263}]
             :option[{:name "020"} {:name "013"} {:name "012"} {:name "030"} {:name "032a"} {:name "039"} {:name "033a"} {:name "034a"} {:name "D16"} {:name "026"}]}

            {:line "Solar"
             :category "Credenza"
             :name "Writing Credenza"
             :description "Full pedestasl with box/box/file drawers."
             :item [{:number "D608361" :total_width 61 :total_depth 20 :kneespace 26.5 :price 3246 :weight 270}
                    {:number "D608372" :total_width 72 :total_depth 20 :kneespace 30.75 :price 3327 :weight 285}
                    {:number "D608381" :total_width 81 :total_depth 20 :kneespace 40.5 :price 3683 :weight 298}]}

            {:line "Solar"
             :category "Credenza"
             :name "Writing Credenza"
             :description "Full pedestal with two lateral file drawrers. CRT keyboard drawer and grommet. Specify left or right pedestal."
             :item [{:number "D6389" :total_width 72 :total_depth 20 :kneespace 34.5 :price 3000 :weight 275}
                    {:number "D6371" :total_width 81 :total_depth 20 :kneespace 42 :price 3341 :weight 288}]
             :option[{:name "012"} {:name "039"} {:name "022"} {:name "D16"}]}

            {:line "Solar"
             :category "Credenza"
             :name "Writing Credenza"
             :description "Full pedestals with four lateral file drawers."
             :item [{:number "D6482" :total_width 96 :total_depth 20 :kneespace 25.5 :price 4503 :weight 389}
                    {:number "D6483" :total_width 108 :total_depth 20 :kneespace 37.5 :price 4728 :weight 406}]
             :option[{:name "020"} {:name "013"} {:name "012"} {:name "030"} {:name "032a"} {:name "039"} {:name "033a"} {:name "022"}]}

            {:line "Solar"
             :category "Cabinet"
             :name "Wardrobe"
             :description "19\" deep has pull-out hanger; 26\" has side-to-side"
             :item [{:number "D6596" :total_width 36 :total_depth 19 :total_height 66 :price 2685 :weight 238}
                    {:number "D6595" :total_width 36 :total_depth 26 :total_height 66 :shelf 4 :price 2954 :weight 268}
                    {:number "D6597" :total_width 36 :total_depth 19 :total_height 72 :price 2940 :weight 248}
                    {:number "D6598" :total_width 36 :total_depth 26 :total_height 72 :shelf 5 :price 3234 :weight 251}]
             :option[{:name "010"}]}

            {:line "Solar"
             :category "Bookcase"
             :name "Bookcase with Lateral Files"
             :description "Single width. 72\" high."
             :item [{:number "D6584" :total_width 37 :total_depth 19 :shelf 2 :price 3838 :weight 328}]
             :option[{:name "012"} {:name "D20"} {:name "D63"}]}

            {:line "Solar"
             :category "Bookcase"
             :name "Bookcase with Lateral Files"
             :description "Double width. 72\" high."
             :item [{:number "D6587" :total_width 72 :total_depth 19 :shelf 4 :price 5805 :weight 503}]
             :option[{:name "012"} {:name "D20"} {:name "D63"}]}

            {:line "Solar"
             :category "Bookcase"
             :name "Bookcase with Drawers"
             :description "3 file / 2 box drawers. 72\" high."
             :item [{:number "D6582" :total_width 41.5 :total_depth 19 :shelf 4 :price 4503 :weight 376}]
             :option[{:name "012"} {:name "D20"} {:name "D63"}]}

            {:line "Solar"
             :category "Bookcase"
             :name "Bookcase with Doors"
             :description "Double width. 72\" high."
             :item [{:number "D6095" :total_width 48 :total_depth 13 :shelf 6 :price 2739 :weight 240}]
             :option[{:name "010"} {:name "D60"} {:name "D22"} {:name "D23"}]}

            {:line "Solar"
             :category "Bookcase"
             :name "Bookcase"
             :description "for credenza tops 42\" high. See page 255 for credenzas"
             :item [{:number "D6137" :total_width 44.75 :total_depth 13 :shelf 2 :price 1653 :weight 120}
                    {:number "D6131" :total_width 57.75 :total_depth 13 :shelf 2 :price 2043 :weight 131}
                    {:number "D6134" :total_width 68.75 :total_depth 13 :shelf 2 :price 2345 :weight 140}
                    {:number "D6136" :total_width 77.75 :total_depth 13 :shelf 2 :price 2838 :weight 147}]
             :option[{:name "D23"}]}

            {:line "Solar"
             :category "Bookcase"
             :name "Open Bookcase"
             :description "Single width 13\" deep."
             :item [{:number "D6094" :total_width 30 :total_depth 30 :shelf 1 :price 1007 :weight 84}
                    {:number "D6142" :total_width 36 :total_depth 30 :shelf 1 :price 1096 :weight 90}
                    {:number "D6143" :total_width 42 :total_depth 30 :shelf 2 :price 1234 :weight 104}
                    {:number "D6144" :total_width 48 :total_depth 30 :shelf 2 :price 1383 :weight 110}
                    {:number "D6146" :total_width 60 :total_depth 30 :shelf 3 :price 1749 :weight 131}
                    {:number "D6147" :total_width 66 :total_depth 30 :shelf 3 :price 1872 :weight 137}
                    {:number "D6148" :total_width 72 :total_depth 30 :shelf 4 :price 1973 :weight 151}
                    {:number "D6149" :total_width 78 :total_depth 30 :shelf 4 :price 2112 :weight 157}
                    {:number "D6151" :total_width 84 :total_depth 30 :shelf 5 :price 2165 :weight 171}
                    {:number "D6098" :total_width 30 :total_depth 36 :shelf 1 :price 1125 :weight 90}
                    {:number "D6093" :total_width 36 :total_depth 36 :shelf 1 :price 1184 :weight 97}
                    {:number "D6152" :total_width 42 :total_depth 36 :shelf 2 :price 1331 :weight 113}
                    {:number "D6153" :total_width 48 :total_depth 36 :shelf 2 :price 1410 :weight 120}
                    {:number "D6154" :total_width 60 :total_depth 36 :shelf 3 :price 1758 :weight 142}
                    {:number "D6156" :total_width 66 :total_depth 36 :shelf 3 :price 1893 :weight 149}
                    {:number "D6157" :total_width 72 :total_depth 36 :shelf 4 :price 2014 :weight 165}
                    {:number "D6158" :total_width 78 :total_depth 36 :shelf 4 :price 2206 :weight 172}
                    {:number "D6159" :total_width 84 :total_depth 36 :shelf 5 :price 2347 :weight 188}]
             :option[{:name "D23"} {:name "060"}]}

            {:line "Solar"
             :category "Bookcase"
             :name "Open Bookcase"
             :description "Double width 13\" deep."
             :item [{:number "D6219" :total_width 30 :total_depth 48 :shelf 2 :price 1266 :weight 104}
                    {:number "D6223" :total_width 36 :total_depth 48 :shelf 2 :price 1357 :weight 112}
                    {:number "D6224" :total_width 42 :total_depth 48 :shelf 4 :price 1599 :weight 131}
                    {:number "D6226" :total_width 48 :total_depth 48 :shelf 4 :price 1730 :weight 139}
                    {:number "D6228" :total_width 60 :total_depth 48 :shelf 6 :price 2018 :weight 166}
                    {:number "D6229" :total_width 66 :total_depth 48 :shelf 6 :price 2090 :weight 174}
                    {:number "D6231" :total_width 72 :total_depth 48 :shelf 8 :price 2186 :weight 193}
                    {:number "D6233" :total_width 78 :total_depth 48 :shelf 8 :price 2306 :weight 201}
                    {:number "D6234" :total_width 84 :total_depth 48 :shelf 10 :price 2429 :weight 220}
                    {:number "D6236" :total_width 30 :total_depth 60 :shelf 2 :price 1582 :weight 119}
                    {:number "D6238" :total_width 36 :total_depth 60 :shelf 2 :price 1784 :weight 127}
                    {:number "D6239" :total_width 42 :total_depth 60 :shelf 4 :price 1983 :weight 146}
                    {:number "D6241" :total_width 48 :total_depth 60 :shelf 4 :price 2180 :weight 154}
                    {:number "D6243" :total_width 60 :total_depth 60 :shelf 6 :price 2550 :weight 173}
                    {:number "D6244" :total_width 66 :total_depth 60 :shelf 6 :price 2580 :weight 181}
                    {:number "D6246" :total_width 72 :total_depth 60 :shelf 8 :price 2624 :weight 200}
                    {:number "D6248" :total_width 78 :total_depth 60 :shelf 8 :price 2864 :weight 208}
                    {:number "D6249" :total_width 84 :total_depth 60 :shelf 10 :price 2995 :weight 227}]
             :option[{:name "D23"} {:name "060"}]}

            {:line "Solar"
             :category "Cabinet"
             :name "Flipper Door Cabinet"
             :description "15\" deep; 18\" high."
             :item [{:number "D6601" :total_width 36 :door 1 :price 1236 :weight 88}
                    {:number "D6603" :total_width 42 :door 1 :price 1482 :weight 96}
                    {:number "D6605" :total_width 48 :door 2 :price 1722 :weight 104}
                    {:number "D6607" :total_width 60 :door 2 :price 1886 :weight 120}
                    {:number "D6609" :total_width 72 :door 2 :price 2050 :weight 128}
                    {:number "D6611" :total_width 81 :door 2 :price 2245 :weight 140}]
             :option[{:name "070"} {:name "011"}]}

            {:line "Solar"
             :category "Hutch"
             :name "Flipper Door Hutch"
             :description "for credenzas. 15\" deep; 36\" high. See page 255 for credenzas."
             :item [{:number "D6250" :total_width 32.75 :door 1 :price 1599 :weight 116}
                    {:number "D6251" :total_width 38.75 :door 1 :price 1852 :weight 124}
                    {:number "D6252" :total_width 44.75 :door 2 :price 2360 :weight 134}
                    {:number "D6253" :total_width 58.75 :door 2 :price 2640 :weight 150}
                    {:number "D6254" :total_width 68.75 :door 2 :price 2940 :weight 163}
                    {:number "D6255" :total_width 77.75 :door 2 :price 3252 :weight 174}]
             :option[{:name "070"} {:name "011"} {:name "D24"} {:name "072"}]}

            {:line "Lunette" 
             :category "Desk"
             :name "Double pedestal desk"
             :description "Box/box/ file pedestals"
             :item [{:number "D36030" :total_width 60 :total_depth 30 :kneespace 27.50 :price 2875 :weight 270}
                    {:number "D36630" :total_width 66 :total_depth 30 :kneespace 33.50 :price 2901 :weight 280}
                    {:number "D36636" :total_width 66 :total_depth 36 :kneespace 33.50 :price 2980 :weight 288}
                    {:number "D37236" :total_width 72 :total_depth 36 :kneespace 39.25 :price 3077 :weight 296}
                    {:number "D38442" :total_width 84 :total_depth 42 :kneespace 45.25 :price 3446 :weight 324}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "033a"} {:name "034a"}]}

            {:line "Lunette" 
             :category "Desk"
             :name "Bow front double"
             :description "pedestal desk Box/box/file pedestals"
             :item [{:number "D3B663" :total_width 66 :total_depth 36 :bow 30 :kneespace 33.50 :price 3528 :weight 282}
                    {:number "D3B723" :total_width 72 :total_depth 36 :bow 30 :kneespace 39.25 :price 3694 :weight 290}
                    {:number "D3B844" :total_width 84 :total_depth 42 :bow 36 :kneespace 45.25 :price 4138 :weight 318}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "033a"} {:name "034a"}]}

            {:line "Lunette" 
             :category "Desk"
             :name "Single pedestal desk"
             :description "Box/box/file pedestal"
             :item [{:number "D3S030" :total_width 60 :total_depth 30 :kneespace 42.50 :price 2421 :weight 205}
                    {:number "D3S630" :total_width 66 :total_depth 30 :kneespace 48.50 :price 2445 :weight 213}
                    {:number "D3S636" :total_width 66 :total_depth 36 :kneespace 48.50 :price 2526 :weight 219}
                    {:number "D3S723" :total_width 72 :total_depth 36 :kneespace 54.50 :price 2623 :weight 225}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "033a"} {:name "034a"}]}

            {:line "Lunette" 
             :category "Desk"
             :name "Bow front single pedestal desk "
             :description "Box/box/file pedestal"
             :item [{:number "D3SB66" :total_width 66 :total_depth 36 :bow 30 :kneespace 48.50 :price 3097 :weight 214}
                    {:number "D3SB72" :total_width 72 :total_depth 36 :bow 30 :kneespace 54.50 :price 3242 :weight 220}
                    {:number "D3SB84" :total_width 84 :total_depth 42 :bow 36 :kneespace 66.50 :price 3599 :weight 242}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "033a"} {:name "034a"}]}

            {:line "Lunette" 
             :category "Return"
             :name "Return with box/box/file pedestal"
             :description "No modesty panel Specify left or right, executive or scretarial height return"
             :item [{:number "D3R948" :total_width 48 :total_depth 19 :kneespace 31.75 :price 1292 :weight 107}
                    {:number "D3R960" :total_width 60 :total_depth 19 :kneespace 43.75 :price 1577 :weight 117}
                    {:number "D3R448" :total_width 48 :total_depth 24 :kneespace 31.75 :price 1461 :weight 119}
                    {:number "D3R460" :total_width 60 :total_depth 24 :kneespace 43.75 :price 1783 :weight 127}]
             :option[{:name "012"} {:name "030"} {:name "031"} {:name "032a"} {:name "033a"} {:name "034a"} {:name "039"} {:name "D64"}]}

            {:line "Lunette" 
             :category "Return"
             :name "Return with 36\" lateral file"
             :description "No modesty panel Specify left or right, executive or scretarial height return"
             :item [{:number "D3RF19" :total_width 48 :total_depth 19 :kneespace 11.50 :price 1775 :weight 137}
                    {:number "D3RF60" :total_width 60 :total_depth 19 :kneespace 23.50 :price 2029 :weight 147}
                    {:number "D3RF48" :total_width 48 :total_depth 24 :kneespace 11.50 :price 1940 :weight 149}
                    {:number "D3RF24" :total_width 60 :total_depth 24 :kneespace 23.50 :price 2194 :weight 157}]
             :option[{:name "012"} {:name "030"} {:name "031"} {:name "032a"} {:name "033a"} {:name "034a"} {:name "039"} {:name "D64"}]}

            {:line "Lunette" 
             :category "Bridge"
             :name "Bridge return"
             :item [{:number "D3RO19" :total_width 48 :total_depth 19 :price 937 :weight 85}
                    {:number "D3RO60" :total_width 60 :total_depth 19 :price 1034 :weight 97}
                    {:number "D3RO48" :total_width 48 :total_depth 24 :price 985 :weight 95}
                    {:number "D3RO24" :total_width 60 :total_depth 24 :price 1086 :weight 107}]
             :option[{:name "012"} {:name "030"} {:name "031"} {:name "032a"} {:name "033a"} {:name "034a"} {:name "039"} {:name "D64"}]}

            {:line "Lunette" 
             :category "Bridge"
             :name "Bridge Top"
             :item [{:number "D3RT19" :total_width 48 :total_depth 19 :price 507 :weight 40}
                    {:number "D3RT60" :total_width 60 :total_depth 19 :price 610 :weight 45}
                    {:number "D3RT48" :total_width 48 :total_depth 24 :price 560 :weight 45}
                    {:number "D3RT24" :total_width 60 :total_depth 24 :price 681 :weight 58}]
             :option[{:name "012"} {:name "030"} {:name "031"} {:name "032a"} {:name "033a"} {:name "034a"} {:name "039"} {:name "D64"}]}

            {:line "Lunette"
             :category "Credenza"
             :name "Credenza"
             :description "Box/box/file pedestal"
             :item [{:number "D3CS60" :total_width 60 :total_depth 19 :kneespace 42.50 :price 2001 :weight 160}
                    {:number "D3CS66" :total_width 66 :total_depth 19 :kneespace 48.50 :price 2123 :weight 175}
                    {:number "D3CS72" :total_width 72.375 :total_depth 19 :kneespace 54.50 :price 2278 :weight 190}
                    {:number "D3CS04" :total_width 60 :total_depth 24 :kneespace 42.50 :price 2141 :weight 185}
                    {:number "D3CS64" :total_width 66 :total_depth 24 :kneespace 48.50 :price 2272 :weight 200}
                    {:number "D3CS74" :total_width 72.375 :total_depth 24 :kneespace 54.50 :price 2438 :weight 215}]
             :option[{:name "010"} {:name "012"}]}

            {:line "Lunette"
             :category "Credenza"
             :name "Combination credenza"
             :description "Two doors, four box and two file drawers"
             :item [{:number "D36019" :total_width 60 :total_depth 19 :price 2702 :weight 234}
                    {:number "D36619" :total_width 66 :total_depth 19 :price 2860 :weight 256}
                    {:number "D37219" :total_width 72.375 :total_depth 19 :price 3058 :weight 280}
                    {:number "D36024" :total_width 60 :total_depth 24 :price 2972 :weight 270}
                    {:number "D36624" :total_width 66 :total_depth 24 :price 3145 :weight 293}
                    {:number "D37224" :total_width 72.375 :total_depth 24 :price 3375 :weight 314}]
             :option[{:name "010"} {:name "012"}]}

            {:line "Lunette"
             :category "Credenza"
             :name "Cabinet credenza"
             :description "Four doors, one adjustable shelf behind each end door, and one adjustable shelf behind the center pair of doors"
             :item [{:number "D3CD60" :total_width 60 :total_depth 19 :price 2548 :weight 187}
                    {:number "D3CD66" :total_width 66 :total_depth 19 :price 2682 :weight 205}
                    {:number "D3CD72" :total_width 72.375 :total_depth 19 :price 2854 :weight 224}
                    {:number "D3CD04" :total_width 60 :total_depth 24 :price 2803 :weight 216}
                    {:number "D3CD64" :total_width 66 :total_depth 24 :price 2950 :weight 234}
                    {:number "D3CD74" :total_width 72.375 :total_depth 24 :price 3140 :weight 251}]
             :option[{:name "010"} {:name "012"}]}

            {:line "Lunette" 
             :category "Credenza"
             :name "Cabinet credenza"
             :description "Two doors with one adjustable shelf behind"
             :item [{:number "D3CD30" :total_width 30 :total_depth 19 :price 1455 :weight 95}
                    {:number "D3CD36" :total_width 37.125 :total_depth 19 :price 1509 :weight 112}
                    {:number "D3CD24" :total_width 30 :total_depth 24 :price 1601 :weight 108}
                    {:number "D3CD34" :total_width 37.125 :total_depth 24 :price 1660 :weight 126}]
             :option[{:name "010"} {:name "012"}]}

            {:line "Lunette" 
             :category "Credenza"
             :name "Writing credenza"
             :description "Two box/box/file pedestals"
             :item [{:number "D3CK60" :total_width 60 :total_depth 19 :kneespace 27.50 :price 2366 :weight 200}
                    {:number "D3CK66" :total_width 66 :total_depth 19 :kneespace 33.50 :price 2504 :weight 219}
                    {:number "D3CK72" :total_width 72.375 :total_depth 19 :kneespace 39.50 :price 2685 :weight 238}
                    {:number "D3CK04" :total_width 60 :total_depth 24 :kneespace 27.50 :price 2538 :weight 231}
                    {:number "D3CK64" :total_width 66 :total_depth 24 :kneespace 33.50 :price 2686 :weight 250}
                    {:number "D3CK74" :total_width 72.375 :total_depth 24 :kneespace 39.50 :price 2880 :weight 269}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "030"} {:name "031"} {:name "032a"} {:name "033a"} {:name "034a"} {:name "039"} {:name "D38"}]}

            {:line "Lunette" 
             :category "Credenza"
             :name "Lateral file with kneespace"
             :description "72 3/8\" & 66\" units have a 36\" lateral file 60\" units have a 30\" lateral file"
             :item [{:number "D3LFS3" :total_width 60 :total_depth 19 :kneespace 28.125 :price 2141 :weight 160}
                    {:number "D3LFS2" :total_width 66 :total_depth 19 :kneespace 28.125 :price 2272 :weight 175}
                    {:number "D3LFS1" :total_width 72.375 :total_depth 19 :kneespace 34.50 :price 2438 :weight 190}
                    {:number "D3LFS6" :total_width 60 :total_depth 24 :kneespace 28.125 :price 2291 :weight 185}
                    {:number "D3LFS5" :total_width 66 :total_depth 24 :kneespace 28.125 :price 2431 :weight 200}
                    {:number "D3LFS4" :total_width 72.375 :total_depth 24 :kneespace 34.50 :price 2609 :weight 215}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "030"} {:name "031"} {:name "032a"} {:name "033a"} {:name "034a"} {:name "039"} {:name "D38"}]}

            {:line "Lunette" 
             :category "Table"
             :name "Peninsula table"
             :description "with Black cylinder"
             :item [{:number "D3P666" :total_width 66 :total_depth 36 :price 2155 :weight 125}
                    {:number "D3P672" :total_width 72 :total_depth 36 :price 2184 :weight 130}
                    {:number "D3P684" :total_width 84 :total_depth 36 :price 2348 :weight 155}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "030"} {:name "031"} {:name "032a"} {:name "033a"} {:name "034a"} {:name "039"} {:name "D38"}]}

            {:line "Lunette" 
             :category "Table"
             :name "Expanded peninsula table"
             :description "with Black cylinder"
             :item [{:number "D3P466" :total_width 66 :total_depth 36 :penninsula_depth 42 :price 2338 :weight 135}
                    {:number "D3P472" :total_width 72 :total_depth 36 :penninsula_depth 42 :price 2369 :weight 145}
                    {:number "D3P484" :total_width 84 :total_depth 36 :penninsula_depth 42 :price 2548 :weight 160}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "030"} {:name "031"} {:name "032a"} {:name "033a"} {:name "034a"} {:name "039"} {:name "D38"}]}

            {:line "Lunette" 
             :category "File"
             :name "Lateral file"
             :description "two drawers"
             :item [{:number "D3LFV0" :total_width 30 :total_depth 19 :total_height 29.5 :price 1637 :weight 140}
                    {:number "D3LFV2" :total_width 37.125 :total_depth 19 :total_height 29.5 :price 1682 :weight 150}
                    {:number "D3LF23" :total_width 30 :total_depth 24 :total_height 29.5 :price 1801 :weight 165}
                    {:number "D3LF24" :total_width 37.125 :total_depth 24 :total_height 29.5 :price 1851 :weight 175}]
             :option[{:name "012"}]}

            {:line "Lunette" 
             :category "File"
             :name "Lateral file"
             :description "four drawers"
             :item [{:number "D3LFH0" :total_width 60 :total_depth 19 :total_height 29.5 :price 2747 :weight 285}
                    {:number "D3LFH4" :total_width 72.375 :total_depth 19 :total_height 29.5 :price 2862 :weight 315}
                    {:number "D3LF60" :total_width 60 :total_depth 24 :total_height 29.5 :price 3036 :weight 335}
                    {:number "D3LF72" :total_width 72.375 :total_depth 24 :total_height 29.5 :price 3163 :weight 365}]
             :option[{:name "012"}]}

            {:line "Lunette" 
             :category "File"
             :name "Lateral file"
             :description "three drawer"
             :item [{:number "D3LFV3" :total_width 37.125 :total_depth 19 :total_height 41 :price 2137 :weight 225}
                    {:number "D3LF41" :total_width 37.125 :total_depth 24 :total_height 41 :price 2362 :weight 255}]
             :option[{:name "012"}]}

            {:line "Lunette" 
             :category "File"
             :name "Lateral file"
             :description "four drawers"
             :item [{:number "D3LFV4" :total_width 37.125 :total_depth 19 :total_height 53 :price 2659 :weight 307}
                    {:number "D3LF53" :total_width 37.125 :total_depth 24 :total_height 53 :price 2938 :weight 337}]
             :option[{:name "012"}]}

            {:line "Lunette" 
             :category "Table"
             :name "Computer corner table"
             :description "with articulating arm for"
             :item [{:number "D3KB41" :total_width 41 :total_depth 41 :return 19 :price 1770 :weight 151}
                    {:number "D3KB46" :total_width 46 :total_depth 46 :return 24 :price 2299 :weight 165}]
             :option[{:name "010"}]}

            {:line "Lunette" 
             :category "Table"
             :name "Computer table"
             :description "with aticulating arm and grommet in back panel"
             :item [{:number "D3KB36" :total_width 37.125 :total_depth 24 :price 1481 :weight 112}
                    {:number "D3KB42" :total_width 42 :total_depth 24 :price 1535 :weight 121}
                    {:number "D3KB48" :total_width 48 :total_depth 24 :price 1599 :weight 127}]}

            {:line "Lunette" 
             :category "Table"
             :name "Telephone table"
             :description "with hinged door"
             :item [{:number "D3TT19" :total_width 19 :total_depth 19 :total_height 27 :price 932 :weight 80}
                    {:number "D3TT24" :total_width 24 :total_depth 24 :total_height 27 :price 1031 :weight 90}]}

            {:line "Lunette" 
             :category "Hutch"
             :name "Hinged door hutch"
             :description "four doors with one adjustable shelf behind each end door and one adjustable shelf behind center pair of doors. Clear space below doors: 42\" unit 12\" high 48\" unit 18\" high"
             :item [{:number "D3H602" :total_width 60 :total_depth 15 :total_height 42 :price 2478 :weight 184}
                    {:number "D3H662" :total_width 66 :total_depth 15 :total_height 42 :price 2556 :weight 191}
                    {:number "D3H722" :total_width 72.375 :total_depth 15 :total_height 42 :price 2635 :weight 202}
                    {:number "D3H608" :total_width 60 :total_depth 15 :total_height 48 :price 2754 :weight 196}
                    {:number "D3H668" :total_width 66 :total_depth 15 :total_height 48 :price 2839 :weight 204}
                    {:number "D3H728" :total_width 72.375 :total_depth 15 :total_height 48 :price 2927 :weight 215}]
             :option[{:name "010"}]}

            {:line "Lunette" 
             :category "Hutch"
             :name "Hinged door hutch"
             :description "two doors with one adjustable shelf behind. Clear space below doors: 42\" unit 12\" high 48\" unit 18\" high"
             :item [{:number "D3H302" :total_width 30 :total_depth 15 :total_height 42 :price 1470 :weight 107}
                    {:number "D3H362" :total_width 37.125 :total_depth 15 :total_height 42 :price 1515 :weight 121}
                    {:number "D3H308" :total_width 30 :total_depth 15 :total_height 48 :price 1515 :weight 113}
                    {:number "D3H368" :total_width 37.125 :total_depth 15 :total_height 48 :price 1561 :weight 128}]
             :option[{:name "010"}]}

            {:line "Lunette" 
             :category "Cabinet"
             :name "Wall hung storage bin with hinged doors"
             :item [{:number "D3W130" :total_width 30 :total_depth 15 :total_height 18 :door 2 :price 1025 :weight 50}
                    {:number "D3W136" :total_width 37.125 :total_depth 15 :total_height 18 :door 2 :price 1056 :weight 60}
                    {:number "D3W160" :total_width 60 :total_depth 15 :total_height 18 :door 4 :price 1616 :weight 80}
                    {:number "D3W166" :total_width 66 :total_depth 15 :total_height 18 :door 4 :price 1736 :weight 82}
                    {:number "D3W172" :total_width 72.375 :total_depth 15 :total_height 18 :door 4 :price 1847 :weight 95}]
             :option[{:name "010"}]}

            {:line "Lunette" 
             :category "Bookcase"
             :name "Open bookcase"
             :item [{:number "D3BK30" :total_width 30 :total_depth 12 :total_height 30 :shelf 1 :price 767 :weight 54}
                    {:number "D3BK31" :total_width 30 :total_depth 12 :total_height 36 :shelf 2 :price 857 :weight 69}
                    {:number "D3BK32" :total_width 30 :total_depth 12 :total_height 42 :shelf 2 :price 1041 :weight 84}
                    {:number "D3BK33" :total_width 30 :total_depth 12 :total_height 48 :shelf 3 :price 1150 :weight 89}
                    {:number "D3BK34" :total_width 30 :total_depth 12 :total_height 60 :shelf 3 :price 1286 :weight 104}
                    {:number "D3BK35" :total_width 30 :total_depth 12 :total_height 66 :shelf 4 :price 1373 :weight 114}
                    {:number "D3BK36" :total_width 30 :total_depth 12 :total_height 72 :shelf 4 :price 1441 :weight 124}
                    {:number "D3BK37" :total_width 30 :total_depth 12 :total_height 84 :shelf 5 :price 1583 :weight 134}
                    {:number "D3BK38" :total_width 30 :total_depth 12 :total_height 96 :shelf 6 :price 1680 :weight 154}
                    {:number "D3BK60" :total_width 36 :total_depth 12 :total_height 30 :shelf 1 :price 821 :weight 64}
                    {:number "D3BK61" :total_width 36 :total_depth 12 :total_height 36 :shelf 2 :price 939 :weight 79}
                    {:number "D3BK62" :total_width 36 :total_depth 12 :total_height 42 :shelf 2 :price 1096 :weight 94}
                    {:number "D3BK63" :total_width 36 :total_depth 12 :total_height 48 :shelf 3 :price 1184 :weight 99}
                    {:number "D3BK64" :total_width 36 :total_depth 12 :total_height 60 :shelf 3 :price 1343 :weight 114}
                    {:number "D3BK65" :total_width 36 :total_depth 12 :total_height 66 :shelf 4 :price 1461 :weight 124}
                    {:number "D3BK66" :total_width 36 :total_depth 12 :total_height 72 :shelf 4 :price 1556 :weight 134}
                    {:number "D3BK67" :total_width 36 :total_depth 12 :total_height 84 :shelf 5 :price 1720 :weight 144}
                    {:number "D3BK68" :total_width 36 :total_depth 12 :total_height 96 :shelf 6 :price 1808 :weight 164}]
             :option[{:name "010"} {:name "060"} {:name "D23"} {:name "D27"} {:name "D29"}]}

            {:line "Lunette" 
             :category "Cabinet"
             :name "Wardrobe/Closet with two hinged doors."
             :description "Hat shelf and clothes bar behind left door. Four adjustable shelves behind right door."
             :item [{:number "D3W246" :total_width 37.125 :total_depth 24 :total_height 72 :price 2611 :weight 232}]
             :option[{:name "010"} {:name "060"} {:name "D23"} {:name "D27"} {:name "D29"}]}

            {:line "Banker"
             :category "Desk"
             :name "Double pedestal desk"
             :description "Box/box/ file pedestals"
             :item [{:number "D26030" :total_width 60 :total_depth 30 :kneespace 27.50 :price 2781 :weight 270}
                    {:number "D26630" :total_width 66 :total_depth 30 :kneespace 33.50 :price 2805 :weight 280}
                    {:number "D26636" :total_width 66 :total_depth 36 :kneespace 33.50 :price 2885 :weight 288}
                    {:number "D27236" :total_width 72 :total_depth 36 :kneespace 39.25 :price 2981 :weight 296}
                    {:number "D28442" :total_width 84 :total_depth 42 :kneespace 45.25 :price 3343 :weight 324}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "033a"} {:name "034a"} {:name "D46"}]}

            {:line "Banker"
             :category "Desk"
             :name "Bow front double pedestal desk"
             :description "Box/box/file pedestals"
             :item [{:number "D2B663" :total_width 66 :total_depth 36 :total_height 30 :kneespace 33.50 :price 3422 :weight 282}
                    {:number "D2B723" :total_width 72 :total_depth 36 :total_height 30 :kneespace 39.25 :price 3599 :weight 290}
                    {:number "D2B844" :total_width 84 :total_depth 42 :total_height 36 :kneespace 45.25 :price 4013 :weight 318}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "033a"} {:name "034a"} {:name "D46"}]}

            {:line "Banker"
             :category "Desk"
             :name "Single pedestal desk"
             :description "Box/box/file pedestal"
             :item [{:number "D2S030" :total_width 60 :total_depth 30 :kneespace 42.50 :price 2325 :weight 205}
                    {:number "D2S630" :total_width 66 :total_depth 30 :kneespace 48.50 :price 2351 :weight 213}
                    {:number "D2S636" :total_width 66 :total_depth 36 :kneespace 48.50 :price 2430 :weight 219}
                    {:number "D2S723" :total_width 72 :total_depth 36 :kneespace 54.50 :price 2527 :weight 225}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "033a"} {:name "034a"} {:name "D46"}]}

            {:line "Banker"
             :category "Desk"
             :name "Bow front single pedestal desk"
             :description "Box/box/file pedestal"
             :item [{:number "D2SB66" :total_width 66 :total_depth 36 :total_height 30 :kneespace 48.50 :price 3097 :weight 214}
                    {:number "D2SB72" :total_width 72 :total_depth 36 :total_height 30 :kneespace 54.50 :price 3147 :weight 220}
                    {:number "D2SB84" :total_width 84 :total_depth 42 :total_height 36 :kneespace 66.50 :price 3491 :weight 242}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "033a"} {:name "034a"} {:name "D46"}]}

            {:line "Banker"
             :category "Return"
             :name "Return with box/box/file pedestal"
             :description "No modesty panel Specify left or right, executive or scretarial height return"
             :item [{:number "D2R948" :total_width 48 :total_depth 19 :kneespace 31.75 :price 1292 :weight 107}
                    {:number "D2R960" :total_width 60 :total_depth 19 :kneespace 43.75 :price 1577 :weight 117}
                    {:number "D2R448" :total_width 48 :total_depth 24 :kneespace 31.75 :price 1461 :weight 119}
                    {:number "D2R460" :total_width 60 :total_depth 24 :kneespace 43.75 :price 1783 :weight 127}]
             :option[{:name "012"} {:name "030"} {:name "031"} {:name "032a"} {:name "033a"} {:name "034a"} {:name "039"} {:name "D46"} {:name "D47"} {:name "D64"}]}

            {:line "Banker"
             :category "Return"
             :name "Return with 36\" lateral file"
             :description "No modesty panel Specify left or right, executive or scretarial height return"
             :item [{:number "D2RF19" :total_width 48 :total_depth 19 :kneespace 11.50 :price 1775 :weight 137}
                    {:number "D2RF60" :total_width 60 :total_depth 19 :kneespace 23.50 :price 2029 :weight 147}
                    {:number "D2RF48" :total_width 48 :total_depth 24 :kneespace 11.50 :price 1940 :weight 149}
                    {:number "D2RF24" :total_width 60 :total_depth 24 :kneespace 23.50 :price 2194 :weight 157}]
             :option[{:name "012"} {:name "030"} {:name "031"} {:name "032a"} {:name "033a"} {:name "034a"} {:name "039"} {:name "D46"} {:name "D47"} {:name "D64"}]}

            {:line "Banker"
             :category "Return"
             :name "Bridge Return"
             :item [{:number "D2RO19" :total_width 48 :total_depth 19 :price 937 :weight 85}
                    {:number "D2RO60" :total_width 60 :total_depth 19 :price 1034 :weight 97}
                    {:number "D2RO48" :total_width 48 :total_depth 24 :price 985 :weight 95}
                    {:number "D2RO24" :total_width 60 :total_depth 24 :price 1086 :weight 107}]
             :option[{:name "012"} {:name "030"} {:name "031"} {:name "032a"} {:name "033a"} {:name "034a"} {:name "039"} {:name "D46"} {:name "D47"} {:name "D64"}]}

            {:line "Banker"
             :category "Bridge"
             :name "Bridge Top"
             :item [{:number "D2RT19" :total_width 48 :total_depth 19 :price 507 :weight 40}
                    {:number "D2RT60" :total_width 60 :total_depth 19 :price 610 :weight 45}
                    {:number "D2RT48" :total_width 48 :total_depth 24 :price 560 :weight 45}
                    {:number "D2RT24" :total_width 60 :total_depth 24 :price 681 :weight 58}]
             :option[{:name "012"} {:name "030"} {:name "031"} {:name "032a"} {:name "033a"} {:name "034a"} {:name "039"} {:name "D46"} {:name "D47"} {:name "D64"}]}

            {:line "Banker"
             :category "Credenza"
             :name "Credenza"
             :description "Box/box/file pedestal"
             :item [{:number "D2CS60" :total_width 60 :total_depth 19 :kneespace 42.50 :price 2001 :weight 160}
                    {:number "D2CS66" :total_width 66 :total_depth 19 :kneespace 48.50 :price 2123 :weight 175}
                    {:number "D2CS72" :total_width 72.375 :total_depth 19 :kneespace 54.50 :price 2278 :weight 190}
                    {:number "D2CS04" :total_width 60 :total_depth 24 :kneespace 42.50 :price 2141 :weight 185}
                    {:number "D2CS64" :total_width 66 :total_depth 24 :kneespace 48.50 :price 2272 :weight 200}
                    {:number "D2CS74" :total_width 72.375 :total_depth 24 :kneespace 54.50 :price 2438 :weight 215}]
             :option[{:name "010"} {:name "012"} {:name "D46"} {:name "D48"}]}

            {:line "Banker"
             :category "Credenza"
             :name "Combination credenza"
             :description "Two doors, four box and two file drawers"
             :item [{:number "D26019" :total_width 60 :total_depth 19 :price 2621 :weight 234}
                    {:number "D26619" :total_width 66 :total_depth 19 :price 2777 :weight 256}
                    {:number "D27219" :total_width 72.375 :total_depth 19 :price 2964 :weight 280}
                    {:number "D26024" :total_width 60 :total_depth 24 :price 2883 :weight 270}
                    {:number "D26624" :total_width 66 :total_depth 24 :price 3051 :weight 293}
                    {:number "D27224" :total_width 72.375 :total_depth 24 :price 3273 :weight 314}]
             :option[{:name "010"} {:name "012"} {:name "D46"} {:name "D48"}]}

            {:line "Banker"
             :category "Credenza"
             :name "Cabinet credenza"
             :description "Four doors one adjustable shelf behind each end, door and one adjustable shelf behind the center pair of doors"
             :item [{:number "D2CD60" :total_width 60 :total_depth 19 :price 2472 :weight 187}
                    {:number "D2CD66" :total_width 66 :total_depth 19 :price 2602 :weight 205}
                    {:number "D2CD72" :total_width 72.375 :total_depth 19 :price 2760 :weight 224}
                    {:number "D2CD04" :total_width 60 :total_depth 24 :price 2719 :weight 216}
                    {:number "D2CD64" :total_width 66 :total_depth 24 :price 2862 :weight 234}
                    {:number "D2CD74" :total_width 72.375 :total_depth 24 :price 3046 :weight 251}]
             :option[{:name "010"} {:name "012"} {:name "D46"} {:name "D48"}]}

            {:line "Banker"
             :category "Credenza"
             :name "Cabinet credenza"
             :description "Two doors with one adjustable shelf behind"
             :item [{:number "D2CD30" :total_width 30 :total_depth 19 :price 1372 :weight 95}
                    {:number "D2CD36" :total_width 37.125 :total_depth 19 :price 1413 :weight 112}
                    {:number "D2CD24" :total_width 30 :total_depth 24 :price 1552 :weight 108}
                    {:number "D2CD34" :total_width 37.125 :total_depth 24 :price 1611 :weight 126}]
             :option[{:name "010"} {:name "012"} {:name "D46"} {:name "D48"}]}

            {:line "Banker"
             :category "Credenza"
             :name "Writing credenza"
             :description "Two box/box/file pedestals"
             :item [{:number "D2CK60" :total_width 60 :total_depth 19 :kneespace 27.50 :price 2295 :weight 200}
                    {:number "D2CK66" :total_width 66 :total_depth 19 :kneespace 33.50 :price 2429 :weight 219}
                    {:number "D2CK72" :total_width 72.375 :total_depth 19 :kneespace 39.50 :price 2590 :weight 238}
                    {:number "D2CK04" :total_width 60 :total_depth 24 :kneespace 27.50 :price 2462 :weight 231}
                    {:number "D2CK64" :total_width 66 :total_depth 24 :kneespace 33.50 :price 2605 :weight 250}
                    {:number "D2CK74" :total_width 72.375 :total_depth 24 :kneespace 39.50 :price 2794 :weight 269}]
             :option[{:name "012"} {:name "030"} {:name "031"} {:name "032a"} {:name "033a"} {:name "034a"} {:name "039"} {:name "D46"} {:name "D47"}]}

            {:line "Banker"
             :category "Credenza"
             :name "Lateral file with kneespace"
             :description "72 3/8\" & 66\" units have a 36\" lateral file 60\" units have a 30\" lateral file"
             :item [{:number "D2LFS3" :total_width 60 :total_depth 19 :kneespace 28.125 :price 2141 :weight 160}
                    {:number "D2LFS2" :total_width 66 :total_depth 19 :kneespace 28.125 :price 2272 :weight 175}
                    {:number "D2LFS1" :total_width 72.375 :total_depth 19 :kneespace 34.50      :price 2438 :weight 190}
                    {:number "D2LFS6" :total_width 60 :total_depth 24 :kneespace 28.125 :price 2291 :weight 185}
                    {:number "D2LFS5" :total_width 66 :total_depth 24 :kneespace 28.125 :price 2431 :weight 200}
                    {:number "D2LFS4" :total_width 72.375 :total_depth 24 :kneespace 34.50 :price 2609 :weight 215}]
             :option[{:name "012"} {:name "030"} {:name "031"} {:name "032a"} {:name "033a"} {:name "034a"} {:name "039"} {:name "D46"} {:name "D47"}]}

            {:line "Banker"
             :category "Table"
             :name "Peninsula table"
             :description "with chrome cylinder"
             :item [{:number "D2P666" :total_width 66 :total_depth 36 :price 2155 :weight 125}
                    {:number "D2P672" :total_width 72 :total_depth 36 :price 2184 :weight 130}
                    {:number "D2P684" :total_width 84 :total_depth 36 :price 2348 :weight 155}]
             :option[{:name "012"} {:name "030"} {:name "031"} {:name "032a"} {:name "033a"} {:name "034a"} {:name "039"} {:name "D46"} {:name "D47"}]}

            {:line "Banker"
             :category "Table"
             :name "Expanded peninsula table"
             :description "with chrome cylinder"
             :item [{:number "D2P466" :total_width 66 :total_depth 36 :penninsula_depth 42 :price 2338 :weight 135}
                    {:number "D2P472" :total_width 72 :total_depth 36 :penninsula_depth 42 :price 2369 :weight 145}
                    {:number "D2P484" :total_width 84 :total_depth 36 :penninsula_depth 42 :price 2548 :weight 160}]
             :option[{:name "012"} {:name "030"} {:name "031"} {:name "032a"} {:name "033a"} {:name "034a"} {:name "039"} {:name "D46"} {:name "D47"}]}

            {:line "Banker"
             :category "File"
             :name "Lateral file"
             :description "two drawers"
             :item [{:number "D2LFV0" :total_width 30 :total_depth 19 :total_height 29.5 :price 1637 :weight 140}
                    {:number "D2LFV2" :total_width 37.125 :total_depth 19 :total_height 29.5 :price 1682 :weight 150}
                    {:number "D2LF23" :total_width 30 :total_depth 24 :total_height 29.5 :price 1801 :weight 165}
                    {:number "D2LF24" :total_width 37.125 :total_depth 24 :total_height 29.5 :price 1851 :weight 175}]
             :option[{:name "012"} {:name "D47"}]}

            {:line "Banker"
             :category "File"
             :name "Lateral file"
             :description "four drawers"
             :item [{:number "D2LFH0" :total_width 60 :total_depth 19 :total_height 29.5 :price 2747 :weight 285}
                    {:number "D2LFH4" :total_width 72.375 :total_depth 19 :total_height 29.5 :price 2862 :weight 315}
                    {:number "D2LF60" :total_width 60 :total_depth 24 :total_height 29.5 :price 3036 :weight 335}
                    {:number "D2LF72" :total_width 72.375 :total_depth 24 :total_height 29.5 :price 3163 :weight 365}]
             :option[{:name "012"} {:name "D47"}]}

            {:line "Banker"
             :category "File"
             :name "Lateral file"
             :description "three drawer"
             :item [{:number "D2LFV3" :total_width 37.125 :total_depth 19 :total_height 41 :price 2137 :weight 225}
                    {:number "D2LF41" :total_width 37.125 :total_depth 24 :total_height 41 :price 2362 :weight 255}]
             :option[{:name "012"} {:name "D47"}]}

            {:line "Banker"
             :category "File"
             :name "Lateral file"
             :description "four drawers"
             :item [{:number "D2LFV4" :total_width 37.125 :total_depth 19 :total_height 53 :price 2659 :weight 307}
                    {:number "D2LF53" :total_width 37.125 :total_depth 24 :total_height 53 :price 2938 :weight 337}]
             :option[{:name "012"} {:name "D47"}]}

            {:line "Banker"
             :category "Table"
             :name "Computer corner table"
             :description "with articulating arm for use with:"
             :item [{:number "D2KB41" :total_width 41 :total_depth 41 :return 19 :price 1770 :weight 151}
                    {:number "D2KB46" :total_width 46 :total_depth 46 :return 24 :price 2299 :weight 165}]
             :option[{:name "010"}]}

            {:line "Banker"
             :category "Table"
             :name "Computer table"
             :description "with aticulating arm and grommet in back panel"
             :item [{:number "D2KB36" :total_width 37.125 :total_depth 24 :price 1481 :weight 112}
                    {:number "D2KB42" :total_width 42 :total_depth 24 :price 1535 :weight 121}
                    {:number "D2KB48" :total_width 48 :total_depth 24 :price 1599 :weight 127}]
             :option[{:name "010"}]}

            {:line "Banker"
             :category "Table"
             :name "Telephone table"
             :description "with hinged door"
             :item [{:number "D2TT19" :total_width 19 :total_depth 19 :total_height 27 :price 904 :weight 80}
                    {:number "D2TT24" :total_width 24 :total_depth 24 :total_height 27 :price 1003 :weight 90}]
             :option[{:name "010"}]}

            {:line "Banker"
             :category "Hutch"
             :name "Hinged door hutch"
             :description "four doors with one adjustable shelf behind each pair of doors. Clear space below doors: 42\" unit 12\" high 48\" unit 18\" high"
             :item [{:number "D2H602" :total_width 60 :total_depth 15 :total_height 42 :price 2334 :weight 184}
                    {:number "D2H662" :total_width 66 :total_depth 15 :total_height 42 :price 2407 :weight 191}
                    {:number "D2H722" :total_width 72.375 :total_depth 15 :total_height 42 :price 2481 :weight 202}
                    {:number "D2H608" :total_width 60 :total_depth 15 :total_height 48 :price 2593 :weight 196}
                    {:number "D2H668" :total_width 66 :total_depth 15 :total_height 48 :price 2674 :weight 204}
                    {:number "D2H728" :total_width 72.375 :total_depth 15 :total_height 48 :price 2756 :weight 215}]
             :option[{:name "010"} {:name "D48"}]}

            {:line "Banker"
             :category "Hutch"
             :name "Hinged door hutch"
             :description "two doors with one adjustable shelf behind. Clear space belw doors 42\" unit 12\" high 48\" unit 18\" high"
             :item [{:number "D2H302" :total_width 30 :total_depth 15 :total_height 42 :price 1378 :weight 107}
                    {:number "D2H362" :total_width 37.125 :total_depth 15 :total_height 42 :price 1421 :weight 121}
                    {:number "D2H308" :total_width 30 :total_depth 15 :total_height 48 :price 1421 :weight 113}
                    {:number "D2H368" :total_width 37.125 :total_depth 15 :total_height 48 :price 1465 :weight 128}]
             :option[{:name "010"} {:name "D48"}]}

            {:line "Banker"
             :category "Cabinet"
             :name "Wall hung cabinet with hinged doors"
             :item [{:number "D2W130" :total_width 30 :total_depth 15 :total_height 18 :door 2 :price 1025 :weight 50}
                    {:number "D2W136" :total_width 37.125 :total_depth 15 :total_height 18 :door 2 :price 1056 :weight 60}
                    {:number "D2W160" :total_width 60 :total_depth 15 :total_height 18 :door 4 :price 1616 :weight 80}
                    {:number "D2W166" :total_width 66 :total_depth 15 :total_height 18 :door 4 :price 1736 :weight 88}
                    {:number "D2W172" :total_width 72.375 :total_depth 15 :total_height 18 :door 4 :price 1847 :weight 95}]
             :option[{:name "010"} {:name "D48"}]}

            {:line "Banker"
             :category "Bookcase"
             :name "Open Bookcase"
             :item [{:number "D2BK30" :total_width 30 :total_depth 12 :total_height 30 :shelf 1 :price 767 :weight 54}
                    {:number "D2BK31" :total_width 30 :total_depth 12 :total_height 36 :shelf 2 :price 857 :weight 69}
                    {:number "D2BK32" :total_width 30 :total_depth 12 :total_height 42 :shelf 2 :price 1041 :weight 84}
                    {:number "D2BK33" :total_width 30 :total_depth 12 :total_height 48 :shelf 3 :price 1150 :weight 89}
                    {:number "D2BK34" :total_width 30 :total_depth 12 :total_height 60 :shelf 3 :price 1286 :weight 104}
                    {:number "D2BK35" :total_width 30 :total_depth 12 :total_height 66 :shelf 4 :price 1373 :weight 114}
                    {:number "D2BK36" :total_width 30 :total_depth 12 :total_height 72 :shelf 4 :price 1441 :weight 124}
                    {:number "D2BK37" :total_width 30 :total_depth 12 :total_height 84 :shelf 5 :price 1583 :weight 134}
                    {:number "D2BK38" :total_width 30 :total_depth 12 :total_height 96 :shelf 6 :price 1680 :weight 154}
                    {:number "D2BK60" :total_width 36 :total_depth 12 :total_height 30 :shelf 1 :price 821 :weight 64}
                    {:number "D2BK61" :total_width 36 :total_depth 12 :total_height 36 :shelf 2 :price 939 :weight 79}
                    {:number "D2BK62" :total_width 36 :total_depth 12 :total_height 42 :shelf 2 :price 1096 :weight 94}
                    {:number "D2BK63" :total_width 36 :total_depth 12 :total_height 48 :shelf 3 :price 1184 :weight 99}
                    {:number "D2BK64" :total_width 36 :total_depth 12 :total_height 60 :shelf 3 :price 1343 :weight 114}
                    {:number "D2BK65" :total_width 36 :total_depth 12 :total_height 66 :shelf 4 :price 1461 :weight 124}
                    {:number "D2BK66" :total_width 36 :total_depth 12 :total_height 72 :shelf 4 :price 1556 :weight 134}
                    {:number "D2BK67" :total_width 36 :total_depth 12 :total_height 84 :shelf 5 :price 1720 :weight 144}
                    {:number "D2BK68" :total_width 36 :total_depth 12 :total_height 96 :shelf 6 :price 1808 :weight 164}]
             :option[{:name "010"} {:name "060"} {:name "D23"} {:name "D27"} {:name "D29"}]}

            {:line "Banker"
             :category "Cabinet"
             :name "Wardrobe/Closet with two hinged doors."
             :description "Hat shelf and clothes bar behind left door. Four adjustable shelves behind right door."
             :item [{:number "D2W246" :total_width 37.125 :total_depth 24 :total_height 72 :price 2611 :weight 232}]
             :option[{:name "010"} {:name "060"} {:name "D23"} {:name "D27"} {:name "D29"}]}

            {:line "Stratford"
             :category "Desk"
             :name "Double Pedestal Desk Flush top"
             :description "Pencil/box/file pedestals, center pencil drawer, and three pullout writing shelves. Pedestal locks."
             :item [{:number "DSEF3672" :total_width 72 :total_depth 36 :kneespace 32 :price 11212 :weight 362}]
             :option[{:name "013"} {:name "D30"} {:name "D40"}]}

            {:line "Stratford"
             :category "Desk"
             :name "Conference Desk Overhanging top"
             :description "Pencil/box/file pedestals, center pencil drawer, and two pullout writing shelves. Pedestal locks."
             :item [{:number "DSEO3672" :total_width 72 :total_depth 36 :kneespace 26.5 :price 10545 :weight 353}
                    {:number "DSEO4284" :total_width 84 :total_depth 42 :kneespace 34.5 :price 11522 :weight 390}]
             :option[{:name "013"} {:name "D30"} {:name "D40"}]}

            {:line "Stratford"
             :category "Credenza"
             :name "Combination Credenza"
             :description "Two box/box/file pedestals, double hinged doors in center with adjustable shelf behind."
             :item [{:number "DSEC1970" :total_width 70.5 :total_depth 19.5 :price 9129 :weight 306}]
             :option[{:name "010"} {:name "012"} {:name "D30"} {:name "D44"}]}

            {:line "Stratford"
             :category "Credenza"
             :name "File Credenza"
             :description "Four lateral file drawers."
             :item [{:number "DSLF1970" :total_width 70.5 :total_depth 19.5 :price 8845 :weight 295}]
             :option[{:name "012"} {:name "D32"} {:name "D44"}]}

            {:line "Stratford"
             :category "Workstation"
             :name "Executive Return Desk"
             :description "A - Desk with pencil/box/file pedestal, center pencil drawer, and two pullout writing shelves. B - Return with pencil/box/file pedestal. Pedestal locks. Specify: Return left or right."
             :style[{:number "DSEL8472" :price 13433 :weight 422
                     :item [{:part "Desk" :total_width 72 :total_depth 36}
                            {:part "Return" :total_width 48 :total_depth 24}]}
                    {:number "DSEL9084" :price 14815 :weight 459
                     :item [{:part "Desk" :total_width 84 :total_depth 42}
                            {:part "Return" :total_width 48 :total_depth 24}]}]
             :option[{:name "013"} {:name "D31"} {:name "D41"}]}

            {:line "Stratford"
             :category "Workstation"
             :name "Executive U Workstation"
             :description "A - Desk with pencil/box/file pedestal, center pencil drawer, and two pullout writing shelves. B - Open Bridge C - Credenza has one pencil/box/file pedestal, two hinged doors with adjustable shelf behind, and open bookcase with adjustable shelf. Pedestal lock in desk only. Specify: Return left or right."
             :style[{:number "DSEU104" :price 19563 :weight 575
                     :item [{:part "Desk" :total_width 72 :total_depth 36}
                            {:part "Open Bridge" :total_width 48 :total_depth 24}
                            {:part "Credenza" :total_width 70.5 :total_depth 19.5}]}
                    {:number "DSEU109" :price 21158 :weight 625
                     :item [{:part "Desk" :total_width 84 :total_depth 42}
                            {:part "Open Bridge" :total_width 48 :total_depth 24}
                            {:part "Credenza" :total_width 84 :total_depth 19.5}]}]
             :option[{:name "010"} {:name "012"} {:name "013"} {:name "D34"} {:name "D42"}]}

            {:line "Stratford"
             :category "Hutch"
             :name "Hutch"
             :description "with leaded glass doors. 48\" high. For credenza tops."
             :item [{:number "DSEH1468" :total_width 70.5 :total_depth 14 :price 9312 :weight 220}
                    {:number "DSEH1484" :total_width 84 :total_depth 14 :price 10070 :weight 233}]
             :option[{:name "D45"}]}

            {:line "Georgetown"
             :category "Desk"
             :name "Double Pedestal Desk"
             :description "Pencil/box/file pedestals and center drawer. Three pullout dictation shelves."
             :variant [{:name "Straight Grain Wood Top"
                      :item [{:number "DGEF3060" :total_width 60 :total_depth 30 :kneespace 25 :price 3797 :weight 301}
                             {:number "DGEF3666" :total_width 66 :total_depth 36 :kneespace 31 :price 3969 :weight 338}
                             {:number "DGEF3672" :total_width 72 :total_depth 36 :kneespace 32 :price 4314 :weight 350}]
                      :option[{:name "013"} {:name "033a"} {:name "034a"} {:name "039"} {:name "D11"} {:name "D30"} {:name "D40"}]}
                     {:name "Gold Tooled Leather Top"
                      :description "Specify leather color: W= Ebony X= Navy, Y= Emerald, Z= Cocoa"
                      :item [{:number "DLT3666" :total_width 66 :total_depth 36 :kneespace 31 :price 5591 :weight 338}
                             {:number "DLT3672" :total_width 72 :total_depth 36 :kneespace 32 :price 5981 :weight 350}]
                      :option[{:name "013"} {:name "033a"} {:name "034a"} {:name "039"} {:name "D11"} {:name "D30"} {:name "D40"}]}
                     {:name "Burl Top"
                      :description "Border Veneer: Quartered Walnut Infield Veneer: Walnut Burl"
                      :item [{:number "DWB3666" :total_width 66 :total_depth 36 :kneespace 31 :price 5992 :weight 338}
                             {:number "DWB3672" :total_width 72 :total_depth 36 :kneespace 32 :price 6289 :weight 350}
                             {:number "DWB3060" :total_width 60 :total_depth 30 :kneespace 25 :price 5547 :weight 301}]
                      :option[{:name "013"} {:name "033a"} {:name "034a"} {:name "039"} {:name "D11"} {:name "D30"} {:name "D40"}]}
                     {:name "Crotch Top"
                      :description "Border Veneer: Quartered Mahogany Infield Veneer: Crotch Cedar"
                      :item [{:number "DCC3666" :total_width 66 :total_depth 36 :kneespace 31 :price 5559 :weight 338}
                             {:number "DCC3672" :total_width 72 :total_depth 36 :kneespace 32 :price 5961 :weight 350}
                             {:number "DCC3060" :total_width 60 :total_depth 30 :kneespace 25 :price 5045 :weight 301}]
                      :option[{:name "013"} {:name "033a"} {:name "034a"} {:name "039"} {:name "D11"} {:name "D30"} {:name "D40"}]}
                     {:name "Black Diamond Marquetry Top"
                      :description "Quartered Lacewood Border Reverse Diamond Anigre Fields Black Marquetry Inlay"
                      :item [{:number "DGLA3666" :total_width 66 :total_depth 36 :kneespace 31 :price 5489 :weight 338}
                             {:number "DGLA3672" :total_width 72 :total_depth 36 :kneespace 32 :price 5868 :weight 350}
                             {:number "DGLA3060" :total_width 60 :total_depth 30 :kneespace 25 :price 5175 :weight 301}]
                      :option[{:name "013"} {:name "033a"} {:name "034a"} {:name "039"} {:name "D11"} {:name "D30"} {:name "D40"}]}
                     {:name "Reverse Diamond Top"
                      :description "Pommele Makore Border Reverse Diamond Cherry Fields Holly Marquetry Inlay"
                      :item [{:number "DGRD3666" :total_width 66 :total_depth 36 :kneespace 31 :price 5920 :weight 338}
                             {:number "DGRD3672" :total_width 72 :total_depth 36 :kneespace 32 :price 6352 :weight 350}]
                      :option[{:name "013"} {:name "033a"} {:name "034a"} {:name "039"} {:name "D11"} {:name "D30"} {:name "D40"}]}
                     {:name "Book Match Top"
                      :description "Quartered Border Veneer Book Matched Infield Veneer T= Walnut, U= Cherry, V= Mahogany"
                      :item [{:number "DBM3666" :total_width 66 :total_depth 36 :kneespace 31 :price 5034 :weight 338}
                             {:number "DBM3672" :total_width 72 :total_depth 36 :kneespace 32 :price 5438 :weight 350}
                             {:number "DBM3060" :total_width 60 :total_depth 30 :kneespace 25 :price 4671 :weight 301}]
                      :option[{:name "013"} {:name "033a"} {:name "034a"} {:name "039"} {:name "D11"} {:name "D30"} {:name "D40"}]}]}

            {:line "Georgetown"
             :category "Desk"
             :name "Conference Desk"
             :description "Pencil/box/file pedestals and center drawer. Two pullout dictation shelves."
             :variant [{:name "Straight Grain Wood Top"
                      :item [{:number "DGEO3672" :total_width 72 :total_depth 36 :kneespace 31 :price 3791 :weight 332}
                             {:number "DGEO4284" :total_width 84 :total_depth 42 :kneespace 37 :price 4928 :weight 390}]
                      :option[{:name "013"} {:name "033a"} {:name "034a"} {:name "039"} {:name "D11"} {:name "D30"} {:name "D40"}]}
                     {:name "Gold Tooled Leather Top"
                      :description "Specify leather color: W= Ebony X= Navy, Y= Emerald, Z= Cocoa"
                      :item [{:number "DLT7236" :total_width 72 :total_depth 36       :kneespace 31 :price 5451 :weight 332}
                             {:number "DLT4284" :total_width 84 :total_depth 42       :kneespace 37 :price 6843 :weight 390}]
                      :option[{:name "013"} {:name "033a"} {:name "034a"} {:name "039"} {:name "D11"} {:name "D30"} {:name "D40"}]}
                     {:name "Burl Top"
                      :description "Border Veneer: Quartered Walnut Infield Veneer: Walnut Burl"
                      :item [{:number "DWB7236" :total_width 72 :total_depth 36       :kneespace 31 :price 5758 :weight 332}
                             {:number "DWB4284" :total_width 84 :total_depth 42       :kneespace 37 :price 7646 :weight 390}]
                      :option[{:name "033a"} {:name "034a"} {:name "039"} {:name "D11"} {:name "D30"} {:name "D40"}]}
                     {:name "Crotch Top"
                      :description "Border Veneer: Quartered Mahogany Infield Veneer: Crotch Cedar"
                      :item [{:number "DCC7236" :total_width 72 :total_depth 36 :kneespace 31 :price 5365 :weight 332}
                             {:number "DCC4284" :total_width 84 :total_depth 42 :kneespace 37 :price 7069 :weight 390}]
                      :option[{:name "033a"} {:name "034a"} {:name "039"} {:name "D11"} {:name "D30"} {:name "D40"}]}
                     {:name "Black Diamond Marquetry Top"
                      :description "Quartered Lacewood Border Reverse Diamond Anigre Fields Black Marquetry Inlay"
                      :item [{:number "DGLA7236" :total_width 72 :total_depth 36 :kneespace 31 :price 5372 :weight 332}
                             {:number "DGLA4284" :total_width 84 :total_depth 42 :kneespace 37 :price 7133 :weight 390}]
                      :option[{:name "013"} {:name "033a"} {:name "034a"} {:name "039"} {:name "D11"} {:name "D30"} {:name "D40"}]}
                     {:name "Reverse Diamond Top"
                      :description "Pommele Makore Border Reverse Diamond Cherry Fields Holly Marquetry Inlay"
                      :item [{:number "DGRD7236" :total_width 72 :total_depth 36 :kneespace 31 :price 5796 :weight 332}
                             {:number "DGRD4284" :total_width 84 :total_depth 42 :kneespace 37 :price 7723 :weight 390}]
                      :option[{:name "013"} {:name "033a"} {:name "034a"} {:name "039"} {:name "D11"} {:name "D30"} {:name "D40"}]}
                     {:name "Book Match Top"
                      :description "Quartered Border Veneer Book Matched Infield Veneer T= Walnut, U= Cherry, V= Mahogany"
                      :item [{:number "DBM7236" :total_width 72 :total_depth 36 :kneespace 31 :price 4907 :weight 332}
                             {:number "DBM4284" :total_width 84 :total_depth 42 :kneespace 37 :price 6326 :weight 309}]
                      :option[{:name "013"} {:name "033a"} {:name "034a"} {:name "039"} {:name "D11"} {:name "D30"} {:name "D40"}]}]}

            {:line "Georgetown"
             :category "Desk"
             :name "Medium Profile Double Pedestal Desk"
             :description "Center Drawer, Pencil/Box/File Pedestals. Ball & Claw or Queen Anne legs"
             :variant [{:name "Ball & Claw"
                        :item [{:number "DGBLP3060" :total_width 60 :total_depth 30 :kneespace 25 :price 4608 :weight 270}
                               {:number "DGBLP3666" :total_width 66 :total_depth 36 :kneespace 31 :price 4838 :weight 300}
                               {:number "DGBLP3672" :total_width 72 :total_depth 36 :kneespace 32 :price 5163 :weight 310}]}
                       {:name "Queen Anne"
                        :item [{:number "DGQLP3060" :total_width 60 :total_depth 30 :kneespace 25 :price 4418 :weight 270}
                               {:number "DGQLP3666" :total_width 66 :total_depth 36 :kneespace 31 :price 4647 :weight 300}
                               {:number "DGQLP3672" :total_width 72 :total_depth 36 :kneespace 32 :price 4972 :weight 310}]}]
             :option[{:name "013"} {:name "D30"} {:name "D40"} {:name "039"} {:name "033a"} {:name "034a"} {:name "D11"}]}

            {:line "Georgetown"
             :category "Credenza"
             :name "Medium Profile Writing Credenza"
             :description "Pencil/Box/File Pedestals. Ball & Claw or Queen Anne legs"
             :variant [{:name "Ball & Claw"
                        :item [{:number "DGBLP6091" :total_width 60 :total_depth 19.5 :kneespace 25 :price 3774 :weight 215}
                               {:number "DGBLP6691" :total_width 66 :total_depth 19.5 :kneespace 31 :price 3851 :weight 223}
                               {:number "DGBLP0791" :total_width 70.5 :total_depth 19.5 :kneespace 32 :price 3929 :weight 232}]}
                       {:name "Queen Anne"
                        :item [{:number "DGQLP6091" :total_width 60 :total_depth 19.5 :kneespace 25 :price 3590 :weight 215}
                               {:number "DGQLP6691" :total_width 66 :total_depth 19.5 :kneespace 31 :price 3663 :weight 223}
                               {:number "DGQLP0791" :total_width 70.5 :total_depth 19.5 :kneespace 32 :price 3737 :weight 232}]}]
             :option[{:name "013"} {:name "D30"} {:name "D40"} {:name "039"} {:name "033a"} {:name "034a"} {:name "D11"}]}

            {:line "Georgetown"
             :category "Credenza"
             :name "Medium Profile Combination Credenza"
             :description "Pencil/Box/File Pedestals.Two doors. Ball & Claw or Queen Anne legs"
             :variant [{:name "Ball & Claw"
                        :item [{:number "DGBLP1960" :total_width 60 :total_depth 19.5 :price 4226 :weight 265}
                               {:number "DGBLP1966" :total_width 66 :total_depth 19.5 :price 4312 :weight 273}
                               {:number "DGBLP1970" :total_width 70.5 :total_depth 19.5 :price 4400 :weight 282}]}
                       {:name "Queen Anne"
                        :item [{:number "DGQLP1960" :total_width 60 :total_depth 19.5 :price 4043 :weight 265}
                               {:number "DGQLP1966" :total_width 66 :total_depth 19.5 :price 4126 :weight 273}
                               {:number "DGQLP1970" :total_width 70.5 :total_depth 19.5 :price 4209 :weight 282}]}]
             :option[{:name "013"} {:name "D30"} {:name "D40"} {:name "039"} {:name "033a"} {:name "034a"} {:name "D11"}]}

            {:line "Georgetown"
             :category "Desk"
             :name "High Profile Double Pedestal Desk"
             :description "Pencil/Box/Box Ped., Center Drawer, Pencil File Pedestal. Ball & Claw or Queen Anne legs"
             :variant [{:name "Ball & Claw"
                        :item [{:number "DGBC3060" :total_width 60 :total_depth 30 :kneespace 25 :price 3720 :weight 240}
                               {:number "DGBC3666" :total_width 66 :total_depth 36 :kneespace 31 :price 3890 :weight 270}
                               {:number "DGBC3672" :total_width 72 :total_depth 36 :kneespace 32 :price 4214 :weight 280}]}
                       {:name "Queen Anne"
                        :item [{:number "DGQA3060" :total_width 60 :total_depth 30 :kneespace 25 :price 3629 :weight 240}
                               {:number "DGQA3666" :total_width 66 :total_depth 36 :kneespace 31 :price 3799 :weight 270}
                               {:number "DGQA3672" :total_width 72 :total_depth 36 :kneespace 32 :price 4123 :weight 280}]}]
             :option[{:name "013"} {:name "D30"} {:name "D40"} {:name "039"} {:name "033a"} {:name "034a"} {:name "D11"}]}

            {:line "Georgetown"
             :category "Credenza"
             :name "High Profile Writing Credenza"
             :description "Pencil/Box/Box Ped., Pencil File Pedestal. Ball & Claw or Queen Anne legs"
             :variant [{:name "Ball & Claw"
                        :item [{:number "DGBC6091" :total_width 60 :total_depth 19.5 :kneespace 25 :price 2940 :weight 185}
                               {:number "DGBC6691" :total_width 66 :total_depth 19.5 :kneespace 31 :price 3000 :weight 193}
                               {:number "DGBC0791" :total_width 70.5 :total_depth 19.5 :kneespace 32 :price 3062 :weight 202}]}
                       {:name "Queen Anne"
                        :item [{:number "DGQA6091" :total_width 60 :total_depth 19.5 :kneespace 25 :price 2853 :weight 185}
                               {:number "DGQA6691" :total_width 66 :total_depth 19.5 :kneespace 31 :price 2912 :weight 193}
                               {:number "DGQA0791" :total_width 70.5 :total_depth 19.5 :kneespace 32 :price 2971 :weight 202}]}]
             :option[{:name "013"} {:name "D30"} {:name "D40"} {:name "039"} {:name "033a"} {:name "034a"} {:name "D11"}]}

            {:line "Georgetown"
             :category "Credenza"
             :name "High Profile Combination Credenza"
             :description "Pencil/Box/Box Ped., Two doors, Pencil File Pedestal. Ball & Claw or Queen Anne legs"
             :variant [{:name "Ball & Claw"
                        :item [{:number "DGBC1960" :total_width 60 :total_depth 19.5 :price 3199 :weight 235}
                               {:number "DGBC1966" :total_width 66 :total_depth 19.5 :price 3265 :weight 243}
                               {:number "DGBC1970" :total_width 70.5 :total_depth 19.5 :price 3331 :weight 252}]}
                       {:name "Queen Anne"
                        :item [{:number "DGQA1960" :total_width 60 :total_depth 19.5 :price 3111 :weight 252}
                               {:number "DGQA1966" :total_width 66 :total_depth 19.5 :price 3175 :weight 243}
                               {:number "DGQA1970" :total_width 70.5 :total_depth 19.5 :price 3240 :weight 252}]}]
             :option[{:name "013"} {:name "D30"} {:name "D40"} {:name "039"} {:name "033a"} {:name "034a"} {:name "D11"}]}

            {:line "Georgetown"
             :category "Desk"
             :name "Exec. Return Desk"
             :description "A - Desk with pencil/box/file pedestal, pullout writing shelf, and center drawer. B - 29.5\" high return with pencil/box/file pedestal. Secretarial height return available at same price Specify: Return left or right."
             :style [{:number "DGEL7860"
                      :price 5540
                      :weight 353
                      :item [{:part "Desk" :total_width 60 :total_depth 30}
                             {:part "Return" :total_width 48 :total_depth 19.5}]}
                     {:number "DGEL8466"
                      :price 5852
                      :weight 388
                      :item [{:part "Desk" :total_width 66 :total_depth 36}
                             {:part "Return" :total_width 48 :total_depth 19.5}]}
                     {:number "DGEL8472"
                      :price 6012
                      :weight 397
                      :item [{:part "Desk" :total_width 72 :total_depth 36}
                             {:part "Return" :total_width 48 :total_depth 24}]}]
             :option[{:name "013"} {:name "D31"} {:name "D41"} {:name "030"} {:name "031"} {:name "032a"} {:name "033a"} {:name "034a"} {:name "039"}]}

            {:line "Georgetown"
             :category "Desk"
             :name "Computer Desk"
             :description "A - 29.5' high return with boxl/box/file pedestal. B - Computer corner with keyboard drawer and grommet."
             :style [{:number "DGCC9090L"
                      :price 6034
                      :weight 407
                      :item [{:part "Return" :total_width 48 :total_depth 19.5}
                             {:part "Corner" :total_width 41 :total_depth 41}]}]
             :option[{:name "D32"} {:name "D44"} {:name "D11"}]}

            {:line "Georgetown"
             :category "Workstation"
             :name "Exec. U Workstation"
             :description "A - Desk with pencil/box/file pedestal, pullout writing shelf, and center drawer. B - 29.5\" high bridge. Sec. ht. same price. C - Credenza with B/B/F ped., 2 doors, and open bookshelf. Specify: Return left or right."
             :style [{:number "DGEU9860" :price 8317 :weight 556
                      :item [{:part "Desk" :total_width 60 :total_depth 30}
                             {:part "Bridge" :total_width 48 :total_depth 19.5}
                             {:part "Credenza" :total_width 60 :total_depth 19.5}]}
                     {:number "DGEU10466" :price 8614 :weight 597
                      :item [{:part "Desk" :total_width 66 :total_depth 36}
                             {:part "Bridge" :total_width 48 :total_depth 19.5}
                             {:part "Credenza" :total_width 66 :total_depth 19.5}]}
                     {:number "DGEU10472" :price 8749 :weight 626
                      :item [{:part "Desk" :total_width 72 :total_depth 36}
                             {:part "Bridge" :total_width 48 :total_depth 24}
                             {:part "Credenza" :total_width 70.5 :total_depth 19.5}]}]
             :option[{:name "013"} {:name "D34"} {:name "D42"} {:name "012"} {:name "010"} {:name "030"} {:name "031"} {:name "032a"}]}

            {:line "Georgetown"
             :category "Workstation"
             :name "Bullet End Desk Exec. U Workstation"
             :description "A - Bullet End Desk B - 29.5\" high bridge Secretarial ht. bridge available at same price. C - Credenza with B/B/F ped., 2 doors, and open bookshelf. Specify: Return left or right."
             :style [{:number "DGBU10372" :price 8027 :weight 483
                      :item [{:part "Desk" :total_width 72 :total_depth 36}
                             {:part "Bridge" :total_width 48 :total_depth 24}
                             {:part "Credenza" :total_width 70.5 :total_depth 19.5}]}
                     {:number "DGBU10384" :price 8874 :weight 516
                      :item [{:part "Desk" :total_width 84 :total_depth 36}
                             {:part "Bridge" :total_width 48 :total_depth 24}
                             {:part "Credenza" :total_width 84 :total_depth 19.5}]}]
             :option[{:name "D39"} {:name "013"} {:name "D31"} {:name "D43"} {:name "010"} {:name "012"} {:name "030"} {:name "031"} {:name "032a"}]}

            {:line "Georgetown"
             :category "Credenza"
             :name "Cabinet Credenza"
             :description "4 doors"
             :variant [{:name "Straight Grain Wood Top"
                        :item [{:number "DGED1960" :total_width 60 :total_depth 19.5 :shelf 2 :price 2850 :weight 214}
                               {:number "DGED2460" :total_width 60 :total_depth 24 :shelf 2 :price 3192 :weight 249}
                               {:number "DGED1970" :total_width 70.5 :total_depth 19.5 :shelf 2 :price 3038 :weight 244}
                               {:number "DGED2470" :total_width 70.5 :total_depth 24 :shelf 2 :price 3403 :weight 279}]
                        :option[{:name "010"} {:name "D30"} {:name "D44"}]}
                       {:name "Burl Top"
                        :description "Border Veneer: Quartered Walnut Infield Veneer: Walnut Burl"
                        :item [{:number "DWB1960" :total_width 60 :total_depth 19.5 :shelf 2 :price 3972 :weight 214}
                               {:number "DWB2460" :total_width 60 :total_depth 24 :shelf 2 :price 4449 :weight 249}
                               {:number "DWB1970" :total_width 70.5 :total_depth 19.5 :shelf 2 :price 4223 :weight 244}
                               {:number "DWB2470" :total_width 70.5 :total_depth 24 :shelf 2 :price 4729 :weight 279}]
                        :option[{:name "010"} {:name "D30"} {:name "D44"}]}
                       {:name "Crotch Top"
                        :description "Border Veneer: Quartered Mahogany Infield Veneer: Crotch Cedar"
                        :item [{:number "DCC1960" :total_width 60 :total_depth 19.5 :shelf 2 :price 3800 :weight 214}
                               {:number "DCC2460" :total_width 60 :total_depth 24 :shelf 2 :price 4256 :weight 249}
                               {:number "DCC1970" :total_width 70.5 :total_depth 19.5 :shelf 2 :price 4024 :weight 244}
                               {:number "DCC2470" :total_width 70.5 :total_depth 24 :shelf 2 :price 4506 :weight 279}]
                        :option[{:name "010"} {:name "D30"} {:name "D44"}]}
                       {:name "Black Diamond Marquetry Top"
                        :description "Quartered Lacewood Border Reverse Diamond Anigre Fields Black Marquetry Inlay"
                        :item [{:number "DGLA1960" :total_width 60 :total_depth 19.5 :shelf 2 :price 3655 :weight 214}
                               {:number "DGLA2460" :total_width 60 :total_depth 24 :shelf 2 :price 4094 :weight 249}
                               {:number "DGLA1970" :total_width 70.5 :total_depth 19.5 :shelf 2 :price 3885 :weight 244}
                               {:number "DGLA2470" :total_width 70.5 :total_depth 24 :shelf 2 :price 4353 :weight 279}]
                        :option[{:name "010"} {:name "D30"} {:name "D44"}]}
                       {:name "Book Match Top"
                        :description "Quartered Border; Book Matched Infield T= Walnut, U= Cherry, V= Mahogany"
                        :item [{:number "DBM1960" :total_width 60 :total_depth 19.5 :shelf 2 :price 3488 :weight 214}
                               {:number "DBM2460" :total_width 60 :total_depth 24 :shelf 2 :price 3907 :weight 249}
                               {:number "DBM1970" :total_width 70.5 :total_depth 19.5 :shelf 2 :price 3712 :weight 244}
                               {:number "DBM2470" :total_width 70.5 :total_depth 24 :shelf 2 :price 4158 :weight 279}]
                        :option[{:name "010"} {:name "D30"} {:name "D44"}]}]}

            {:line "Georgetown"
             :category "Credenza"
             :name "Combination Credenza"
             :description "2 doors and 2 box/box/file pedestals."
             :variant [{:name "Straight Grain Wood Top"
                        :item [{:number "DGEC1970" :total_width 70.5 :total_depth 19.5 :shelf 1 :price 3399 :weight 308}
                               {:number "DGEC2470" :total_width 70.5 :total_depth 24 :shelf 1 :price 3807 :weight 343}]
                        :option[{:name "010"} {:name "012"} {:name "D11"} {:name "D30"} {:name "D44"}]}
                       {:name "Burl Top"
                        :description "Border Veneer: Quartered Walnut Infield Veneer: Walnut Burl"
                        :item [{:number "DWB7019" :total_width 70.5 :total_depth 19.5 :shelf 1 :price 4590 :weight 308}
                               {:number "DWB7024" :total_width 70.5 :total_depth 24 :shelf 1 :price 5140 :weight 343}]
                        :option[{:name "010"} {:name "012"} {:name "D11"} {:name "D30"} {:name "D44"}]}
                       {:name "Crotch Top"
                        :description "Border Veneer: Quartered Mahogany Infield Veneer: Crotch Cedar"
                        :item [{:number "DCC7019" :total_width 70.5 :total_depth 19.5 :shelf 1 :price 4389 :weight 308}
                               {:number "DCC7024" :total_width 70.5 :total_depth 24 :shelf 1 :price 4916 :weight 343}]
                        :option[{:name "010"} {:name "012"} {:name "D11"} {:name "D30"} {:name "D44"}]}
                       {:name "Black Diamond Marquetry Top"
                        :description "Quartered Lacewood Border Reverse Diamond Anigre Fields Black Marquetry Inlay"
                        :item [{:number "DGLA7019" :total_width 70.5 :total_depth 19.5 :shelf 1 :price 4223 :weight 308}
                               {:number "DGLA7024" :total_width 70.5 :total_depth 24 :shelf 1 :price 4729 :weight 343}]
                        :option[{:name "010"} {:name "012"} {:name "D11"} {:name "D30"} {:name "D44"}]}
                       {:name "Book Match Top"
                        :description "Quartered Border; Book Matched Infield T= Walnut, U= Cherry, V= Mahogany"
                        :item [{:number "DBM7019" :total_width 70.5 :total_depth 19.5 :shelf 1 :price 4078 :weight 308}
                               {:number "DBM7024" :total_width 70.5 :total_depth 24 :shelf 1 :price 4568 :weight 343}]
                        :option[{:name "010"} {:name "012"} {:name "D11"} {:name "D30"} {:name "D44"}]}]}

            {:line "Georgetown"
             :category "Credenza"
             :name "Writing Credenza"
             :description "with box/box/file pedestals"
             :variant [{:name "Straight Grain Wood Top"
                        :item [{:number "DGEK1970" :total_width 70.5 :total_depth 19.5 :kneespace 35 :price 3124 :weight 246}
                               {:number "DGEK2470" :total_width 70.5 :total_depth 24 :kneespace 35 :price 3498 :weight 281}]
                        :option[{:name "012"} {:name "D11"} {:name "D32"} {:name "D44"}]}
                       {:name "Burl Top"
                        :description "Border Veneer: Quartered Walnut Infield Veneer: Walnut Bur"
                        :item [{:number "DWB0791" :total_width 70.5 :total_depth 19.5 :kneespace 35 :price 4310 :weight 246}
                               {:number "DWB0724" :total_width 70.5 :total_depth 24 :kneespace 35 :price 4828 :weight 281}]
                        :option[{:name "012"} {:name "D11"} {:name "D32"} {:name "D44"}]}
                       {:name "Crotch Top"
                        :description "Border Veneer: Quartered Mahogany Infield Veneer: Crotch Cedar"
                        :item [{:number "DCC0791" :total_width 70.5 :total_depth 19.5 :kneespace 35 :price 4111 :weight 246}
                               {:number "DCC0724" :total_width 70.5 :total_depth 24 :kneespace 35 :price 4605 :weight 281}]
                        :option[{:name "012"} {:name "D11"} {:name "D32"} {:name "D44"}]}
                       {:name "Black Diamond Marquetry Top"
                        :description "Quartered Lacewood Border Reverse Diamond Anigre Fields Black Marquetry Inlay"
                        :item [{:number "DGLA0791" :total_width 70.5 :total_depth 19.5 :kneespace 35 :price 3965 :weight 246}
                               {:number "DGLA0724" :total_width 70.5 :total_depth 24 :kneespace 35 :price 4441 :weight 281}]
                        :option[{:name "012"} {:name "D11"} {:name "D32"} {:name "D44"}]}
                       {:name "Book Match Top"
                        :description "Quartered Border; Book Matched Infield T= Walnut, U= Cherry, V= Mahogany"
                        :item [{:number "DBM0791" :total_width 70.5 :total_depth 19.5 :kneespace 35 :price 3800 :weight 246}
                               {:number "DBM0724" :total_width 70.5 :total_depth 24 :kneespace 35 :price 4256 :weight 281}]
                        :option[{:name "012"} {:name "D11"} {:name "D32"} {:name "D44"}]}]}

            {:line "Georgetown"
             :category "Credenza"
             :name "Cabinet Credenza"
             :description "2 doors. Straight grain top."
             :item [{:number "DGED1930" :total_width 30 :total_depth 19.5 :price 1868 :weight 117}
                    {:number "DGED1936" :total_width 36 :total_depth 19.5 :price 1945 :weight 132}
                    {:number "DGED2430" :total_width 30 :total_depth 24 :price 2091 :weight 132}
                    {:number "DGED2436" :total_width 36 :total_depth 24 :price 2179 :weight 147}]
             :option[{:name "010"} {:name "D30"} {:name "D44"}]}

            {:line "Georgetown"
             :category "Credenza"
             :name "Refrigerator"
             :description "2 doors. Straight grain top."
             :item [{:number "DGRF1938" :total_width 38.5 :total_depth 19.5 :price 4323 :weight 185}]
             :option[{:name "D30"} {:name "D44"}]}

            {:line "Georgetown"
             :category "Part"
             :name "Credenza Tops"
             :description "1.25\" high"
             :item [{:number "DGTV1918" :total_width 17.75 :total_depth 19.5 :price 357 :weight 20}
                    {:number "DGTV1934" :total_width 33.5 :total_depth 19.5 :price 431 :weight 33}
                    {:number "DGTC1938" :total_width 38 :total_depth 19.5 :price 456 :weight 36}
                    {:number "DGTV1949" :total_width 49.25 :total_depth 19.5 :price 596 :weight 46}
                    {:number "DGTV1965" :total_width 65 :total_depth 19.5 :price 621 :weight 56}
                    {:number "DGTC1974" :total_width 74 :total_depth 19.5 :price 719 :weight 63}
                    {:number "DGTC19110" :total_width 110 :total_depth 19.5 :price 1147 :weight 91}]}

            {:line "Georgetown"
             :category "Part"
             :name "Box/box/file Pedestal"
             :description "25\" high. For use with DGTV Tops and DGBV Bases"
             :item [{:number "DGCB1715L" :total_width 15.75 :total_depth 17.625 :price 1246 :weight 95}
                    {:number "DGCB1715R" :total_width 15.75 :total_depth 17.625 :price 1246 :weight 95}
                    {:number "DGCB1715X" :total_width 15.75 :total_depth 17.625 :price 1302 :weight 95}
                    {:number "DGCB1715O" :total_width 15.75 :total_depth 17.625 :price 1190 :weight 95}]
             :option[{:name "012"} {:name "D11"}]}

            {:line "Georgetown"
             :category "Part"
             :name "File/File Pedestal"
             :description "25\" high. For use with DGTV Tops and DGBV Bases."
             :item [{:number "DGCF1715L" :total_width 15.75 :total_depth 17.625 :price 1192 :weight 90}
                    {:number "DGCF1715R" :total_width 15.75 :total_depth 17.625 :price 1192 :weight 90}
                    {:number "DGCF1715X" :total_width 15.75 :total_depth 17.625 :price 1247 :weight 90}
                    {:number "DGCF1715O" :total_width 15.75 :total_depth 17.625 :price 1134 :weight 90}]
             :option[{:name "012"} {:name "D11"}]}

            {:line "Georgetown"
             :category "Part"
             :name "Single Door Pedestal"
             :description "25\" high. For use with DGTV Tops and DGBV Bases."
             :item [{:number "DGCD1715L" :total_width 15.75 :total_depth 17.625 :price 1003 :weight 67}
                    {:number "DGCD1715R" :total_width 15.75 :total_depth 17.625 :price 1003 :weight 67}
                    {:number "DGCD1715X" :total_width 15.75 :total_depth 17.625 :price 1056 :weight 67}
                    {:number "DGCD1715O" :total_width 15.75 :total_depth 17.625 :price 945 :weight 67}]
             :option[{:name "011"}]}

            {:line "Georgetown"
             :category "Part"
             :name "Double Door Pedestal"
             :description "25\" high. For use with DGTC Tops and DGBC Bases"
             :item [{:number "DGCD1736L" :total_width 36 :total_depth 17.625 :price 1115 :weight 106}
                    {:number "DGCD1736R" :total_width 36 :total_depth 17.625 :price 1115 :weight 106}
                    {:number "DGCD1736X" :total_width 36 :total_depth 17.625 :price 1173 :weight 106}
                    {:number "DGCD1736O" :total_width 36 :total_depth 17.625 :price 1055 :weight 106}]
             :option[{:name "010"}]}

            {:line "Georgetown"
             :category "Part"
             :name "Lateral File Pedestal"
             :description "25\" high. For use with DGTC Tops and DGBC Bases."
             :item [{:number "DGLF1736L" :total_width 36 :total_depth 17.625 :price 1321 :weight 139}
                    {:number "DGLF1736R" :total_width 36 :total_depth 17.625 :price 1321 :weight 139}
                    {:number "DGLF1736X" :total_width 36 :total_depth 17.625 :price 1385 :weight 139}
                    {:number "DGLF1736O" :total_width 36 :total_depth 17.625 :price 1270 :weight 139}]
             :option[{:name "012"} {:name "D11"}]}

            {:line "Georgetown"
             :category "Part"
             :name "Credenza Base"
             :description "3.25\" high"
             :item [{:number "DGBV1917" :total_width 17.25 :total_depth 19.125 :price 302 :weight 17}
                    {:number "DGBV1933" :total_width 33 :total_depth 19.125 :price 356 :weight 25}
                    {:number "DGBC1937" :total_width 37.5 :total_depth 19.125 :price 392 :weight 28}
                    {:number "DGBV1949" :total_width 48.75 :total_depth 19.125 :price 454 :weight 34}
                    {:number "DGBV1964" :total_width 64.5 :total_depth 19.125 :price 534 :weight 42}
                    {:number "DGBC1973" :total_width 73.5 :total_depth 19.125 :price 600 :weight 48}
                    {:number "DGBC19110" :total_width 109.5 :total_depth 19.125 :price 919 :weight 68}]}

            {:line "Georgetown"
             :category "Part"
             :name "Cornice Tops"
             :description "4\" high"
             :item [{:number "DGCC1438" :total_width 38 :total_depth 14 :price 484 :weight 25}
                    {:number "DGCC1474" :total_width 74 :total_depth 14 :price 719 :weight 42}
                    {:number "DGCC14110" :total_width 110 :total_depth 14 :price 1144 :weight 62}]}

            {:line "Georgetown"
             :category "Hutch"
             :name "Open Hutch with Shelves"
             :description "44\" high."
             :item [{:number "DGOC1336" :total_width 36 :total_depth 13 :price 959 :weight 88}]
             :option[{:name "060"}]}

            {:line "Georgetown"
             :category "Hutch"
             :name "Hutch with Wire Mesh Doors"
             :description "44\" high."
             :item [{:number "DGMC1336" :total_width 36 :total_depth 13 :price 1743 :weight 104}]
             :option[{:name "060"}]}

            {:line "Georgetown"
             :category "Hutch"
             :name "Hutch with Wood Doors"
             :description "44\" high"
             :item [{:number "DGSC1313" :total_width 36 :total_depth 13 :price 1406 :weight 121}]
             :option[{:name "060"}]}

            {:line "Georgetown"
             :category "Hutch"
             :name "Hutch Bookcase Base"
             :description "3.25\" high."
             :item [{:number "DGBB1337" :total_width 37.5 :total_depth 13.75 :price 225 :weight 24}
                    {:number "DGBB1373" :total_width 73.5 :total_depth 13.75 :price 413 :weight 40}
                    {:number "DGBB1310" :total_width 109.5 :total_depth 13.75 :price 615 :weight 56}]}

            {:line "Georgetown"
             :category "Cabinet"
             :name "Flipper Door Cabinet"
             :description "18\" high"
             :item [{:number "DGBB1530" :total_width 30 :total_depth 15 :price 1249 :weight 69}
                    {:number "DGBB1536" :total_width 36 :total_depth 15 :price 1287 :weight 79}]
             :option[{:name "011"} {:name "070"}]}

            {:line "Georgetown"
             :category "Hutch"
             :name "Hutch with Flipper Doors"
             :description "36\" high."
             :item [{:number "DGBB1560" :total_width 60 :total_depth 15 :price 2655 :weight 148}
                    {:number "DGBB1570" :total_width 70.5 :total_depth 15 :price 2828 :weight 160}]
             :option[{:name "D37"} {:name "011"} {:name "070"} {:name "071"} {:name "072"}]}

            {:line "Georgetown"
             :category "Hutch"
             :name "Hutch with Wire Mesh Doors"
             :description "36\" high"
             :item [{:number "DGHM1460" :total_width 60 :total_depth 14 :price 2849 :weight 143}
                    {:number "DGHM1466" :total_width 66 :total_depth 14 :price 2929 :weight 152}
                    {:number "DGHM1470" :total_width 70.5 :total_depth 14 :price 3013 :weight 161}]
             :option[{:name "D37"} {:name "070"} {:name "071"} {:name "072"}]}

            {:line "Georgetown"
             :category "Hutch"
             :name "Hutch with Two Hinged Doors"
             :description "and two adjustable shelves. 48\" high"
             :item [{:number "DGHB1458" :total_width 60 :total_depth 14 :price 2669 :weight 194}
                    {:number "DGHB1464" :total_width 66 :total_depth 14 :price 2811 :weight 200}
                    {:number "DGHB1468" :total_width 70.5 :total_depth 14 :price 2900 :weight 207}]
             :option[{:name "011"} {:name "D06"} {:name "D09"}]}

            {:line "Georgetown"
             :category "Cabinet"
             :name "Wardrobe/Closet"
             :description "with two full doors. 72\" high. 19.5\" deep unit has pullout clothing carrier. 24\" deep unit is half shelves and half wardrobe"
             :item [{:number "DTGW361" :total_width 36 :total_depth 19.5 :price 2900 :weight 206}
                    {:number "DTGW364" :total_width 36 :total_depth 24 :price 3529 :weight 232}]
             :option[{:name "010"} {:name "D31"}]}

            {:line "Georgetown"
             :category "Cabinet"
             :name "Storage Closet with File Drawers"
             :description "72\" high."
             :item [{:number "DGFD361" :total_width 36 :total_depth 19.5 :shelf 2 :price 4097 :weight 267}
                    {:number "DGFD364" :total_width 36 :total_depth 24 :shelf 2 :price 4590 :weight 324}]
             :option[{:name "010"} {:name "012"} {:name "D31"} {:name "D11"}]}

            {:line "Georgetown"
             :category "Bookcase"
             :name "Enclosed Bookcase"
             :description "Two glass doors on top; two wood doors below."
             :item [{:number "DTGB326" :total_width 32 :total_depth 16 :total_height 60 :shelf 2 :price 3772 :weight 163}
                    {:number "DTGB386" :total_width 38 :total_depth 16 :total_height 60 :shelf 2 :price 3933 :weight 184}
                    {:number "DTGB327" :total_width 32 :total_depth 16 :total_height 72 :shelf 3 :price 3965 :weight 181}
                    {:number "DTGB387" :total_width 38 :total_depth 16 :total_height 72 :shelf 3 :price 4090 :weight 198}]
             :option[{:name "010"}]}

            {:line "Georgetown"
             :category "Credenza"
             :name "File Credenza"
             :description "2 lateral file drawers."
             :item [{:number "DGLF1936" :total_width 36 :total_depth 19.5 :price 1982 :weight 163}]
             :option[{:name "012"} {:name "D32"} {:name "D44"} {:name "D11"}]}

            {:line "Georgetown"
             :category "Credenza"
             :name "File Credenza"
             :description "3 lateral file drawers. 41.25\" high"
             :item [{:number "DGLF1941" :total_width 36 :total_depth 19.5 :price 2915 :weight 216}]
             :option[{:name "012"} {:name "D32"} {:name "D44"} {:name "D11"}]}

            {:line "Georgetown"
             :category "Credenza"
             :name "File Credenza"
             :description "4 lateral file drawers."
             :item [{:number "DGLF1970" :total_width 70.5 :total_depth 19.5 :price 3428 :weight 261}]
             :option[{:name "012"} {:name "D32"} {:name "D44"} {:name "D11"}]}

            {:line "Georgetown"
             :category "Bookcase"
             :name "Open Bookcase Vertical"
             :description "13\" deep."
             :item [{:number "DTOP302" :total_height 30 :total_width 32 :shelf 1 :price 1165 :weight 75}
                    {:number "DTOP362" :total_height 36 :total_width 32 :shelf 1 :price 1251 :weight 80}
                    {:number "DTOP482" :total_height 48 :total_width 32 :shelf 2 :price 1513 :weight 98}
                    {:number "DTOP602" :total_height 60 :total_width 32 :shelf 3 :price 1634 :weight 118}
                    {:number "DTOP722" :total_height 72 :total_width 32 :shelf 4 :price 1792 :weight 136}
                    {:number "DTOP842" :total_height 84 :total_width 32 :shelf 5 :price 1913 :weight 156}
                    {:number "DTOP962" :total_height 96 :total_width 32 :shelf 6 :price 2000 :weight 174}
                    {:number "DTOP30" :total_height 30 :total_width 37 :shelf 1 :price 1217 :weight 83}
                    {:number "DTOP36" :total_height 36 :total_width 37 :shelf 1 :price 1338 :weight 89}
                    {:number "DTOP48" :total_height 48 :total_width 37 :shelf 2 :price 1566 :weight 109}
                    {:number "DTOP608" :total_height 60 :total_width 37 :shelf 3 :price 1721 :weight 131}
                    {:number "DTOP728" :total_height 72 :total_width 37 :shelf 4 :price 1896 :weight 151}
                    {:number "DTOP848" :total_height 84 :total_width 37 :shelf 5 :price 2051 :weight 173}
                    {:number "DTOP968" :total_height 96 :total_width 37 :shelf 6 :price 2121 :weight 191}]
             :option[{:name "D05"} {:name "D04"} {:name "010"} {:name "060"}]}

            {:line "Georgetown"
             :category "Bookcase"
             :name "Open Bookcase Horizontal"
             :description "13\" deep."
             :item [{:number "DTSB305" :total_height 30 :total_width 50 :shelf 2 :price 1722 :weight 110}
                    {:number "DTSB365" :total_height 36 :total_width 50 :shelf 2 :price 1822 :weight 117}
                    {:number "DTSB425" :total_height 42 :total_width 50 :shelf 4 :price 1890 :weight 136}
                    {:number "DTSB485" :total_height 48 :total_width 50 :shelf 4 :price 1959 :weight 143}
                    {:number "DTSB307" :total_height 30 :total_width 70.5 :shelf 2 :price 2046 :weight 138}
                    {:number "DTSB367" :total_height 36 :total_width 70.5 :shelf 2 :price 2250 :weight 145}
                    {:number "DTSB427" :total_height 42 :total_width 70.5 :shelf 4 :price 2319 :weight 170}
                    {:number "DTSB487" :total_height 48 :total_width 70.5 :shelf 4 :price 2395 :weight 177}]
             :option[{:name "D05"} {:name "060"}]}

            {:line "Georgetown"
             :category "Table"
             :name "Corner Computer Table"
             :description "with keyboard drawer and grommet. 19.5\" deep on each end"
             :item [{:number "DGCC4242" :total_width 42 :total_depth 42 :price 2745 :weight 156}]
             :option[{:name "033a"} {:name "034a"} {:name "036"} {:name "039"}]}

            {:line "Georgetown"
             :category "Table"
             :name "Computer Table"
             :description "with keyboard drawer and grommet."
             :item [{:number "DGCT1948" :total_width 48 :total_depth 19.5 :price 1894 :weight 110}
                    {:number "DGCT4824" :total_width 48 :total_depth 24 :price 2015 :weight 127}]
             :option[{:name "D32"} {:name "D44"} {:name "033a"} {:name "034a"} {:name "036"} {:name "039"}]}

            {:line "Georgetown"
             :category "Table"
             :name "Printer Table"
             :description "with paper feed slot and casters"
             :item [{:number "DGPS2448" :total_width 48 :total_depth 24 :price 1851 :weight 122}]
             :option[{:name "D32"} {:name "D44"} {:name "033a"} {:name "034a"} {:name "036"} {:name "039"}]}

            {:line "Georgetown"
             :category "Table"
             :name "Mobile Typing Table"
             :description "with grommet and casters."
             :item [{:number "DGTT1948" :total_width 48 :total_depth 19.5 :price 1602 :weight 100}
                    {:number "DGTT2448" :total_width 48 :total_depth 24 :price 1764 :weight 117}]
             :option[{:name "D32"} {:name "D44"} {:name "033a"} {:name "034a"} {:name "036"} {:name "039"}]}

            {:line "Georgetown"
             :category "Table"
             :name "Lamp/End Table"
             :description "24\" high."
             :item [{:number "DGLT1919" :total_width 19.5 :total_depth 19.5 :price 1079 :weight 64}
                    {:number "DGLT2424" :total_width 24 :total_depth 24 :price 1157 :weight 80}]
             :option[{:name "D35"} {:name "D21"}]}

            {:line "Georgetown"
             :category "Table"
             :name "Telephone Table"
             :description "27\" high."
             :item [{:number "DTE1919" :total_width 19.5 :total_depth 19.5 :price 1218 :weight 80}]
             :option[{:name "D36"} {:name "D44"}]}

            {:line "Covington"
             :category "Desk"
             :name "Double Pedestal Desk Top overhang"
             :description "Pencil/box/file pedestals. Center drawer. Full modesty panel."
             :item [{:number "DCDO3672" :total_width 72 :total_depth 36 :price 3105 :weight 335}
                    {:number "DCDO4278" :total_width 78 :total_depth 42 :price 3804 :weight 378}]
             :option[{:name "013"} {:name "033a"} {:name "034a"} {:name "039"} {:name "D25"}]}

            {:line "Covington"
             :category "Desk"
             :name "Double Pedestal Desk Flush top"
             :description "Pencil/box/file pedestals. Center drawer. Full modesty panel"
             :item [{:number "DCDF3060" :total_width 60 :total_depth 30 :price 3022 :weight 292}
                    {:number "DCDF3066" :total_width 66 :total_depth 30 :price 3130 :weight 328}
                    {:number "DCDF3672" :total_width 72 :total_depth 36 :price 3349 :weight 340}]
             :option[{:name "013"} {:name "033a"} {:name "034a"} {:name "039"} {:name "D25"}]}

            {:line "Covington"
             :category "Desk"
             :name "Single Pedestal Desk"
             :description "Top overhang. Pencil/box/file pedestal. Center drawer. Full modesty panel. For use with returns or bridges shown below. Specify left or right return."
             :item [{:number "DCDS3060" :total_width 60 :total_depth 30 :price 2695 :weight 200}
                    {:number "DCDS3066" :total_width 66 :total_depth 30 :price 2794 :weight 233}
                    {:number "DCDS3672" :total_width 72 :total_depth 36 :price 2902 :weight 245}]
             :option[{:name "013"} {:name "033a"} {:name "034a"} {:name "039"} {:name "D25"}]}

            {:line "Covington"
             :category "Return"
             :name "Executive Return"
             :description "Pencil/box/file pedestal. Full modesty panel. For use with single pedestal desks shown above, or computer corner on page 353. Specify left or right return"
             :item [{:number "DCPR1948" :total_width 48 :total_depth 19.5 :price 1770 :weight 165}
                    {:number "DCPR2448" :total_width 48 :total_depth 24 :price 1863 :weight 172}]
             :option[{:name "030"} {:name "031"} {:name "032a"} {:name "033a"} {:name "034a"} {:name "039"} {:name "D26"}]}

            {:line "Covington"
             :category "Bridge"
             :name "Executive Bridge"
             :item [{:number "DCBR1948" :total_width 48 :total_depth 19.5 :price 1073 :weight 80}
                    {:number "DCBR2448" :total_width 48 :total_depth 24 :price 1137 :weight 90}]
             :option[{:name "030"} {:name "031"} {:name "032"} {:name "033a"} {:name "034a"} {:name "039"} {:name "D26"}]}

            {:line "Covington"
             :category "Credenza"
             :name "Credenza"
             :description "Two doors and pencil/box/file pedestal"
             :item [{:number "DCDP1970" :total_width 70.5 :total_depth 19.5 :price 2692 :weight 265}]
             :option[{:name "010"} {:name "012"} {:name "D26"}]}

            {:line "Covington"
             :category "Credenza"
             :name "Credenza"
             :description "Pencil/box/file pedestal."
             :item [{:number "DCPC1960" :total_width 60 :total_depth 19.5 :price 2237 :weight 150}
                    {:number "DCPC1966" :total_width 66 :total_depth 19.5 :price 2330 :weight 170}
                    {:number "DCPC1970" :total_width 70.5 :total_depth 19.5 :price 2378 :weight 190}]
             :option[{:name "010"} {:name "012"} {:name "D26"}]}

            {:line "Covington"
             :category "Credenza"
             :name "File Credenza"
             :description "Two lateral file drawers"
             :item [{:number "DCLC1960" :total_width 60 :total_depth 19.5 :price 2573 :weight 200}
                    {:number "DCLC1966" :total_width 66 :total_depth 19.5 :price 2649 :weight 235}
                    {:number "DCLC1970" :total_width 70.5 :total_depth 19.5 :price 2728 :weight 250}]
             :option[{:name "010"} {:name "012"} {:name "D26"}]}

            {:line "Covington"
             :category "Credenza"
             :name "Combination Credenza"
             :description "Two doors and two pencil/box/file pedestals."
             :item [{:number "DCCC1970" :total_width 70.5 :total_depth 19.5 :price 2778 :weight 298}]
             :option[{:name "010"} {:name "012"} {:name "D26"}]}

            {:line "Covington"
             :category "Credenza"
             :name "Writing Credenza"
             :description "Two pencil/box/file pedestals. Modesty panel."
             :item [{:number "DCKC1970" :total_width 70.5 :total_depth 19.5 :price 2632 :weight 235}]
             :option[{:name "010"} {:name "012"} {:name "D26"}]}

            {:line "Covington"
             :category "Credenza"
             :name "File Credenza"
             :description "Four lateral file drawers."
             :item [{:number "DCLF1970" :total_width 70.5 :total_depth 19.5 :price 2968 :weight 255}]
             :option[{:name "012"} {:name "D26"}]}

            {:line "Covington"
             :category "Credenza"
             :name "File Credenza"
             :description "Two lateral file drawers."
             :item [{:number "DCLF1936" :total_width 36 :total_depth 19.5 :price 1767 :weight 158}]
             :option[{:name "012"} {:name "D26"}]}

            {:line "Covington"
             :category "Table"
             :name "Computer Table"
             :description "with keyboard drawer and grommet."
             :item [{:number "DCTC2448" :total_width 48 :total_depth 24 :price 1687 :weight 124}]
             :option[{:name "D26"}]}

            {:line "Covington"
             :category "Table"
             :name "Corner Computer Table"
             :description "with keyboard drawer and grommet. For 19.5” returns."
             :item [{:number "DCC1941" :total_width 41 :total_depth 41 :price 2333 :weight 151}]}

            {:line "Covington"
             :category "Table"
             :name "Corner Computer Table"
             :description "with articulating arm and grommet. For 24” returns."
             :item [{:number "DCC2441" :total_width 41 :total_depth 41 :price 2496 :weight 160}]}

            {:line "Covington"
             :category "Hutch"
             :name "Hutch"
             :description "with two doors. 14\" deep."
             :item [{:number "DCOH7048" :total_width 70.5 :total_depth 48 :price 2875 :weight 200}]
             :option[{:name "D28"}]}

            {:line "Covington"
             :category "Bookcase"
             :name "Open Bookcase"
             :description "13\" deep"
             :item [{:number "DCOB3036" :total_width 30 :total_depth 36 :price 1066 :weight 80}
                    {:number "DCOB3048" :total_width 30 :total_depth 48 :price 1246 :weight 98}
                    {:number "DCOB3072" :total_width 30 :total_depth 72 :price 1624 :weight 136}]}

            {:line "Covington"
             :category "Bookcase"
             :name "Bookcase"
             :description "with wire mesh doors. 13\" deep."
             :item [{:number "DCMB3036" :total_width 30 :total_depth 36 :price 1718 :weight 85}
                    {:number "DCMB3048" :total_width 30 :total_depth 48 :price 2025 :weight 105}]}

            {:line "Bedford"
             :category "Desk"
             :name "Double Pedestal Desk"
             :description "Box/file pedestals with pull-out writing shelves. Center pencil drawer with lock. Desk chassis always 64.5 | 28.5:"
             :item [{:number "DBD6630" :total_width 66 :total_depth 30 :kneespace 33 :price 2626 :weight 271}
                    {:number "DBD7236" :total_width 72 :total_depth 36 :kneespace 33 :price 2755 :weight 315}]
             :option[{:name "033a"} {:name "034a"} {:name "D18"}]}

            {:line "Bedford"
             :category "Desk"
             :name "Single Pedestal Desk"
             :description "Box/file pedestal with pull-out writing shelf. Center pencil drawer with lock. Specify: Left or right pedestal"
             :item [{:number "DBS4824" :total_width 48 :total_depth 24 :kneespace 30 :price 2158 :weight 172}]
             :option[{:name "033a"} {:name "034a"} {:name "D18"}]}

            {:line "Bedford"
             :category "Credenza"
             :name "Combination Credenza"
             :description "Two box/file pedestals, double hinged doors in center with adjustable shelf behind."
             :item [{:number "DBC6619" :total_width 66 :total_depth 19.5 :price 2456 :weight 268}
                    {:number "DBC7019" :total_width 70.5 :total_depth 19.5 :price 2566 :weight 277}]
             :option[{:name "010"} {:name "012"} {:name "D19"} {:name "D10"}]}

            {:line "Bedford"
             :category "Credenza"
             :name "File Credenza"
             :description "Four lateral file drawers. Two lateral file drawers. (Not illustrated)"
             :item [{:number "DBF1970" :total_width 70.5 :total_depth 19.5 :price 3001 :weight 248}
                    {:number "DBF1936" :total_width 36 :total_depth 19.5 :price 1850 :weight 150}]
             :option[{:name "012"} {:name "D19"}]}

            {:line "Bedford"
             :category "Workstation"
             :name "Executive Return Desk"
             :description "A - Desk with box/file pedestal, and pullout writing shelf. Center pencil drawer with lock. B - 29.25” high return with box/file pedestal. Secretarial ht. same price. Specify: left or right return."
             :style [{:number "DBL7866" :price 3951 :weight 349 :item [{:part "Desk" :total_width 66 :total_depth 30} {:part "Return" :total_width 48 :total_depth 24}]}
                     {:number "DBL8472" :price 4097 :weight 357 :item [{:part "Desk" :total_width 72 :total_depth 36} {:part "Return" :total_width 48 :total_depth 24}]}]
             :option[{:name "030"} {:name "031"} {:name "032a"} {:name "033a"} {:name "034a"} {:name "012"} {:name "D17"} {:name "039"} {:name "027"}]}

            {:line "Bedford"
             :category "Workstation"
             :name "Executive U Workstation"
             :description "A - Desk with box/file pedestal, and pullout writing shelf. Center pencil drawer with lock. B - 29.5” high Bridge Secretarial ht. same price. C - Credenza with box/file pedestal. Specify: left or right return"
             :style [{:number "DBU9866" :price 5511 :weight 537
                      :item [{:part "Desk" :total_width 66 :total_depth 30}
                             {:part "Bridge" :total_width 48 :total_depth 24}
                             {:part "Credenza" :total_width 66 :total_depth 19.5}]}
                     {:number "DSBU1072" :price 5712 :weight 563
                      :item [{:part "Desk" :total_width 72 :total_depth 36}
                             {:part "Bridge" :total_width 48 :total_depth 24}
                             {:part "Credenza" :total_width 70.5 :total_depth 19.5}]}]
             :option[{:name "030"} {:name "031"} {:name "032a"} {:name "033a"} {:name "034a"} {:name "012"} {:name "D17"} {:name "039"} {:name "027"}]}

            {:line "Bedford"
             :category "Bookcase"
             :name "Hutch"
             :description "Hinged doors. 36\" high."
             :item [{:number "DBH6613" :total_width 66 :total_depth 13 :price 2044 :weight 152}
                    {:number "DBH7013" :total_width 70.5 :total_depth 13 :price 2136 :weight 161}]
             :option[{:name "010"} {:name "033a"} {:name "070"} {:name "071"} {:name "072"}]}

            {:line "Bedford"
             :category "Bookcase"
             :name "Open Bookcase"
             :description "Two adjustable shelves. 48\" high."
             :item [{:number "DBB4836" :total_width 36 :total_depth 13 :price 958 :weight 109}]
             :option[{:name "D07"} {:name "D08"} {:name "010"} {:name "060"}]}

            {:line "Classic"
             :category "Desk"
             :name "Table Desk"
             :description "Wood panel legs. Center pencil drawer."
             :item [{:number "AT2230" :total_width 66 :total_depth 30 :kneespace 29 :price 2991 :weight 200}
                    {:number "AT2240" :total_width 72 :total_depth 36 :kneespace 35 :price 3339 :weight 210}
                    {:number "AT2250" :total_width 84 :total_depth 36 :kneespace 47 :price 3758 :weight 230}]
             :option[{:name "013"} {:name "033a"}]}

            {:line "Classic"
             :category "Desk"
             :name "Kidney-Shaped Table Desk"
             :description "with center pencil drawer Lock not available."
             :item [{:number "AT2872" :total_width 72 :total_depth 36 :kneespace 26 :price 6304 :weight 220}
                    {:number "AT2878" :total_width 78 :total_depth 36 :kneespace 28 :price 6626 :weight 230}
                    {:number "AT2841" :total_width 84 :total_depth 42 :kneespace 32 :price 6954 :weight 240}
                    {:number "AT2842" :total_width 90 :total_depth 42 :kneespace 34 :price 7363 :weight 245}
                    {:number "AT2843" :total_width 96 :total_depth 42 :kneespace 36 :price 7454 :weight 250}]
             :option[{:name "033a"} {:name "A45"}]}

            {:line "Classic"
             :category "Desk"
             :name "Double Pedestal Desk with Overhang Ends"
             :description "Box/box/file pedestals. Pull-out dictation shelves. Center pencil drawer. Locks in each pedestal."
             :item [{:number "AT2050" :total_width 66 :total_depth 30 :kneespace 24 :price 4261 :weight 320}
                    {:number "AT2060" :total_width 72 :total_depth 36 :kneespace 30 :price 4410 :weight 340}
                    {:number "AT2070" :total_width 84 :total_depth 36 :kneespace 34 :price 4852 :weight 375}]
             :option[{:name "013"} {:name "033a"}]}

            {:line "Classic"
             :category "Desk"
             :name "Double Pedestal Desk with Flush Ends"
             :description "Box/box/file pedestals. Pull-out dictation shelves. Center pencil drawer. Locks in each pedestal."
             :item [{:number "AT2051" :total_width 66 :total_depth 30 :kneespace 29 :price 4273 :weight 320}
                    {:number "AT2061" :total_width 72 :total_depth 36 :kneespace 35 :price 4424 :weight 340}
                    {:number "AT2071" :total_width 84 :total_depth 36 :kneespace 47 :price 4864 :weight 375}]}

            {:line "Classic"
             :category "Desk"
             :name "Bowshaped Top Double Pedestal Desk"
             :description "Box/box/file pedestals. Pull-out dictation shelves. Center pencil drawer. Locks in each pedestal."
             :item [{:number "AT2053" :total_width 84 :total_depth 36 :kneespace 34 :price 5567 :weight 375}
                    {:number "AT2054" :total_width 92 :total_depth 36 :kneespace 38 :price 5904 :weight 420}]
             :option[{:name "013"} {:name "033a"}]}

            {:line "Classic"
             :category "Desk"
             :name "Single Pedestal Desk"
             :description "Box/box/file pedestal and open storage pedestal. Pull-out dictation shelf. For use with any return on following page. Specify left or right return."
             :item [{:number "AT2214" :total_width 66 :total_depth 30 :kneespace 28 :price 3912 :weight 245}
                    {:number "AT2215" :total_width 66 :total_depth 36 :kneespace 28 :price 3983 :weight 255}
                    {:number "AT2216" :total_width 72 :total_depth 36 :kneespace 34 :price 4063 :weight 275}
                    {:number "AT2217" :total_width 78 :total_depth 36 :kneespace 38 :price 4438 :weight 300}]
             :option[{:name "013"} {:name "012"} {:name "020"}]}

            {:line "Classic"
             :category "Desk"
             :name "Bullet End Table Desk"
             :description "Center pencil drawer. 6\" dia. wood cylinder. For use with any return on following page. Specify left or right return."
             :item [{:number "AT2840" :total_width 72 :total_depth 36 :kneespace 32 :price 4141 :weight 165}
                    {:number "AT2850" :total_width 84 :total_depth 36 :kneespace 44 :price 4471 :weight 190}
                    {:number "AT2870" :total_width 90 :total_depth 42 :kneespace 60 :price 5298 :weight 210}]
             :option[{:name "013"} {:name "080"} {:name "081"}]}

            {:line "Classic"
             :category "Table"
             :name "Computer Corner Table"
             :description "with keyboard drawer and grommet. AT2380’s angled work edge is 30” wide w/ 19” ends. AT2381’s angled  edge is 31” wide w/ 25” ends."
             :item [{:number "AT2380" :total_width 40 :total_depth 40 :kneespace 30 :price 2498 :weight 195}
                    {:number "AT2381" :total_width 48 :total_depth 48 :kneespace 31 :price 2890 :weight 240}]}


            {:line "Classic"
             :category "Return"
             :name "Bridge Return"
             :description "For connecting single pedestal desks with credenzas to create a U workstation."
             :item [{:number "AT2670" :total_width 42 :total_depth 19 :kneespace 42 :price 1071 :weight 70}
                    {:number "AT2671" :total_width 48 :total_depth 19 :kneespace 48 :price 1181 :weight 80}
                    {:number "AT2672" :total_width 54 :total_depth 19 :kneespace 54 :price 1210 :weight 90}
                    {:number "AT2673" :total_width 60 :total_depth 19 :kneespace 60 :price 1255 :weight 95}
                    {:number "AT2675" :total_width 42 :total_depth 25 :kneespace 42 :price 1129 :weight 75}
                    {:number "AT2676" :total_width 48 :total_depth 25 :kneespace 48 :price 1255 :weight 85}
                    {:number "AT2677" :total_width 54 :total_depth 25 :kneespace 54 :price 1287 :weight 95}
                    {:number "AT2678" :total_width 60 :total_depth 25 :kneespace 60 :price 1343 :weight 100}]
             :option[{:name "030"} {:name "031"} {:name "032"} {:name "033a"} {:name "012"} {:name "020"} {:name "013"}]}

            {:line "Classic"
             :category "Return"
             :name "Return Shell"
             :description "Specify left or right return and executive or secretarial height."
             :item [{:number "AT2221" :total_width 48 :total_depth 19 :kneespace 45 :price 1138 :weight 90}
                    {:number "AT2222" :total_width 60 :total_depth 19 :kneespace 57 :price 1327 :weight 105}
                    {:number "AT2231" :total_width 48 :total_depth 25 :kneespace 45 :price 1422 :weight 95}
                    {:number "AT2232" :total_width 60 :total_depth 25 :kneespace 57 :price 1660 :weight 110}]
             :option[{:name "030"} {:name "032"} {:name "033a"} {:name "012"}]}

            {:line "Classic"
             :category "Return"
             :name "Return with box/box/file pedestal"
             :description "Specify left or right return and executive or secretarial height."
             :item [{:number "AT2223" :total_width 48 :total_depth 19 :kneespace 30 :price 1673 :weight 120}
                    {:number "AT2224" :total_width 60 :total_depth 19 :kneespace 42 :price 2043 :weight 130}
                    {:number "AT2233" :total_width 48 :total_depth 25 :kneespace 30 :price 2090 :weight 135}
                    {:number "AT2234" :total_width 60 :total_depth 25 :kneespace 42 :price 2554 :weight 145}]
             :option[{:name "030"} {:name "032"} {:name "033a"} {:name "012"}]}

            {:line "Classic"
             :category "Return"
             :name "Return with 36\" lateral file"
             :description "Specify left or right return and executive or secretarial height."
             :item [{:number "AT2225" :total_width 48 :total_depth 19 :kneespace 11 :price 2525 :weight 210}
                    {:number "AT2226" :total_width 60 :total_depth 19 :kneespace 21 :price 2795 :weight 220}
                    {:number "AT2235" :total_width 48 :total_depth 25 :kneespace 11 :price 3155 :weight 215}
                    {:number "AT2236" :total_width 60 :total_depth 25 :kneespace 21 :price 3493 :weight 225}]
             :option[{:name "012"}]}

            {:line "Classic"
             :category "Return"
             :name "Return with two box/box/file pedestals"
             :description "Specify left or right return and executive or secretarial height."
             :item [{:number "AT2227" :total_width 48 :total_depth 19 :kneespace 13 :price 3246 :weight 195}
                    {:number "AT2228" :total_width 60 :total_depth 19 :kneespace 25 :price 3522 :weight 205}
                    {:number "AT2237" :total_width 48 :total_depth 25 :kneespace 13 :price 4057 :weight 210}
                    {:number "AT2238" :total_width 60 :total_depth 25 :kneespace 25 :price 4404 :weight 220}]
             :option[{:name "012"}]}

            {:line "Classic"
             :category "Credenza"
             :name "Cabinet Credenza 29\" high."
             :description "1 adjustable shelf behind each pair of doors. 96” and 120” credenzas will be shipped K/D"
             :item [{:number "AT2530" :total_width 35.5 :total_depth 19 :door 2 :price 1729 :weight 150}
                    {:number "AT2320" :total_width 42 :total_depth 19 :door 2 :price 2081 :weight 165}
                    {:number "AT2340" :total_width 52.5 :total_depth 19 :door 3 :price 2892 :weight 210}
                    {:number "AT2330" :total_width 72 :total_depth 19 :door 4 :price 3458 :weight 260}
                    {:number "AT2331" :total_width 86.5 :total_depth 19 :door 5 :price 4638 :weight 350}
                    {:number "AT2332" :total_width 96 :total_depth 19 :door 6 :price 5366 :weight 420}
                    {:number "AT2333" :total_width 120 :total_depth 19 :door 8 :price 6081 :weight 550}
                    {:number "AT2341" :total_width 35.5 :total_depth 25 :door 2 :price 2161 :weight 180}
                    {:number "AT2342" :total_width 42 :total_depth 25 :door 2 :price 2602 :weight 210}
                    {:number "AT2343" :total_width 52.5 :total_depth 25 :door 3 :price 3615 :weight 240}
                    {:number "AT2344" :total_width 72 :total_depth 25 :door 4 :price 4323 :weight 300}
                    {:number "AT2345" :total_width 86.5 :total_depth 25 :door 5 :price 5797 :weight 390}
                    {:number "AT2346" :total_width 96 :total_depth 25 :door 6 :price 6708 :weight 490}
                    {:number "AT2347" :total_width 120 :total_depth 25 :door 8 :price 7601 :weight 615}]
             :option[{:name "010"} {:name "033a"}]}

            {:line "Classic"
             :category "Credenza"
             :name "36\" high. Cabinet Credenza"
             :description "2 adjustable shelves behind each pair of doors. 96” and 120” credenzas will be shipped K/D"
             :item [{:number "AT2560" :total_width 35.5 :total_depth 19 :door 2 :price 2075 :weight 160}
                    {:number "AT2561" :total_width 42 :total_depth 19 :door 2 :price 2498 :weight 180}
                    {:number "AT2562" :total_width 52.5 :total_depth 19 :door 3 :price 3458 :weight 235}
                    {:number "AT2563" :total_width 72 :total_depth 19 :door 4 :price 4163 :weight 300}
                    {:number "AT2564" :total_width 86.5 :total_depth 19 :door 5 :price 5564 :weight 370}
                    {:number "AT2565" :total_width 96 :total_depth 19 :door 6 :price 6440 :weight 435}
                    {:number "AT2566" :total_width 120 :total_depth 19 :door 8 :price 7393 :weight 585}
                    {:number "AT2520" :total_width 35.5 :total_depth 25 :door 2 :price 2595 :weight 200}
                    {:number "AT2521" :total_width 42 :total_depth 25 :door 2 :price 3123 :weight 220}
                    {:number "AT2522" :total_width 52.5 :total_depth 25 :door 3 :price 4323 :weight 260}
                    {:number "AT2523" :total_width 72 :total_depth 25 :door 4 :price 5203 :weight 340}
                    {:number "AT2524" :total_width 86.5 :total_depth 25 :door 5 :price 6955 :weight 430}
                    {:number "AT2525" :total_width 96 :total_depth 25 :door 6 :price 8049 :weight 530}
                    {:number "AT2526" :total_width 120 :total_depth 25 :door 8 :price 9242 :weight 650}]
             :option[{:name "010"} {:name "033a"}]}

            {:line "Classic"
             :category "Credenza"
             :name "Refrigerator / Storage"
             :description "2 cubic ft. capacity."
             :item [{:number "AT2490" :total_width 35.5 :total_depth 19 :door 2 :price 4770 :weight 200}
                    {:number "AT2491" :total_width 35.5 :total_depth 25 :door 2 :price 5962 :weight 220}]
             :option[{:name "011"}]}

            {:line "Classic"
             :category "Credenza"
             :name "Conference Room Credenza"
             :description "29\" high. A 6\" utility drawer located behind the two center doors. One adjustable shelf in each end section. 96” and 120” credenzas will be shipped K/D"
             :item [{:number "AT2290" :total_width 35.5 :total_depth 19 :door 2 :price 2361 :weight 165}
                    {:number "AT2291" :total_width 72 :total_depth 19 :door 4 :price 4049 :weight 295}
                    {:number "AT2296" :total_width 96 :total_depth 19 :door 6 :price 6152 :weight 430}
                    {:number "AT2297" :total_width 120 :total_depth 19 :door 8 :price 7012 :weight 570}
                    {:number "AT2280" :total_width 35.5 :total_depth 25 :door 2 :price 2952 :weight 195}
                    {:number "AT2281" :total_width 72 :total_depth 25 :door 4 :price 5061 :weight 320}
                    {:number "AT2282" :total_width 96 :total_depth 25 :door 6 :price 7691 :weight 515}
                    {:number "AT2283" :total_width 120 :total_depth 25 :door 8 :price 8764 :weight 625}]
             :option[{:name "010"} {:name "033a"}]}

            {:line "Classic"
             :category "Credenza"
             :name "36” High Buffet Credenza"
             :description "Doors and drawers. 108” credenza will be shipped K/D"
             :item [{:number "AT2292" :total_width 37.5 :total_depth 19 :door 2 :price 3166 :weight 175}
                    {:number "AT2293" :total_width 73.5 :total_depth 19 :door 4 :price 4984 :weight 330}
                    {:number "AT2294" :total_width 108 :total_depth 19 :door 6 :price 8442 :weight 475}
                    {:number "AT2272" :total_width 37.5 :total_depth 25 :door 2 :price 3957 :weight 215}
                    {:number "AT2273" :total_width 73.5 :total_depth 25 :door 4 :price 6230 :weight 360}
                    {:number "AT2274" :total_width 108 :total_depth 25 :door 6 :price 10553 :weight 535}]
             :option[{:name "010"} {:name "013"} {:name "033a"}]}

            {:line "Classic"
             :category "Credenza"
             :name "Combination Credenza"
             :description "2 doors, 1 file and 2 box drawers"
             :item [{:number "AT2350" :total_width 52.5 :total_depth 19 :price 3613 :weight 275}
                    {:number "AT2351" :total_width 52.5 :total_depth 25 :price 4517 :weight 300}]
             :option[{:name "011"} {:name "012"}]}

            {:line "Classic"
             :category "Credenza"
             :name "Combination Credenza"
             :description "2 doors, 2 file and 4 box drawers."
             :item [{:number "AT2030" :total_width 72 :total_depth 19 :price 4043 :weight 325}
                    {:number "AT2031" :total_width 72 :total_depth 25 :price 5054 :weight 375}]
             :option[{:name "011"} {:name "012"}]}

            {:line "Classic"
             :category "Credenza"
             :name "File Credenza"
             :item [{:number "AT2042" :total_width 35.5 :total_depth 19 :drawers 4 :price 2575 :weight 200}
                    {:number "AT2041" :total_width 52.5 :total_depth 19 :drawers 6 :price 4150 :weight 290}
                    {:number "AT2040" :total_width 69.5 :total_depth 19 :drawers 8 :price 4561 :weight 350}
                    {:number "AT2045" :total_width 35.5 :total_depth 25 :drawers 4 :price 3219 :weight 220}
                    {:number "AT2046" :total_width 52.5 :total_depth 25 :drawers 6 :price 5187 :weight 325}
                    {:number "AT2047" :total_width 69.5 :total_depth 25 :drawers 8 :price 5701 :weight 390}]
             :option[{:name "012"}]}

            {:line "Classic"
             :category "Credenza"
             :name "Writing Credenza"
             :description "4 box and 2 file drawers."
             :item [{:number "AT2360" :total_width 60 :total_depth 19 :price 3387 :weight 225}
                    {:number "AT2370" :total_width 72 :total_depth 19 :price 3691 :weight 250}
                    {:number "AT2361" :total_width 60 :total_depth 25 :price 4235 :weight 240}
                    {:number "AT2371" :total_width 72 :total_depth 25 :price 4614 :weight 265}]
             :option[{:name "012"} {:name "033a"}]}

            {:line "Classic"
             :category "File"
             :name "Lateral File"
             :description "2 drawers"
             :item [{:number "AT2300" :total_width 31.5 :total_depth 19 :price 2104 :weight 175}
                    {:number "AT2310" :total_width 37.5 :total_depth 19 :price 2235 :weight 190}
                    {:number "AT2301" :total_width 31.5 :total_depth 25 :price 2629 :weight 200}
                    {:number "AT2302" :total_width 37.5 :total_depth 25 :price 2793 :weight 245}]
             :option[{:name "012"}]}

            {:line "Classic"
             :category "File"
             :name "Lateral File"
             :description "4 drawers"
             :item [{:number "AT2311" :total_width 61.5 :total_depth 19 :price 4069 :weight 350}
                    {:number "AT2312" :total_width 73.5 :total_depth 19 :price 4326 :weight 375}
                    {:number "AT2315" :total_width 61.5 :total_depth 25 :price 5087 :weight 395}
                    {:number "AT2316" :total_width 73.5 :total_depth 25 :price 5407 :weight 425}]
             :option[{:name "012"}]}

            {:line "Classic"
             :category "File"
             :name "Lateral File"
             :description "3 drawers; 41\" high"
             :item [{:number "AT2303" :total_width 31.5 :total_depth 19 :price 3194 :weight 225}
                    {:number "AT2304" :total_width 37.5 :total_depth 19 :price 3373 :weight 240}
                    {:number "AT2305" :total_width 31.5 :total_depth 25 :price 3992 :weight 275}
                    {:number "AT2306" :total_width 37.5 :total_depth 25 :price 4215 :weight 290}]
             :option[{:name "012"}]}

            {:line "Classic"
             :category "File"
             :name "Lateral File"
             :description "4 drawers; 53\" high"
             :item [{:number "AT2307" :total_width 31.5 :total_depth 19 :price 3833 :weight 295}
                    {:number "AT2308" :total_width 37.5 :total_depth 19 :price 4048 :weight 310}
                    {:number "AT2317" :total_width 31.5 :total_depth 25 :price 4790 :weight 345}
                    {:number "AT2318" :total_width 37.5 :total_depth 25 :price 4989 :weight 360}]
             :option[{:name "012"}]}

            {:line "Classic"
             :category "Credenza"
             :name "Writing Credenza"
             :description "with 36” lateral file pedestal. Specify file pedestal left or right."
             :item [{:number "AT2140" :total_width 60 :total_depth 19 :price 3661 :weight 245}
                    {:number "AT2150" :total_width 72 :total_depth 19 :price 3917 :weight 265}
                    {:number "AT2141" :total_width 60 :total_depth 25 :price 4575 :weight 275}
                    {:number "AT2151" :total_width 72 :total_depth 25 :price 4897 :weight 295}]
             :option[{:name "033a"} {:name "020"} {:name "013"} {:name "030"}]}

            {:line "Classic"
             :category "Cabinet"
             :name "Open CPU Cabinet"
             :description "with keyboard drawer and grommet. 29.5\" high"
             :item [{:number "AT2650" :total_width 36 :total_depth 25 :price 2118 :weight 90}
                    {:number "AT2651" :total_width 42 :total_depth 25 :price 2214 :weight 95}
                    {:number "AT2652" :total_width 48 :total_depth 25 :price 2312 :weight 105}]}

            {:line "Classic"
             :category "Cabinet"
             :name "Wall Hung Cabinet"
             :description "with hinged doors. Inside ht.13.5\" Inside depth 12\" Outside ht.18\""
             :item [{:number "AT2700" :total_width 31.5 :total_depth 15 :door 2 :price 1445 :weight 50}
                    {:number "AT2710" :total_width 35.5 :total_depth 15 :door 2 :price 1566 :weight 60}
                    {:number "AT2720" :total_width 52.5 :total_depth 15 :door 3 :price 2092 :weight 70}
                    {:number "AT2730" :total_width 61.5 :total_depth 15 :door 4 :price 2394 :weight 80}
                    {:number "AT2740" :total_width 72 :total_depth 15 :door 4 :price 2477 :weight 95}]
             :option[{:name "011"} {:name "A48"}]}

            {:line "Classic"
             :category "Hutch"
             :name "Hinged Door Hutch"
             :description "for credenzas. OA ht. 36\". Inside cabinet ht. 13.5\" Inside cabinet depth 12\". Optional: 42\" OA ht. + 10% 54\" OA ht. + 15% For credenzas see p. 143"
             :item [{:number "AT2600" :total_width 31.5 :total_depth 15 :door 2 :price 1788 :weight 70}
                    {:number "AT2610" :total_width 35.5 :total_depth 15 :door 2 :price 1801 :weight 95}
                    {:number "AT2620" :total_width 37.5 :total_depth 15 :door 2 :price 1894 :weight 100}
                    {:number "AT2630" :total_width 52.5 :total_depth 15 :door 3 :price 3129 :weight 150}
                    {:number "AT2640" :total_width 61.5 :total_depth 15 :door 4 :price 3799 :weight 160}
                    {:number "AT2641" :total_width 72 :total_depth 15 :door 4 :price 3833 :weight 175}
                    {:number "AT2642" :total_width 73.5 :total_depth 15 :door 4 :price 3872 :weight 180}]
             :option[{:name "011"} {:name "070"} {:name "071"} {:name "072"} {:name "A48"}]}

            {:line "Classic"
             :category "Hutch"
             :name "Hinged Door Hutch"
             :description "for credenzas. OA ht.42\". Two glass doors in center with two adjustable shelves. Wood doors on ends with two shelves. For credenzas see p. 143"
             :item [{:number "AT2690" :total_width 69.5 :total_depth 15 :door 2 :price 4332 :weight 180}
                    {:number "AT2691" :total_width 72 :total_depth 15 :door 2 :price 4371 :weight 185}
                    {:number "AT2692" :total_width 73.5 :total_depth 15 :door 2 :price 4410 :weight 195}]
             :option[{:name "010"} {:name "A46"}]}

            {:line "Classic"
             :category "Hutch"
             :name "Bookcase Hutch"
             :description "for credenzas. OA ht. 54\" Optional: 42\" OA ht. Deduct 10% For credenzas see p. 143"
             :item [{:number "AT2661" :total_width 31.5 :total_depth 15 :price 1518 :weight 65}
                    {:number "AT2662" :total_width 35.5 :total_depth 15 :price 1571 :weight 70}
                    {:number "AT2663" :total_width 37.5 :total_depth 15 :price 1637 :weight 95}
                    {:number "AT2664" :total_width 52.5 :total_depth 15 :price 2839 :weight 150}
                    {:number "AT2666" :total_width 72 :total_depth 15 :price 3741 :weight 160}
                    {:number "AT2667" :total_width 73.5 :total_depth 15 :price 3898 :weight 175}]
             :option[{:name "070"} {:name "071"} {:name "072"}]}

            {:line "Classic"
             :category "Cabinet"
             :name "Storage/Wardrobe"
             :description "Two doors. 1/3 storage with shelves. 2/3 wardrobe with one shelf and coat rod. 19” deep units are shipped with front to back clothes rod"
             :item [{:number "AT2591" :total_height 72 :total_width 31.5 :total_depth 19 :price 3281 :weight 185}
                    {:number "AT2592" :total_height 84 :total_width 31.5 :total_depth 19 :price 3471 :weight 195}
                    {:number "AT2593" :total_height 72 :total_width 37.5 :total_depth 19 :price 3478 :weight 195}
                    {:number "AT2594" :total_height 84 :total_width 37.5 :total_depth 19 :price 3753 :weight 205}
                    {:number "AT2596" :total_height 72 :total_width 31.5 :total_depth 25 :price 3773 :weight 215}
                    {:number "AT2597" :total_height 84 :total_width 31.5 :total_depth 25 :price 3991 :weight 230}
                    {:number "AT2598" :total_height 72 :total_width 37.5 :total_depth 25 :price 4000 :weight 235}
                    {:number "AT2599" :total_height 84 :total_width 37.5 :total_depth 25 :price 4316 :weight 250}]
             :option[{:name "010"} {:name "A34"}]}

            {:line "Classic"
             :category "Bookcase"
             :name "Open Vertical Bookcase"
             :description "13\" OA depth"
             :item [{:number "AT2927" :total_height 30 :total_width 31.5 :shelf 1 :price 973 :weight 50}
                    {:number "AT2926" :total_height 36 :total_width 31.5 :shelf 2 :price 1085 :weight 65}
                    {:number "AT2925" :total_height 42 :total_width 31.5 :shelf 2 :price 1314 :weight 80}
                    {:number "AT2924" :total_height 48 :total_width 31.5 :shelf 3 :price 1445 :weight 85}
                    {:number "AT2923" :total_height 60 :total_width 31.5 :shelf 3 :price 1617 :weight 100}
                    {:number "AT2922" :total_height 72 :total_width 31.5 :shelf 4 :price 1820 :weight 120}
                    {:number "AT2921" :total_height 84 :total_width 31.5 :shelf 5 :price 2004 :weight 130}
                    {:number "AT2920" :total_height 96 :total_width 31.5 :shelf 6 :price 2104 :weight 150}
                    {:number "AT2937" :total_height 30 :total_width 37.5 :shelf 1 :price 1039 :weight 60}
                    {:number "AT2936" :total_height 36 :total_width 37.5 :shelf 2 :price 1183 :weight 75}
                    {:number "AT2935" :total_height 42 :total_width 37.5 :shelf 2 :price 1381 :weight 90}
                    {:number "AT2934" :total_height 48 :total_width 37.5 :shelf 3 :price 1507 :weight 95}
                    {:number "AT2933" :total_height 60 :total_width 37.5 :shelf 3 :price 1702 :weight 110}
                    {:number "AT2932" :total_height 72 :total_width 37.5 :shelf 4 :price 1972 :weight 130}
                    {:number "AT2931" :total_height 84 :total_width 37.5 :shelf 5 :price 2190 :weight 140}
                    {:number "AT2930" :total_height 96 :total_width 37.5 :shelf 6 :price 2289 :weight 160}]
             :option[{:name "060"} {:name "061"} {:name "A32"}]}

            {:line "Classic"
             :category "Bookcase"
             :name "Open Horizontal Bookcase"
             :description "13\" OA depth"
             :item [{:number "AT2980" :total_height 30 :total_width 42 :shelf 1 :price 1566 :weight 55}
                    {:number "AT2981" :total_height 48 :total_width 42 :shelf 3 :price 2202 :weight 85}
                    {:number "AT2983" :total_height 30 :total_width 48 :shelf 1 :price 1775 :weight 65}
                    {:number "AT2984" :total_height 48 :total_width 48 :shelf 3 :price 2399 :weight 95}
                    {:number "AT2985" :total_height 30 :total_width 60 :shelf 1 :price 1966 :weight 75}
                    {:number "AT2986" :total_height 48 :total_width 60 :shelf 3 :price 2965 :weight 110}
                    {:number "AT2987" :total_height 30 :total_width 72 :shelf 1 :price 2123 :weight 100}
                    {:number "AT2988" :total_height 48 :total_width 72 :shelf 3 :price 3036 :weight 150}]
             :option[{:name "061"} {:name "A32"}]}

            {:line "Classic"
             :category "Table"
             :name "Telephone Table"
             :description "with hinged door and one adjustable shelf. Door hinged on left. 27\" high."
             :item [{:number "AT2473" :total_width 18.5 :total_depth 19 :price 1256 :weight 45}
                    {:number "AT2474" :total_width 18.5 :total_depth 27 :price 1413 :weight 50}]
             :option[{:name "011"}]}

            {:line "Classic"
             :category "Cabinet"
             :name "Video Console/ Computer Combination"
             :description "72\" high with pocket doors. Adjustable shelf in upper section. Pull-out swivel shelf at 29.5\" height. Keyboard drawer. Pull-out shelf in lower section. Inside dimensions: *29\" wide | 16\" deep **29\" wide | 22\" deep"
             :item [{:number "AT2475" :total_width 37.5 :total_depth 19 :price 5680 :weight 250}
                    {:number "AT2476" :total_width 37.5 :total_depth 24 :price 6284 :weight 295}]
             :option[{:name "010"}]}

            {:line "Classic"
             :category "Cabinet"
             :name "Storage File Cabinet"
             :description "Two doors with adjustable shelves, and two lateral file drawers."
             :item [{:number "AT2505" :total_height 72 :total_width 19 :total_depth 31.5 :shelf 2 :price 4261 :weight 195}
                    {:number "AT2506" :total_height 84 :total_width 19 :total_depth 31.5 :shelf 3 :price 4746 :weight 205}
                    {:number "AT2507" :total_height 72 :total_width 19 :total_depth 37.5 :shelf 2 :price 4438 :weight 205}
                    {:number "AT2508" :total_height 84 :total_width 19 :total_depth 37.5 :shelf 3 :price 4924 :weight 215}
                    {:number "AT2501" :total_height 72 :total_width 25 :total_depth 31.5 :shelf 2 :price 4687 :weight 215}
                    {:number "AT2502" :total_height 84 :total_width 25 :total_depth 31.5 :shelf 3 :price 5220 :weight 225}
                    {:number "AT2503" :total_height 72 :total_width 25 :total_depth 37.5 :shelf 2 :price 4882 :weight 245}
                    {:number "AT2504" :total_height 84 :total_width 25 :total_depth 37.5 :shelf 3 :price 5417 :weight 260}]
             :option[{:name "010"} {:name "012"}]}

            {:line "Classic"
             :category "Bookcase"
             :name "Open Bookcase with Two Door Credenza"
             :description "19” deep credenzas have 13” deep bookcase. 25” deep credenzas have 15” deep bookcase."
             :item [{:number "AT2510" :total_height 60 :total_width 19 :total_depth 31.5 :shelf 1 :price 2913 :weight 155}
                    {:number "AT2511" :total_height 72 :total_width 19 :total_depth 31.5 :shelf 2 :price 3281 :weight 165}
                    {:number "AT2512" :total_height 84 :total_width 19 :total_depth 31.5 :shelf 3 :price 3570 :weight 175}
                    {:number "AT2513" :total_height 60 :total_width 19 :total_depth 37.5 :shelf 1 :price 3102 :weight 165}
                    {:number "AT2514" :total_height 72 :total_width 19 :total_depth 37.5 :shelf 2 :price 3452 :weight 175}
                    {:number "AT2515" :total_height 84 :total_width 19 :total_depth 37.5 :shelf 3 :price 3773 :weight 185}
                    {:number "AT2540" :total_height 60 :total_width 25 :total_depth 31.5 :shelf 1 :price 3204 :weight 175}
                    {:number "AT2541" :total_height 72 :total_width 25 :total_depth 31.5 :shelf 2 :price 3610 :weight 185}
                    {:number "AT2542" :total_height 84 :total_width 25 :total_depth 31.5 :shelf 3 :price 3927 :weight 200}
                    {:number "AT2543" :total_height 60 :total_width 25 :total_depth 37.5 :shelf 1 :price 3413 :weight 185}
                    {:number "AT2544" :total_height 72 :total_width 25 :total_depth 37.5 :shelf 2 :price 3797 :weight 200}
                    {:number "AT2545" :total_height 84 :total_width 25 :total_depth 37.5 :shelf 3 :price 4150 :weight 215}]
             :option[{:name "010"} {:name "061"} {:name "A38"}]}

            {:line "Classic"
             :category "Bookcase"
             :name "Open Bookcase with Lateral File"
             :description "19” deep credenzas have 13” deep bookcase. 25” deep credenzas have 15” deep bookcase"
             :item [{:number "AT2570" :total_height 60 :total_width 19 :total_depth 31.5 :shelf 1 :price 2984 :weight 205}
                    {:number "AT2571" :total_height 72 :total_width 19 :total_depth 31.5 :shelf 2 :price 3326 :weight 225}
                    {:number "AT2572" :total_height 84 :total_width 19 :total_depth 31.5 :shelf 3 :price 3636 :weight 235}
                    {:number "AT2573" :total_height 60 :total_width 19 :total_depth 37.5 :shelf 1 :price 3165 :weight 225}
                    {:number "AT2574" :total_height 72 :total_width 19 :total_depth 37.5 :shelf 2 :price 3530 :weight 235}
                    {:number "AT2575" :total_height 84 :total_width 19 :total_depth 37.5 :shelf 3 :price 3812 :weight 245}
                    {:number "AT2550" :total_height 60 :total_width 25 :total_depth 31.5 :shelf 1 :price 3283 :weight 245}
                    {:number "AT2551" :total_height 72 :total_width 25 :total_depth 31.5 :shelf 2 :price 3659 :weight 255}
                    {:number "AT2552" :total_height 84 :total_width 25 :total_depth 31.5 :shelf 3 :price 4000 :weight 270}
                    {:number "AT2553" :total_height 60 :total_width 25 :total_depth 37.5 :shelf 1 :price 3482 :weight 255}
                    {:number "AT2554" :total_height 72 :total_width 25 :total_depth 37.5 :shelf 2 :price 3883 :weight 270}
                    {:number "AT2555" :total_height 84 :total_width 25 :total_depth 37.5 :shelf 3 :price 4195 :weight 290}]
             :option[{:name "012"} {:name "061"}]}

            {:line "Classic"
             :category "Bookcase"
             :name "Vertical Bookcase with Partial Doors."
             :item [{:number "AT2901" :total_height 60 :total_width 19 :total_depth 31.5 :shelf 2 :price 2072 :weight 155}
                    {:number "AT2902" :total_height 72 :total_width 19 :total_depth 31.5 :shelf 3 :price 2259 :weight 175}
                    {:number "AT2903" :total_height 84 :total_width 19 :total_depth 31.5 :shelf 4 :price 2477 :weight 185}
                    {:number "AT2904" :total_height 96 :total_width 19 :total_depth 31.5 :shelf 5 :price 2577 :weight 220}
                    {:number "AT2911" :total_height 60 :total_width 19 :total_depth 37.5 :shelf 2 :price 2156 :weight 165}
                    {:number "AT2912" :total_height 72 :total_width 19 :total_depth 37.5 :shelf 3 :price 2359 :weight 185}
                    {:number "AT2913" :total_height 84 :total_width 19 :total_depth 37.5 :shelf 4 :price 2588 :weight 195}
                    {:number "AT2914" :total_height 96 :total_width 19 :total_depth 37.5 :shelf 5 :price 2712 :weight 230}
                    {:number "AT2940" :total_height 60 :total_width 25 :total_depth 31.5 :shelf 2 :price 2279 :weight 175}
                    {:number "AT2941" :total_height 72 :total_width 25 :total_depth 31.5 :shelf 3 :price 2485 :weight 190}
                    {:number "AT2942" :total_height 84 :total_width 25 :total_depth 31.5 :shelf 4 :price 2725 :weight 200}
                    {:number "AT2943" :total_height 96 :total_width 25 :total_depth 31.5 :shelf 5 :price 2835 :weight 230}
                    {:number "AT2944" :total_height 60 :total_width 25 :total_depth 37.5 :shelf 2 :price 2371 :weight 185}
                    {:number "AT2945" :total_height 72 :total_width 25 :total_depth 37.5 :shelf 3 :price 2595 :weight 200}
                    {:number "AT2946" :total_height 84 :total_width 25 :total_depth 37.5 :shelf 4 :price 2717 :weight 215}
                    {:number "AT2947" :total_height 96 :total_width 25 :total_depth 37.5 :shelf 5 :price 2849 :weight 230}]
             :option[{:name "010"} {:name "061"} {:name "A32"}]}

            {:line "Classic"
             :category "Part"
             :name "Full Modesty Panel"
             :description "27\" high."
             :item [{:number "AT2480" :total_width 24 :price 220 :weight 15}
                    {:number "AT2481" :total_width 30 :price 262 :weight 20}
                    {:number "AT2482" :total_width 36 :price 344 :weight 25}
                    {:number "AT2483" :total_width 42 :price 400 :weight 30}
                    {:number "AT2484" :total_width 48 :price 482 :weight 35}]
             :option[{:name "033a"}]}

            {:line "Classic"
             :category "Part"
             :name "3/4 Modesty Panel"
             :description "18\" high."
             :item [{:number "AT2460" :total_width 24 :price 202 :weight 12}
                    {:number "AT2461" :total_width 30 :price 239 :weight 15}
                    {:number "AT2462" :total_width 36 :price 310 :weight 20}
                    {:number "AT2463" :total_width 42 :price 359 :weight 25}
                    {:number "AT2465" :total_width 48 :price 435 :weight 30}]
             :option[{:name "033a"}]}

            {:line "Classic"
             :category "Part"
             :name "Full Wrap Around"
             :description "Modesty Panel for bullet desk tops."
             :item [{:number "AT2466" :total_width 72 :total_depth 36 :price 1033 :weight 95}
                    {:number "AT2467" :total_width 84 :total_depth 36 :price 1102 :weight 125}
                    {:number "AT2468" :total_width 90 :total_depth 42 :price 1170 :weight 150}]}

            {:line "Classic"
             :category "Part"
             :name "Wood Cylinder Base with 12\" high kinked modesty panel"
             :description "for bullet desk tops only"
             :item [{:number "AT2470" :total_width 72 :total_depth 36 :price 946 :weight 85}
                    {:number "AT2471" :total_width 84 :total_depth 36 :price 1003 :weight 115}
                    {:number "AT2472" :total_width 90 :total_depth 42 :price 1059 :weight 140}]}

            {:line "Classic"
             :category "Part"
             :name "Wood Cylinder Base"
             :description "Black Metal Cylinder Base is Optional - same price. Please specify."
             :item [{:number "AT2458" :diamater "6\" dia" :price 601 :weight 20}]}

            {:line "Classic"
             :category "Part"
             :name "Add-On Pedestals"
             :description "No top. 27.5\" OA ht A - box/ box/ file for credenzas B - box/ box/ file for desks"
             :item [{:number "AT2401 (A)" :total_width 18.5 :total_depth 18 :price 1009 :weight 77}
                    {:number "AT2402 (A)" :total_width 18.5 :total_depth 24 :price 1072 :weight 85}
                    {:number "AT2403 (B)" :total_width 18.5 :total_depth 26 :price 1103 :weight 90}]
             :option[{:name "012"} {:name "010"} {:name "100"}]}

            {:line "Classic"
             :category "Part"
             :name "Add-On Pedestals"
             :description "No top. 27.5\" high. A - File/file for credenzas B - File/file for desks D - Double lateral file for credenzas. 27.5\"H"
             :item [{:number "AT2405 (A)" :total_width 18.5 :total_depth 18 :price 1044 :weight 75}
                    {:number "AT2406 (B)" :total_width 18.5 :total_depth 24 :price 1126 :weight 85}
                    {:number "AT2407 (D)" :total_width 30 :total_depth 18 :price 1269 :weight 155}
                    {:number "AT2408 (D)" :total_width 36 :total_depth 18 :price 1365 :weight 165}]
             :option[{:name "012"} {:name "010"} {:name "100"}]}

            {:line "Classic"
             :category "Part"
             :name "Add-On Pedestals"
             :description "No top. 27.5\"H. Specify door hinge left/right. A - Double door pedestal for credenzas. B - CPU Cabinet for credenzas w/ grommet Inside 10 | 16 | 22.5\" H B - CPU Cabinet for desks Inside 10 | 22 | 22.5\" H C - Hinged door cabinet for credenzas"
             :item [{:number "AT2410 (A)" :total_width 36 :total_depth 18 :price 1143 :weight 105}
                    {:number "AT2411 (B)" :total_width 12 :total_depth 18 :price 593 :weight 65}
                    {:number "AT2412 (B)" :total_width 12 :total_depth 24 :price 656 :weight 70}
                    {:number "AT2413 (C)" :total_width 18.5 :total_depth 18 :price 718 :weight 70}]
             :option[{:name "012"} {:name "010"} {:name "100"}]}

            {:line "Classic"
             :category "Part"
             :name "Desk Top"
             :description "Tops have a 2\" builtup hardwood edge with a configurated edge on two long sides."
             :item [{:number "AT2420" :total_width 60 :total_depth 30 :price 721 :weight 60}
                    {:number "AT2421" :total_width 66 :total_depth 30 :price 1124 :weight 65}
                    {:number "AT2422" :total_width 66 :total_depth 36 :price 1251 :weight 75}
                    {:number "AT2423" :total_width 72 :total_depth 36 :price 1495 :weight 85}
                    {:number "AT2424" :total_width 78 :total_depth 36 :price 1671 :weight 90}]
             :option[{:name "033a"}]}

            {:line "Classic"
             :category "Part"
             :name "Bullet End Desk Top"
             :description "Tops have a 2\" builtup hardwood edge with a configurated edge on two long sides and bullet end"
             :item [{:number "AT2425" :total_width 30 :total_depth 60 :price 2069 :weight 55}
                    {:number "AT2426" :total_width 72 :total_depth 36 :price 2333 :weight 75}
                    {:number "AT2427" :total_width 84 :total_depth 36 :price 2743 :weight 95}
                    {:number "AT2428" :total_width 90 :total_depth 42 :price 2957 :weight 125}
                    {:number "AT2429" :total_width 96 :total_depth 42 :price 3153 :weight 150}]
             :option[{:name "033a"}]}

            {:line "Classic"
             :category "Part"
             :name "Credenza/Bridge Top"
             :description "Tops have a 2\" builtup hardwood edge with a configurated edge on one long side."
             :item [{:number "AT2440" :total_width 38.5 :total_depth 19 :price 616 :weight 20}
                    {:number "AT2441" :total_width 48 :total_depth 19 :price 704 :weight 30}
                    {:number "AT2442" :total_width 60 :total_depth 19 :price 794 :weight 40}
                    {:number "AT2443" :total_width 71 :total_depth 19 :price 1059 :weight 45}
                    {:number "AT2444" :total_width 84 :total_depth 19 :price 1321 :weight 55}
                    {:number "AT2445" :total_width 96 :total_depth 19 :price 1586 :weight 75}
                    {:number "AT2446" :total_width 120 :total_depth 19 :price 1933 :weight 110}
                    {:number "AT2447" :total_width 144 :total_depth 19 :price 2305 :weight 125}
                    {:number "AT2448" :total_width 38.5 :total_depth 25 :price 676 :weight 23}
                    {:number "AT2449" :total_width 48 :total_depth 25 :price 776 :weight 35}
                    {:number "AT2450" :total_width 60 :total_depth 25 :price 872 :weight 46}
                    {:number "AT2451" :total_width 71 :total_depth 25 :price 1153 :weight 52}
                    {:number "AT2452" :total_width 84 :total_depth 25 :price 1454 :weight 63}
                    {:number "AT2453" :total_width 96 :total_depth 25 :price 1744 :weight 86}
                    {:number "AT2454" :total_width 120 :total_depth 25 :price 2126 :weight 127}
                    {:number "AT2455" :total_width 144 :total_depth 25 :price 2528 :weight 145}]}

            {:line "Kastina"
             :category "Desk"
             :name "Curved Starburst Top with Angled Ends Double Pedestal Desk"
             :item [{:number "AK3000" :total_width 84 :total_depth 36 :kneespace 35 :price 6192 :weight 375}
                    {:number "AK3001" :total_width 92 :total_depth 36 :kneespace 46 :price 7109 :weight 420}]
             :option[{:name "033a"} {:name "013"}]}

            {:line "Kastina"
             :category "Desk"
             :name "Bowshaped Starburst Top Double Pedestal Desk"
             :item [{:number "AK3010" :total_width 72 :total_depth 42 :total_height 36 :kneespace 28 :price 5504 :weight 340}
                    {:number "AK3011" :total_width 84 :total_depth 42 :total_height 36 :kneespace 35 :price 5962 :weight 375}
                    {:number "AK3012" :total_width 92 :total_depth 42 :total_height 36 :kneespace 46 :price 6880 :weight 420}]
             :option[{:name "033a"} {:name "013"}]}

            {:line "Kastina"
             :category "Desk"
             :name "Starburst Top Double Pedestal Desk"
             :item [{:number "AK3020" :total_width 72 :total_depth 36 :kneespace 30 :price 4827 :weight 330}
                    {:number "AK3021" :total_width 78 :total_depth 36 :kneespace 36 :price 5412 :weight 350}
                    {:number "AK3022" :total_width 84 :total_depth 36 :kneespace 36 :price 6066 :weight 375}]
             :option[{:name "033a"} {:name "013"}]}

            {:line "Kastina"
             :category "Desk"
             :name "Starburst Top Single Pedestal Desk"
             :description "For use with returns. Specify: Pedestal left or right."
             :item [{:number "AK3030" :total_width 72 :total_depth 36 :kneespace 34 :price 4701 :weight 275}
                    {:number "AK3031" :total_width 78 :total_depth 36 :kneespace 36 :price 5286 :weight 300}
                    {:number "AK3032" :total_width 84 :total_depth 36 :kneespace 36 :price 5940 :weight 325}]
             :option[{:name "033a"} {:name "013"}]}

            {:line "Kastina"
             :category "Bridge"
             :name "Bridge Top"
             :description "Tops have a 2.25\" builtup hardwood edge with a configurated edge on one long side. For 19\" tops, deduct 10%"
             :item [{:number "AK3100" :total_width 36 :total_depth 25 :price 674 :weight 25}
                    {:number "AK3101" :total_width 48 :total_depth 25 :price 774 :weight 35}
                    {:number "AK3102" :total_width 60 :total_depth 25 :price 870 :weight 46}
                    {:number "AK3103" :total_width 72 :total_depth 25 :price 1146 :weight 52}
                    {:number "AK3104" :total_width 84 :total_depth 25 :price 1422 :weight 65}]
             :option[{:name "033a"} {:name "020"} {:name "030"} {:name "013"}]}

            {:line "Kastina"
             :category "Return"
             :name "Return with box/box/file pedestal"
             :item [{:number "AK3110" :total_width 48 :total_depth 19 :kneespace 30 :price 1812 :weight 120}
                    {:number "AK3111" :total_width 48 :total_depth 25 :kneespace 30 :price 2169 :weight 135}
                    {:number "AK3112" :total_width 60 :total_depth 19 :kneespace 42 :price 1945 :weight 130}
                    {:number "AK3113" :total_width 60 :total_depth 25 :kneespace 42 :price 2292 :weight 145}]
             :option[{:name "033a"} {:name "030"}]}

            {:line "Kastina"
             :category "Return"
             :name "Return with 2 box/box/file pedestals"
             :item [{:number "AK3115" :total_width 48 :total_depth 19 :kneespace 13 :price 2844 :weight 195}
                    {:number "AK3116" :total_width 48 :total_depth 25 :kneespace 13 :price 3201 :weight 205}
                    {:number "AK3117" :total_width 60 :total_depth 19 :kneespace 25 :price 2977 :weight 210}
                    {:number "AK3118" :total_width 60 :total_depth 25 :kneespace 25 :price 3324 :weight 220}]
             :option[{:name "033a"}]}

            {:line "Kastina"
             :category "Return"
             :name "Return with 36” lateral file"
             :description "For 30\" lateral file, deduct $75."
             :item [{:number "AK3120" :total_width 48 :total_depth 19 :kneespace 11 :price 2292 :weight 210}
                    {:number "AK3121" :total_width 48 :total_depth 25 :kneespace 11 :price 2534 :weight 220}
                    {:number "AK3122" :total_width 60 :total_depth 19 :kneespace 21 :price 2549 :weight 215}
                    {:number "AK3123" :total_width 60 :total_depth 25 :kneespace 21 :price 2804 :weight 225}]
             :option[{:name "033a"}]}

            {:line "Kastina"
             :category "Part"
             :name "Modesty Panel"
             :description "27\" high. Use multiples for larger modesty panels."
             :item [{:number "AK3130" :total_width 24 :price 200 :weight 15}
                    {:number "AK3131" :total_width 30 :price 238 :weight 20}
                    {:number "AK3132" :total_width 36 :price 315 :weight 25}
                    {:number "AK3133" :total_width 42 :price 363 :weight 30}
                    {:number "AK3134" :total_width 48 :price 438 :weight 35}]}

            {:line "Kastina"
             :category "Part"
             :name "Wood Panel Leg"
             :item [{:number "AK3140" :total_width 18 :price 270 :weight 25}
                    {:number "AK3141" :total_width 24 :price 312 :weight 31}
                    {:number "AK3142" :total_width 28 :price 345 :weight 36}]}

            {:line "Kastina"
             :category "Credenza"
             :name "Wall Unit"
             :description "C - Comb. Credenza 72 | 19\" H - Hinged Door Cabinet 72 | 15 | 18\"H HD - Door Credenza 36 | 19\" K - Bookcase 36 | 12 | 42\"H L - Lateral File 36 | 19\" Optional: HD, C & L 25\" deep: + $1310. Optional: 84\" OA ht.: +10%"
             :item [{:number "AK3190" :price 14454 :weight 1045}]
             :option[{:name "010"} {:name "012"} {:name "033a"} {:name "070"} {:name "071"} {:name "072"} {:name "A34"}]}

            {:line "Kastina"
             :category "Credenza"
             :name "Wall Unit"
             :description "W - Writing Credenza 72 | 19\" with two box/box/file pedestals and keyboard drawer H - Hinged Door Cabinet 72 | 15 | 18\"H G - Wardrobe/Storage 36 | 19 | 84\"H Optional: 84\" OA ht.: + 10%"
             :item [{:number "AK3191" :price 13554 :weight 850}]
             :option[{:name "010"} {:name "012"} {:name "033a"} {:name "070"} {:name "071"} {:name "072"} {:name "A34"}]}

            {:line "Kastina"
             :category "Credenza"
             :name "Wall Unit"
             :description "HDL - Storage/File cabinet 36 | 19 | 72\"H G - Wardrobe/Storage 36 | 19 | 72\"H Optional: 84\" OA ht.: + 10%"
             :item [{:number "AK3192" :price 11301 :weight 600}]
             :option[{:name "010"} {:name "012"} {:name "033a"} {:name "070"} {:name "071"} {:name "072"} {:name "A34"}]}

            {:line "Kastina"
             :category "Credenza"
             :name "Wall Unit"
             :description "C - Combination Credenza 2 - 36\" lateral files 1 - 36\" hinged door H - Hutch with 4 glass doors and open shelves  in center. Optional: 84\" OA ht. + 10%"
             :item [{:number "AK3193" :price 12105 :weight 750}]
             :option[{:name "010"} {:name "012"} {:name "033a"} {:name "070"} {:name "072"} {:name "030"}]}

            {:line "Kastina"
             :category "Credenza"
             :name "Wall Unit"
             :description "W - Writing Credenza w/ 2 - 36\" lateral files 144 | 19 | 29 1/2\"H H - Hutch with 4 wood doors, open shelves, 4 glass doors, facia and organizers. 144 | 15 | 42\"H Optional: 84\" OA ht. + 10%"
             :item [{:number "AK3194" :price 13920 :weight 870}]
             :option[{:name "010"} {:name "012"} {:name "033a"} {:name "070"} {:name "072"} {:name "030"}]}

            {:line "Kastina"
             :category "Credenza"
             :name "Wall Unit"
             :description "W - Writing Credenza with 2- 36\" lateral files 2 - 2 box/file pedestals 144 | 19\"H H - Hutch with 4 wood doors 4 glass doors and facia. 144 | 15 | 42\"H Optional: 84\" OA ht. + 10%"
             :item [{:number "AK3195" :price 16439 :weight 1075}]
             :option[{:name "010"} {:name "012"} {:name "033a"} {:name "070"} {:name "072"} {:name "030"}]}

            {:line "Kastina"
             :category "Credenza"
             :name "Combination Credenza"
             :item [{:number "AK3040" :total_width 72 :total_depth 19 :kneespace 2 :price 4358 :weight 325}
                    {:number "AK3041" :total_width 72 :total_depth 25 :kneespace 2 :price 4759 :weight 375}
                    {:number "AK3042" :total_width 60 :total_depth 19 :kneespace 2 :price 4128 :weight 300}
                    {:number "AK3043" :total_width 60 :total_depth 25 :kneespace 2 :price 4517 :weight 350}]
             :option[{:name "012"} {:name "011"} {:name "033a"}]}

            {:line "Kastina"
             :category "Credenza"
             :name "Cabinet Credenza"
             :item [{:number "AK3050" :total_width 38 :total_depth 19 :door 2 :price 1811 :weight 160}
                        {:number "AK3051" :total_width 62 :total_depth 19 :door 4 :price 3554 :weight 240}
                        {:number "AK3052" :total_width 72 :total_depth 19 :door 4 :price 3669 :weight 260}
                        {:number "AK3053" :total_width 38 :total_depth 25 :door 2 :price 2001 :weight 180}
                        {:number "AK3054" :total_width 62 :total_depth 25 :door 4 :price 3899 :weight 260}
                        {:number "AK3055" :total_width 72 :total_depth 25 :door 4 :price 4013 :weight 280}]
             :option[{:name "011"} {:name "033a"}]}

            {:line "Kastina"
             :category "File"
             :name "Lateral File"
             :description "29.5\" high"
             :item [{:number "AK3060" :total_width 38 :total_depth 19 :price 2133 :weight 190}
                    {:number "AK3061" :total_width 62 :total_depth 19 :price 3876 :weight 340}
                    {:number "AK3062" :total_width 72 :total_depth 19 :price 4116 :weight 375}]
             :option[{:name "012"}]}

            {:line "Kastina"
             :category "Credenza"
             :name "Writing Credenza"
             :item [{:number "AK3065" :total_width 70.5 :total_depth 19 :price 3784 :weight 250}
                    {:number "AK3066" :total_width 70.5 :total_depth 25 :price 4059 :weight 265}]
             :option[{:name "012"}]}

            {:line "Kastina"
             :category "Credenza"
             :name "Conference Room Buffet Credenza"
             :description "36\" High Shipped K/D"
             :item [{:number "AK3070" :total_width 38 :total_depth 19 :door 2 :price 3188 :weight 175}
                    {:number "AK3071" :total_width 74 :total_depth 19 :door 4 :price 4891 :weight 330}
                    {:number "AK3072" :total_width 110 :total_depth 19 :door 6 :price 8485 :weight 495}
                    {:number "AK3075" :total_width 38 :total_depth 25 :door 2 :price 3507 :weight 215}
                    {:number "AK3076" :total_width 74 :total_depth 25 :door 4 :price 5254 :weight 360}
                    {:number "AK3077" :total_width 110 :total_depth 25 :door 6 :price 9345 :weight 530}]
             :option[{:name "013"} {:name "011"} {:name "033a"}]}

            {:line "Kastina"
             :category "Table"
             :name "Computer Corner Table"
             :description "with keyboard drawer and grommet. Angled work edge is 30\" wide. To be used with 25\" returns and credenzas"
             :item [{:number "AK3150" :total_width 40 :total_depth 40 :price 2293 :weight 195}
                    {:number "AK3151" :total_width 48 :total_depth 48 :price 3086 :weight 240}]}

            {:line "Kastina"
             :category "Hutch"
             :name "Hinged Door Hutch"
             :description "42\" high | 15\" deep"
             :item [{:number "AK3155" :total_width 38 :total_depth 15 :door 2 :price 1665 :weight 95}
                    {:number "AK3156" :total_width 48 :total_depth 15 :door 3 :price 2764 :weight 150}
                    {:number "AK3157" :total_width 60 :total_depth 15 :door 4 :price 3575 :weight 170}
                    {:number "AK3158" :total_width 72 :total_depth 15 :door 4 :price 3726 :weight 190}]
             :option[{:name "070"} {:name "033a"} {:name "072"}]}

            {:line "Kastina"
             :category "Cabinet"
             :name "Wall Hung Cabinet with Hinged Doors"
             :item [{:number "AK3160" :total_width 38 :door 2 :price 1458 :weight 60}
                    {:number "AK3161" :total_width 48 :door 3 :price 1878 :weight 75}
                    {:number "AK3162" :total_width 60 :door 4 :price 2216 :weight 95}
                    {:number "AK3163" :total_width 72 :door 4 :price 2408 :weight 105}]
             :option[{:name "070"} {:name "033a"} {:name "072"}]}

            {:line "Kastina"
             :category "Cabinet"
             :name "Storage/Wardrobe with fixed shelf and rod"
             :description "1/3 is storage. 36\" wide. 19\" deep units have front to back clothing rod."
             :item [{:number "AK3165" :total_height 72 :total_width 19 :price 3380 :weight 195}
                    {:number "AK3166" :total_height 84 :total_width 19 :price 3644 :weight 205}
                    {:number "AK3167" :total_height 72 :total_width 25 :price 3784 :weight 235}
                    {:number "AK3168" :total_height 84 :total_width 25 :price 4121 :weight 250}]
             :option[{:name "033a"}]}

            {:line "Kastina"
             :category "Bookcase"
             :name "Open Vertical Bookcase"
             :description "13\" OA deep"
             :item [{:number "AK3170" :total_height 30 :total_width 32 :price 847 :weight 50}
                    {:number "AK3171" :total_height 42 :total_width 32 :price 1146 :weight 80}
                    {:number "AK3172" :total_height 60 :total_width 32 :price 1413 :weight 100}
                    {:number "AK3173" :total_height 72 :total_width 32 :price 1591 :weight 120}
                    {:number "AK3174" :total_height 84 :total_width 32 :price 1747 :weight 130}
                    {:number "AK3175" :total_height 30 :total_width 38 :price 905 :weight 60}
                    {:number "AK3176" :total_height 42 :total_width 38 :price 1213 :weight 90}
                    {:number "AK3177" :total_height 60 :total_width 38 :price 1488 :weight 110}
                    {:number "AK3178" :total_height 72 :total_width 38 :price 1720 :weight 130}
                    {:number "AK3179" :total_height 84 :total_width 38 :price 1995 :weight 140}]
             :option[{:name "A47"}]}

            {:line "Stellar"
             :category "Desk"
             :name "Double Pedestal Desk With glass modesty panel"
             :description "Two B/B/F pedestals. Locks in each pedestal. Frosted glass or wood modesty panel."
             :item [{:number "AS7000" :total_width 72 :total_depth 36 :kneespace 36 :price 3927 :weight 340}
                    {:number "AS7001" :total_width 78 :total_depth 36 :kneespace 42 :price 4200 :weight 360}]
             :option[{:name "013"} {:name "020"} {:name "033a"} {:name "A42"} {:name "A50"}]}

            {:line "Stellar"
             :category "Desk"
             :name "Double Pedestal Desk With wood modesty panel"
             :description "Two B/B/F pedestals. Locks in each pedestal. Frosted glass or wood modesty panel."
             :item [{:number "AS7005" :total_width 72 :total_depth 36 :kneespace 36 :price 3675 :weight 340}
                    {:number "AS7006" :total_width 78 :total_depth 36 :kneespace 42 :price 3885 :weight 360}]
             :option[{:name "013"} {:name "020"} {:name "033a"} {:name "A42"} {:name "A50"}]}

            {:line "Stellar"
             :category "Desk"
             :name "Double Pedestal Desk Starburst Top With glass modesty panel"
             :description "Two B/B/F pedestals. Locks in each pedestal. Frosted glass or wood modesty panel."
             :item [{:number "AS7010" :total_width 72 :total_depth 36 :kneespace 36 :price 4527 :weight 340}
                    {:number "AS7011" :total_width 78 :total_depth 36 :kneespace 42 :price 4830 :weight 360}]
             :option[{:name "013"} {:name "020"} {:name "033a"} {:name "A42"} {:name "A50"}]}

            {:line "Stellar"
             :category "Desk"
             :name "Double Pedestal Desk Starburst Top With wood modesty panel"
             :description "Two B/B/F pedestals. Locks in each pedestal. Frosted glass or wood modesty panel."
             :item [{:number "AS7015" :total_width 72 :total_depth 36 :kneespace 36 :price 4220 :weight 340}
                    {:number "AS7016" :total_width 78 :total_depth 36 :kneespace 42 :price 4467 :weight 360}]
             :option[{:name "013"} {:name "020"} {:name "033a"} {:name "A42"} {:name "A50"}]}

            {:line "Stellar"
             :category "Desk"
             :name "Double Pedestal Desk Bowfront Top With glass modesty panel"
             :description "Two B/B/F pedestals. Locks in each pedestal. Frosted glass or wood modesty panel."
             :item [{:number "AS7020" :total_width 72 :total_depth 42 :total_height 36 :kneespace 36 :price 4672 :weight 340}
                    {:number "AS7021" :total_width 78 :total_depth 42 :total_height 36 :kneespace 42 :price 4905 :weight 360}]
             :option[{:name "013"} {:name "020"} {:name "033a"} {:name "A42"} {:name "A50"}]}

            {:line "Stellar"
             :category "Desk"
             :name "Double Pedestal Desk Bowfront Top With wood modesty panel"
             :description "Two B/B/F pedestals. Locks in each pedestal. Frosted glass or wood modesty panel."
             :item [{:number "AS7025" :total_width 72 :total_depth 42 :total_height 36 :kneespace 36 :price 4410 :weight 340}
                    {:number "AS7026" :total_width 78 :total_depth 42 :total_height 36 :kneespace 42 :price 4630 :weight 360}]
             :option[{:name "013"} {:name "020"} {:name "033a"} {:name "A42"} {:name "A50"}]}

            {:line "Stellar"
             :category "Desk"
             :name "Double Pedestal Desk Starburst Bowfront Top With glass modesty panel"
             :description "Two B/B/F pedestals. Locks in each pedestal. Frosted glass or wood modesty panel."
             :item [{:number "AS7030" :total_width 72 :total_depth 42 :total_height 36 :kneespace 36 :price 5372 :weight 340}
                    {:number "AS7031" :total_width 78 :total_depth 42 :total_height 36 :kneespace 42 :price 5640 :weight 360}]
             :option[{:name "013"} {:name "020"} {:name "033a"} {:name "A42"} {:name "A50"}]}

            {:line "Stellar"
             :category "Desk"
             :name "Double Pedestal Desk Starburst Bowfront Top With wood modesty panel"
             :description "Two B/B/F pedestals. Locks in each pedestal. Frosted glass or wood modesty panel."
             :item [{:number "AS7035" :total_width 72 :total_depth 42 :total_height 36 :kneespace 36 :price 5071 :weight 340}
                    {:number "AS7036" :total_width 78 :total_depth 42 :total_height 36 :kneespace 42 :price 5324 :weight 360}]
             :option[{:name "013"} {:name "020"} {:name "033a"} {:name "A42"} {:name "A50"}]}

            {:line "Stellar"
             :category "Desk"
             :name "Single Pedestal Desk With glass modesty panel"
             :description "B/B/F pedestal and open storage pedestal. No locks. Frosted glass or wood modesty panel."
             :item [{:number "AS7040" :total_width 72 :total_depth 36 :total_height 36 :price 3570 :weight 275}
                    {:number "AS7041" :total_width 78 :total_depth 36 :total_height 42 :price 3832 :weight 300}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "033a"} {:name "A42"} {:name "A50"}]}

            {:line "Stellar"
             :category "Desk"
             :name "Single Pedestal Desk With wood modesty panel"
             :description "B/B/F pedestal and open storage pedestal. No locks. Frosted glass or wood modesty panel."
             :item [{:number "AS7045" :total_width 72 :total_depth 36 :kneespace 36 :price 3307 :weight 275}
                    {:number "AS7046" :total_width 78 :total_depth 36 :kneespace 42 :price 3517 :weight 300}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "033a"} {:name "A42"} {:name "A50"}]}

            {:line "Stellar"
             :category "Desk"
             :name "Single Pedestal Desk Starburst Top With glass modesty panel"
             :description "B/B/F pedestal and open storage pedestal. No locks. Frosted glass or wood modesty panel."
             :item [{:number "AS7050" :total_width 72 :total_depth 36 :kneespace 36 :price 4105 :weight 275}
                    {:number "AS7051" :total_width 78 :total_depth 36 :kneespace 42 :price 4406 :weight 300}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "033a"} {:name "A42"} {:name "A50"}]}

            {:line "Stellar"
             :category "Desk"
             :name "Single Pedestal Desk Starburst Top With wood modesty panel"
             :description "B/B/F pedestal and open storage pedestal. No locks. Frosted glass or wood modesty panel."
             :item [{:number "AS7055" :total_width 72 :total_depth 36 :kneespace 36 :price 3803 :weight 275}
                    {:number "AS7056" :total_width 78 :total_depth 36 :kneespace 42 :price 4044 :weight 300}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "033a"} {:name "A42"} {:name "A50"}]}

            {:line "Stellar"
             :category "Desk"
             :name "Single Pedestal Desk Bowfront Top With glass modesty panel"
             :description "B/B/F pedestal and open storage pedestal. No locks. Frosted glass or wood modesty panel."
             :item [{:number "AS7060" :total_width 72 :total_depth 42 :total_height 36 :kneespace 36 :price 4252 :weight 275}
                    {:number "AS7061" :total_width 78 :total_depth 42 :total_height 36 :kneespace 42 :price 4464 :weight 300}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "033a"} {:name "A42"} {:name "A50"}]}

            {:line "Stellar"
             :category "Desk"
             :name "Single Pedestal Desk Bowfront Top With wood modesty panel"
             :description "B/B/F pedestal and open storage pedestal. No locks. Frosted glass or wood modesty panel."
             :item [{:number "AS7065" :total_width 72 :total_depth 42 :total_height 36 :kneespace 36 :price 3990 :weight 275}
                    {:number "AS7066" :total_width 78 :total_depth 42 :total_height 36 :kneespace 42 :price 4180 :weight 300}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "033a"} {:name "A42"} {:name "A50"}]}

            {:line "Stellar"
             :category "Desk"
             :name "Single Pedestal Desk Starburst Bowfront Top With glass modesty panel"
             :description "B/B/F pedestal and open storage pedestal. No locks. Frosted glass or wood modesty panel."
             :item [{:number "AS7070" :total_width 72 :total_depth 42 :total_height 36 :kneespace 36 :price 4889 :weight 275}
                    {:number "AS7071" :total_width 78 :total_depth 42 :total_height 36 :kneespace 42 :price 5133 :weight 300}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "033a"} {:name "A42"} {:name "A50"}]}

            {:line "Stellar"
             :category "Desk"
             :name "Single Pedestal Desk Starburst Bowfront Top With wood modesty panel"
             :description "B/B/F pedestal and open storage pedestal. No locks. Frosted glass or wood modesty panel."
             :item [{:number "AS7075" :total_width 72 :total_depth 42 :total_height 36 :kneespace 36 :price 4709 :weight 275}
                    {:number "AS7076" :total_width 78 :total_depth 42 :total_height 36 :kneespace 42 :price 4933 :weight 300}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "033a"} {:name "A42"} {:name "A50"}]}

            {:line "Stellar"
             :category "Return"
             :name "Bridge Return"
             :description "For connecting single pedestal desks with credenzas to create a U workstation."
             :item [{:number "AS7100" :total_width 42 :total_depth 20 :kneespace 42 :price 1124 :weight 100}
                    {:number "AS7101" :total_width 48 :total_depth 20 :kneespace 48 :price 1239 :weight 110}]
             :option[{:name "013"} {:name "020"} {:name "030"} {:name "031"} {:name "032"} {:name "033a"}]}

            {:line "Stellar"
             :category "Return"
             :name "Return with"
             :description "File/file Pedestal Specify left or right return"
             :item [{:number "AS7105" :total_width 42 :total_depth 20 :kneespace 23.5 :price 1692 :weight 160}
                    {:number "AS7106" :total_width 48 :total_depth 20 :kneespace 29 :price 1852 :weight 170}]
             :option[{:name "012"} {:name "030"} {:name "031"} {:name "032"} {:name "033a"}]}

            {:line "Stellar"
             :category "Return"
             :name "Return with 36” Lateral File Pedestal"
             :description "For 30” lateral file deduct $75. Specify left or right return"
             :item [{:number "AS7110" :total_width 42 :total_depth 20 :kneespace 6 :price 2153 :weight 185}
                    {:number "AS7111" :total_width 48 :total_depth 20 :kneespace 12 :price 2313 :weight 200}]
             :option[{:name "012"} {:name "033a"}]}

            {:line "Stellar"
             :category "Table"
             :name "Computer Corner Table"
             :description "with mini-keyboard arm  and grommet. angled work edge is 30” wide"
             :item [{:number "AS7115" :total_width 40 :total_depth 40 :kneespace 30 :price 2409 :weight 195}
                    {:number "AS7116" :total_width 48 :total_depth 48 :kneespace 31 :price 3238 :weight 230}]
             :option[{:name "031"}]}

            {:line "Stellar"
             :category "Credenza"
             :name "Rear Unit with 36” Lateral File Pedestal"
             :description "For 30” lateral file deduct $75. Specify pedestal left or right."
             :item [{:number "AS7120" :total_width 72 :total_depth 20 :kneespace 30 :price 3304 :weight 265}
                    {:number "AS7121" :total_width 78 :total_depth 20 :kneespace 36 :price 3680 :weight 275}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "031"} {:name "032"} {:name "033a"} {:name "A42"} {:name "A50"}]}

            {:line "Stellar"
             :category "Credenza"
             :name "Rear Unit with 36” Storage Pedestal"
             :description "Two wood hinged doors. For 30” wide storage pedestal deduct $35. Specify pedestal left or right."
             :item [{:number "AS7125" :total_width 72 :total_depth 20 :kneespace 30 :price 3304 :weight 250}
                    {:number "AS7126" :total_width 78 :total_depth 20 :kneespace 36 :price 3680 :weight 260}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "031"} {:name "032"} {:name "033a"} {:name "A42"} {:name "A50"}]}

            {:line "Stellar"
             :category "Credenza"
             :name "Rear Unit with one File/file pedestal and one Storage Pedestal One wood hinged door."
             :description "Specify pedestal left or right."
             :item [{:number "AS7130" :total_width 72 :total_depth 20 :kneespace 30 :price 3337 :weight 260}
                    {:number "AS7131" :total_width 78 :total_depth 20 :kneespace 36 :price 3713 :weight 270}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "031"} {:name "032"} {:name "033a"} {:name "A42"} {:name "A50"}]}

            {:line "Stellar"
             :category "Credenza"
             :name "Rear Unit with 36” Storage Pedestal with Two Glass Doors"
             :description "Specify pedestal left or right."
             :item [{:number "AS7135" :total_width 72 :total_depth 20 :kneespace 30 :price 3504 :weight 250}
                    {:number "AS7136" :total_width 78 :total_depth 20 :kneespace 36 :price 3880 :weight 260}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "031"} {:name "032"} {:name "033a"} {:name "A42"} {:name "A50"}]}

            {:line "Stellar"
             :category "Credenza"
             :name "Cabinet Credenza With Wood Doors"
             :description "29” high. One adjustable shelf behind each pair of doors"
             :item [{:number "AS7140" :total_width 60 :total_depth 20 :door 4 :price 2789 :weight 225}
                    {:number "AS7141" :total_width 72 :total_depth 20 :door 4 :price 3347 :weight 270}
                    {:number "AS7142" :total_width 84 :total_depth 20 :door 4 :price 4474 :weight 300}]
             :option[{:name "010"} {:name "033a"} {:name "A42"} {:name "A50"}]}

            {:line "Stellar"
             :category "Credenza"
             :name "Cabinet Credenza With Satin Glass Doors"
             :description "29” high. One adjustable shelf behind each pair of doors"
             :item [{:number "AS7145" :total_width 60 :total_depth 20 :door 4 :price 3139 :weight 225}
                    {:number "AS7146" :total_width 72 :total_depth 20 :door 4 :price 3697 :weight 270}
                    {:number "AS7147" :total_width 84 :total_depth 20 :door 4 :price 4824 :weight 300}]
             :option[{:name "010"} {:name "033a"} {:name "A42"} {:name "A50"}]}

            {:line "Stellar"
             :category "Credenza"
             :name "Cabinet Credenza With Two Satin Glass Doors in center and One Wood Door on each end"
             :description "29” high. One adjustable shelf behind each pair of doors"
             :item [{:number "AS7150" :total_width 60 :total_depth 20 :door 4 :price 2989 :weight 225}
                    {:number "AS7151" :total_width 72 :total_depth 20 :door 4 :price 3547 :weight 270}
                    {:number "AS7152" :total_width 84 :total_depth 20 :door 4 :price 4674 :weight 300}]
             :option[{:name "010"} {:name "033a"} {:name "A42"} {:name "A50"}]}

            {:line "Stellar"
             :category "Credenza"
             :name "Combination Credenza"
             :description "29” high. Two wood doors, four box drawers, and two file drawers."
             :item [{:number "AS7160" :total_width 60 :total_depth 20 :door 4 :price 3775 :weight 250}
                    {:number "AS7161" :total_width 72 :total_depth 20 :door 4 :price 3900 :weight 325}]
             :option[{:name "010"} {:name "012"} {:name "A50"}]}

            {:line "Stellar"
             :category "Credenza"
             :name "Writing Credenza"
             :description "29” high. Four box drawers, and two file drawers"
             :item [{:number "AS7165" :total_width 60 :total_depth 20 :price 3259 :weight 225}
                    {:number "AS7166" :total_width 72 :total_depth 20 :price 3558 :weight 270}]
             :option[{:name "012"} {:name "033a"} {:name "A50"}]}

            {:line "Stellar"
             :category "Credenza"
             :name "Conference Room Buffet Credenza"
             :description "36” high. Wood doors and drawers"
             :item [{:number "AS7170" :total_width 36 :total_depth 20 :door 2 :price 2279 :weight 165}
                    {:number "AS7171" :total_width 72 :total_depth 20 :door 4 :price 3904 :weight 295}
                    {:number "AS7172" :total_width 108 :total_depth 20 :door 6 :price 6763 :weight 525}]
             :option[{:name "010"} {:name "013"} {:name "033a"} {:name "A50"}]}

            {:line "Stellar"
             :category "File"
             :name "Lateral file"
             :description "29” high. Two drawers. Filing is side to side, adjustable for letter and legal"
             :item [{:number "AS7180" :total_width 30 :total_depth 20 :price 1854 :weight 175}
                    {:number "AS7181" :total_width 36 :total_depth 20 :price 1959 :weight 190}]
             :option[{:name "012"} {:name "A41"} {:name "A50"}]}

            {:line "Stellar"
             :category "File"
             :name "Lateral file"
             :description "29” high. Four drawers. Filing is side to side, adjustable for letter and legal."
             :item [{:number "AS7185" :total_width 60 :total_depth 20 :price 3559 :weight 350}
                    {:number "AS7186" :total_width 72 :total_depth 20 :price 3692 :weight 375}]
             :option[{:name "012"} {:name "A42"} {:name "A50"}]}

            {:line "Stellar"
             :category "File"
             :name "Lateral file"
             :description "42” high. Three drawers. Filing is side to side, adjustable for letter and legal. Shipped with anti-tilt safety system."
             :item [{:number "AS7190" :total_width 30 :total_depth 20 :price 2791 :weight 225}
                    {:number "AS7191" :total_width 36 :total_depth 20 :price 2951 :weight 240}]
             :option[{:name "012"} {:name "A41"} {:name "A50"}]}

            {:line "Stellar"
             :category "File"
             :name "Lateral file"
             :description "55” high. Four drawers. Filing is side to side, adjustable for letter and legal. Shipped with anti-tilt safety system."
             :item [{:number "AS7195" :total_width 30 :total_depth 20 :price 3785 :weight 295}
                    {:number "AS7196" :total_width 36 :total_depth 20 :price 4102 :weight 310}]
             :option[{:name "012"} {:name "A41"} {:name "A50"}]}

            {:line "Stellar"
             :category "Cabinet"
             :name "Wall Hung Cabinet With Wood Doors"
             :description "19.5” inside height. 21” outside height 13.5” inside depth Doors can be all wood, all frosted glass, or two glass doors in center and wood doors on each end."
             :item [{:number "AS7200" :total_width 30 :total_depth 15 :door 2 :price 1173 :weight 50}
                    {:number "AS7201" :total_width 36 :total_depth 15 :door 2 :price 1275 :weight 60}
                    {:number "AS7202" :total_width 60 :total_depth 15 :door 4 :price 1937 :weight 80}
                    {:number "AS7203" :total_width 72 :total_depth 15 :door 4 :price 2040 :weight 95}]
             :option[{:name "010"} {:name "070"}]}

            {:line "Stellar"
             :category "Cabinet"
             :name "Wall Hung Cabinet With Two Satin Glass and Two Wood Doors"
             :description "19.5” inside height. 21” outside height 13.5” inside depth Doors can be all wood, all frosted glass, or two glass doors in center and wood doors on each end."
             :item [{:number "AS7212" :total_width 60 :total_depth 15 :door 4 :price 2137 :weight 80}
                    {:number "AS7213" :total_width 72 :total_depth 15 :door 4 :price 2240 :weight 95}]
             :option[{:name "010"} {:name "070"}]}

            {:line "Stellar"
             :category "Cabinet"
             :name "Wall Hung Cabinet With Satin Glass Doors"
             :description "19.5” inside height. 21” outside height 13.5” inside depth Doors can be all wood, all frosted glass, or two glass doors in center and wood doors on each end."
             :item [{:number "AS7220" :total_width 30 :total_depth 15 :door 2 :price 1375 :weight 50}
                    {:number "AS7221" :total_width 36 :total_depth 15 :door 2 :price 1475 :weight 60}
                    {:number "AS7222" :total_width 60 :total_depth 15 :door 4 :price 2337 :weight 80}
                    {:number "AS7223" :total_width 72 :total_depth 15 :door 4 :price 2440 :weight 95}]
             :option[{:name "010"} {:name "070"}]}

            {:line "Stellar"
             :category "Cabinet"
             :name "Single Door Wardrobe"
             :description "Coat carrier and one adjustable shelf. 18” OA depth"
             :item [{:number "AS7230" :total_width 24 :total_depth 67.5 :door 1 :price 2295 :weight 145}
                    {:number "AS7231" :total_width 24 :total_depth 79.25 :door 1 :price 2856 :weight 165}]
             :option[{:name "011"}]}

            {:line "Stellar"
             :category "Cabinet"
             :name "Single Door Unit with Shelves"
             :description "4 adjustable shelves. 18” OA depth"
             :item [{:number "AS7235" :total_width 24 :total_depth 67.5 :door 1 :price 2448 :weight 150}
                    {:number "AS7236" :total_width 24 :total_depth 79.25 :door 1 :price 3034 :weight 170}]
             :option[{:name "011"}]}

            {:line "Stellar"
             :category "Cabinet"
             :name "Double Door Wardrobe/Storage Unit"
             :description "Right half storage with 4 adjustable shelves; left half wardrobe with coat carrier and one adjustable  24” OA depth"
             :item [{:number "AS7240" :total_width 36 :total_depth 67.5 :door 2 :price 3264 :weight 225}
                    {:number "AS7241" :total_width 36 :total_depth 79.25 :door 2 :price 4029 :weight 245}]
             :option[{:name "011"}]}

            {:line "Stellar"
             :category "Bookcase"
             :name "Open Horizontal Bookcase"
             :description "14” OA depth (top)"
             :item [{:number "AS7250" :total_width 36 :total_depth 29.5 :shelf 1 :price 1208 :weight 55}
                    {:number "AS7251" :total_width 42 :total_depth 29.5 :shelf 1 :price 1368 :weight 60}
                    {:number "AS7252" :total_width 48 :total_depth 29.5 :shelf 1 :price 1552 :weight 65}]
             :option[{:name "033a"} {:name "061"} {:name "A32"} {:name "A42"}]}

            {:line "Stellar"
             :category "Bookcase"
             :name "Open Vertical Bookcase"
             :description "14” OA depth (top)"
             :item [{:number "AS7255" :total_width 36 :total_depth 67.5 :shelf 4 :price 1675 :weight 125}
                    {:number "AS7256" :total_width 36 :total_depth 79.25 :shelf 5 :price 1859 :weight 140}]
             :option[{:name "010"} {:name "012"} {:name "033a"} {:name "060"} {:name "061"} {:name "A32"} {:name "A41"} {:name "A47"} {:name "A49"}]}

            {:line "Stellar"
             :category "Bookcase"
             :name "Open Bookcase With Glass Doors in bottom Section Only"
             :description "Glass or wood doors. 14” OA depth"
             :item [{:number "AS7260" :total_width 36 :total_depth 67.5 :shelf 2 :price 2206 :weight 150}
                    {:number "AS7261" :total_width 36 :total_depth 79.25 :shelf 2 :price 2398 :weight 180}]
             :option[{:name "010"} {:name "012"} {:name "033a"} {:name "060"} {:name "061"} {:name "A32"} {:name "A41"} {:name "A47"} {:name "A49"}]}

            {:line "Stellar"
             :category "Bookcase"
             :name "Open Bookcase With Wood Doors in bottom Section Only"
             :description "Glass or wood doors. 14” OA depth"
             :item [{:number "AS7265" :total_width 36 :total_depth 67.5 :shelf 2 :price 2006 :weight 150}
                    {:number "AS7266" :total_width 36 :total_depth 79.25 :shelf 2 :price 2198 :weight 180}]
             :option[{:name "010"} {:name "012"} {:name "033a"} {:name "060"} {:name "061"} {:name "A32"} {:name "A41"} {:name "A47"} {:name "A49"}]}

            {:line "Stellar"
             :category "File"
             :name "Double Door Storage Unit with Lateral File With Glass Doors"
             :description "Glass or wood doors. 20” OA depth"
             :item [{:number "AS7270" :total_width 36 :total_depth 67.5 :shelf 2 :price 3967 :weight 250}
                    {:number "AS7271" :total_width 36 :total_depth 79.25 :shelf 2 :price 4383 :weight 280}]
             :option[{:name "010"} {:name "012"} {:name "033a"} {:name "060"} {:name "061"} {:name "A32"} {:name "A41"} {:name "A47"} {:name "A49"}]}

            {:line "Stellar"
             :category "File"
             :name "Double Door Storage Unit with Lateral File With Wood Doors"
             :description "Glass or wood doors. 20” OA depth"
             :item [{:number "AS7272" :total_width 36 :total_depth 67.5 :shelf 2 :price 3767 :weight 250}
                    {:number "AS7273" :total_width 36 :total_depth 79.25 :shelf 2 :price 4183 :weight 280}]
             :option[{:name "010"} {:name "012"} {:name "033a"} {:name "060"} {:name "061"} {:name "A32"} {:name "A41"} {:name "A47"} {:name "A49"}]}

            {:line "Nova"
             :category "Desk"
             :name "Table Desk"
             :description "with center pencil drawer Wood panel legs."
             :item [{:number "AN9230" :total_width 66 :total_depth 30 :kneespace 26 :price 2495 :weight 200}
                    {:number "AN9240" :total_width 72 :total_depth 36 :kneespace 30 :price 2771 :weight 210}
                    {:number "AN9250" :total_width 84 :total_depth 36 :kneespace 36 :price 3131 :weight 230}]
             :option[{:name "013"} {:name "030"}]}

            {:line "Nova"
             :category "Desk"
             :name "Curved Top Table Desk"
             :description "with center pencil drawer Plain wood top. Wood panel legs. For parquet top, add 10%"
             :item [{:number "AN9161" :total_width 84 :total_depth 36 :kneespace 30 :price 2957 :weight 230}
                    {:number "AN9171" :total_width 92 :total_depth 36 :kneespace 36 :price 3214 :weight 245}]
             :option[{:name "013"} {:name "030"}]}

            {:line "Nova"
             :category "Desk"
             :name "Racetrack Table Desk"
             :description "with center pencil drawer Lock not available. For starburst top, add 20%"
             :item [{:number "AN9815" :total_width 72 :total_depth 36 :kneespace 28 :price 4489 :weight 205}
                    {:number "AN9816" :total_width 84 :total_depth 36 :kneespace 30 :price 5006 :weight 230}
                    {:number "AN9817" :total_width 96 :total_depth 42 :kneespace 34 :price 5659 :weight 250}]
             :option[{:name "A42"}]}

            {:line "Nova"
             :category "Desk"
             :name "Kidney-Shaped Table Desk"
             :description "with center pencil drawer Lock not available. For starburst top, add 20%"
             :item [{:number "AN9872" :total_width 72 :total_depth 36 :kneespace 26 :price 5252 :weight 220}
                    {:number "AN9878" :total_width 78 :total_depth 36 :kneespace 28 :price 5521 :weight 230}
                    {:number "AN9841" :total_width 84 :total_depth 42 :kneespace 32 :price 5797 :weight 240}
                    {:number "AN9842" :total_width 90 :total_depth 42 :kneespace 34 :price 6143 :weight 245}
                    {:number "AN9843" :total_width 96 :total_depth 42 :kneespace 36 :price 6211 :weight 250}]
             :option[{:name "A42"}]}

            {:line "Nova"
             :category "Desk"
             :name "Double Pedestal Desk Shadowbox Front"
             :item [{:number "AN9050" :total_width 66 :total_depth 30 :kneespace 28 :price 3549 :weight 320}
                    {:number "AN9060" :total_width 72 :total_depth 36 :kneespace 35 :price 3675 :weight 340}
                    {:number "AN9070" :total_width 84 :total_depth 36 :kneespace 47 :price 4046 :weight 375}]
             :option[{:name "013"} {:name "A42"}]}

            {:line "Nova"
             :category "Desk"
             :name "Double Pedestal Desk Straight Front"
             :item [{:number "AN9065" :total_width 66 :total_depth 30 :kneespace 28 :price 3371 :weight 320}
                    {:number "AN9066" :total_width 72 :total_depth 36 :kneespace 35 :price 3492 :weight 340}
                    {:number "AN9067" :total_width 84 :total_depth 36 :kneespace 47 :price 3852 :weight 375}]
             :option[{:name "033a"} {:name "013"}]}

            {:line "Nova"
             :category "Desk"
             :name "Double Pedestal Desk Bowshaped Top Shadowbox Front"
             :item [{:number "AN9052" :total_width 72 :total_depth 42 :total_height 36 :kneespace 35 :price 4094 :weight 340}
                    {:number "AN9053" :total_width 84 :total_depth 42 :total_height 36 :kneespace 47 :price 4466 :weight 375}
                    {:number "AN9054" :total_width 92 :total_depth 42 :total_height 36 :kneespace 47 :price 5056 :weight 420}]
             :option[{:name "033a"} {:name "013"}]}

            {:line "Nova"
             :category "Desk"
             :name "Double Pedestal Desk Bowshaped Top Straight Front"
             :item [{:number "AN9072" :total_width 72 :total_depth 42 :total_height 36 :kneespace 35 :price 3913 :weight 340}
                    {:number "AN9073" :total_width 84 :total_depth 42 :total_height 36 :kneespace 47 :price 4287 :weight 375}
                    {:number "AN9074" :total_width 92 :total_depth 42 :total_height 36 :kneespace 47 :price 4876 :weight 420}]
             :option[{:name "033a"} {:name "013"}]}

            {:line "Nova"
             :category "Desk"
             :name "Single Pedestal Desk Bowshaped Top Shadowbox Front"
             :description "Open storage pedestal"
             :item [{:number "AN9080" :total_width 66 :total_depth 36 :total_height 30 :kneespace 28 :price 3563 :weight 245}
                    {:number "AN9081" :total_width 66 :total_depth 42 :total_height 36 :kneespace 28 :price 3624 :weight 255}
                    {:number "AN9082" :total_width 72 :total_depth 42 :total_height 36 :kneespace 35 :price 3793 :weight 275}
                    {:number "AN9083" :total_width 78 :total_depth 42 :total_height 36 :kneespace 41 :price 4118 :weight 300}]
             :option[{:name "020"} {:name "013"} {:name "012"} {:name "A42"} {:name "033a"}]}

            {:line "Nova"
             :category "Desk"
             :name "Single Pedestal Desk Bowshaped Top Straight Front"
             :item [{:number "AN9085" :total_width 66 :total_depth 36 :total_height 30 :kneespace 28 :price 3142 :weight 230}
                    {:number "AN9086" :total_width 66 :total_depth 42 :total_height 36 :kneespace 28 :price 3203 :weight 240}
                    {:number "AN9087" :total_width 72 :total_depth 42 :total_height 36 :kneespace 35 :price 3492 :weight 260}
                    {:number "AN9088" :total_width 78 :total_depth 42 :total_height 36 :kneespace 41 :price 3696 :weight 275}]
             :option[{:name "020"} {:name "013"} {:name "012"} {:name "A42"} {:name "033a"}]}

            {:line "Nova"
             :category "Desk"
             :name "Single Pedestal Desk Shadowbox Front"
             :description "B/b/f pedestal with pull-out dictation shelf, and open storage pedestal. No lock. For use with any return shown on page 173. Specify left or right return"
             :item [{:number "AN9214" :total_width 66 :total_depth 30 :kneespace 28 :price 3260 :weight 245}
                    {:number "AN9215" :total_width 66 :total_depth 36 :kneespace 28 :price 3320 :weight 255}
                    {:number "AN9216" :total_width 72 :total_depth 36 :kneespace 35 :price 3386 :weight 275}
                    {:number "AN9217" :total_width 78 :total_depth 36 :kneespace 41 :price 3696 :weight 300}]
             :option[{:name "013"} {:name "012"} {:name "A42"} {:name "020"} {:name "033a"}]}

            {:line "Nova"
             :category "Desk"
             :name "Single Pedestal Desk Straight Front"
             :description "B/b/f pedestal with pull-out dictation shelf. No lock. 48\" and 54\" wide desks aren’t for use with returns. Specify ped. left or right."
             :item [{:number "AN9100" :total_width 48 :total_depth 24 :kneespace 24 :price 1972 :weight 175}
                    {:number "AN9120" :total_width 54 :total_depth 30 :kneespace 30 :price 2426 :weight 200}
                    {:number "AN9130" :total_width 60 :total_depth 30 :kneespace 22 :price 2504 :weight 205}
                    {:number "AN9104" :total_width 66 :total_depth 30 :kneespace 28 :price 2709 :weight 210}
                    {:number "AN9105" :total_width 66 :total_depth 36 :kneespace 28 :price 2902 :weight 220}
                    {:number "AN9106" :total_width 72 :total_depth 36 :kneespace 35 :price 3070 :weight 230}
                    {:number "AN9107" :total_width 78 :total_depth 36 :kneespace 41 :price 3274 :weight 260}]
             :option[{:name "013"} {:name "012"} {:name "A42"} {:name "020"} {:name "033a"}]}

            {:line "Nova"
             :category "Desk"
             :name "Bullet End Table Desk Wall Supported"
             :description "with pencil drawer. 6\" dia. black cylinder. Attached to wall with cleat. For wood cyl. add $273. Specify bullet end on left or right."
             :item [{:number "AN9800" :total_width 72 :total_depth 36 :kneespace 50 :price 2890 :weight 155}
                    {:number "AN9810" :total_width 84 :total_depth 36 :kneespace 62 :price 3160 :weight 180}
                    {:number "AN9820" :total_width 90 :total_depth 42 :kneespace 68 :price 3347 :weight 200}]
             :option[{:name "013"} {:name "A23"} {:name "081"} {:name "033a"}]}

            {:line "Nova"
             :category "Desk"
             :name "Bullet End Table Desk"
             :description "Center pencil drawer. 6\" dia. black cylinder. For wood cyl. add $273. For use with any return shown on page 173. Specify left or right return."
             :item [{:number "AN9270" :total_width 72 :total_depth 36 :kneespace 32 :price 3160 :weight 165}
                    {:number "AN9271" :total_width 84 :total_depth 36 :kneespace 44 :price 3413 :weight 190}
                    {:number "AN9272" :total_width 90 :total_depth 42 :kneespace 60 :price 3539 :weight 210}]
             :option[{:name "013"} {:name "A23"} {:name "081"} {:name "033a"}]}

            {:line "Nova"
             :category "Desk"
             :name "Expanded Bullet End Table Desk Self Supported"
             :description "with center pencil drawer. 6\" dia. black cylinder. For wood cyl. add $273. Specify bullet end on left or right."
             :item [{:number "AN9845" :total_width 72 :total_depth 36 :kneespace 40 :price 3492 :weight 170}
                    {:number "AN9846" :total_width 84 :total_depth 36 :kneespace 52 :price 3774 :weight 195}
                    {:number "AN9847" :total_width 90 :total_depth 42 :kneespace 58 :price 3901 :weight 215}]
             :option[{:name "013"} {:name "A23"} {:name "081"} {:name "033a"}]}

            {:line "Nova"
             :category "Bridge"
             :name "Credenza/Bridge Top"
             :description "Tops have a 2\" builtup hardwood edge with a configurated edge on one long side. To find cost of a custom length top, go to the next larger size"
             :item [{:number "AN9420" :total_width 36 :total_depth 19 :kneespace 36 :price 587 :weight 20}
                    {:number "AN9421" :total_width 48 :total_depth 19 :kneespace 48 :price 670 :weight 30}
                    {:number "AN9422" :total_width 60 :total_depth 19 :kneespace 60 :price 756 :weight 40}
                    {:number "AN9423" :total_width 72 :total_depth 19 :kneespace 72 :price 1009 :weight 45}
                    {:number "AN9424" :total_width 84 :total_depth 19 :kneespace 84 :price 1258 :weight 55}
                    {:number "AN9425" :total_width 96 :total_depth 19 :kneespace 96 :price 1510 :weight 75}
                    {:number "AN9426" :total_width 120 :total_depth 19 :kneespace 120 :price 1841 :weight 110}
                    {:number "AN9427" :total_width 144 :total_depth 19 :kneespace 144 :price 2195 :weight 125}
                    {:number "AN9435" :total_width 36 :total_depth 25 :kneespace 36 :price 644 :weight 23}
                    {:number "AN9436" :total_width 48 :total_depth 25 :kneespace 48 :price 739 :weight 35}
                    {:number "AN9437" :total_width 60 :total_depth 25 :kneespace 60 :price 830 :weight 46}
                    {:number "AN9438" :total_width 72 :total_depth 25 :kneespace 72 :price 1098 :weight 52}
                    {:number "AN9439" :total_width 84 :total_depth 25 :kneespace 84 :price 1385 :weight 63}
                    {:number "AN9448" :total_width 96 :total_depth 25 :kneespace 96 :price 1661 :weight 86}
                    {:number "AN9449" :total_width 120 :total_depth 25 :kneespace 120 :price 2025 :weight 127}
                    {:number "AN9428" :total_width 144 :total_depth 25 :kneespace 144 :price 2408 :weight 145}]
             :option[{:name "030"} {:name "032"} {:name "031"} {:name "033a"} {:name "012"} {:name "020"} {:name "013"}]}

            {:line "Nova"
             :category "Return"
             :name "Bridge Return"
             :description "For connecting single pedestal desks with credenzas to create a U workstation."
             :item [{:number "AN9241" :total_width 42 :total_depth 19 :kneespace 42 :price 1023 :weight 70}
                    {:number "AN9242" :total_width 48 :total_depth 19 :kneespace 48 :price 1128 :weight 80}
                    {:number "AN9243" :total_width 54 :total_depth 19 :kneespace 54 :price 1155 :weight 90}
                    {:number "AN9244" :total_width 60 :total_depth 19 :kneespace 60 :price 1198 :weight 95}
                    {:number "AN9245" :total_width 42 :total_depth 25 :kneespace 42 :price 1077 :weight 75}
                    {:number "AN9246" :total_width 48 :total_depth 25 :kneespace 48 :price 1198 :weight 85}
                    {:number "AN9247" :total_width 54 :total_depth 25 :kneespace 54 :price 1228 :weight 95}
                    {:number "AN9248" :total_width 60 :total_depth 25 :kneespace 60 :price 1282 :weight 100}]
             :option[{:name "030"} {:name "032"} {:name "031"} {:name "033a"} {:name "012"} {:name "020"} {:name "013"}]}

            {:line "Nova"
             :category "Return"
             :name "Return with box/box/file pedestal"
             :description "Specify left or right return, executive or secretarial ht. Option: File/file pedestal - no additional charge."
             :item [{:number "AN9223" :total_width 48 :total_depth 19 :kneespace 30 :price 1655 :weight 120}
                    {:number "AN9224" :total_width 60 :total_depth 19 :kneespace 42 :price 1776 :weight 130}
                    {:number "AN9233" :total_width 48 :total_depth 25 :kneespace 30 :price 1980 :weight 135}
                    {:number "AN9234" :total_width 60 :total_depth 25 :kneespace 42 :price 2106 :weight 145}]
             :option[{:name "A41"} {:name "033a"}]}

            {:line "Nova"
             :category "Return"
             :name "Return with 36\" lateral file"
             :description "Specify left or right return, and executive or secretarial height. For 30\" lateral file, deduct $75."
             :item [{:number "AN9225" :total_width 48 :total_depth 19 :kneespace 11 :price 2104 :weight 210}
                    {:number "AN9226" :total_width 60 :total_depth 19 :kneespace 21 :price 2327 :weight 220}
                    {:number "AN9237" :total_width 48 :total_depth 25 :kneespace 11 :price 2313 :weight 215}
                    {:number "AN9238" :total_width 60 :total_depth 25 :kneespace 21 :price 2560 :weight 225}]
             :option[{:name "A42"} {:name "033a"}]}

            {:line "Nova"
             :category "Table"
             :name "Computer Corner Table"
             :description "with keyboard drawer and grommet. Angled work edge is 30\" wide. To be used with 25\" deep units."
             :item [{:number "AN9380" :total_width 40 :total_depth 40 :kneespace 30 :price 2096 :weight 195}
                    {:number "AN9383" :total_width 48 :total_depth 48 :kneespace 31 :price 2817 :weight 204}]}

            {:line "Nova"
             :category "Credenza"
             :name "Rear Unit"
             :description "One box/ box/ file pedestal. Option: File/file/pedestal - no additional charge. Specify: pedestal on right or left Units will be shipped KD"
             :item [{:number "AN9540" :total_width 60 :total_depth 19 :kneespace 40 :price 2463 :weight 175}
                    {:number "AN9541" :total_width 66 :total_depth 19 :kneespace 46 :price 2643 :weight 185}
                    {:number "AN9542" :total_width 72 :total_depth 19 :kneespace 52 :price 2714 :weight 195}
                    {:number "AN9543" :total_width 84 :total_depth 19 :kneespace 64 :price 3046 :weight 205}
                    {:number "AN9544" :total_width 96 :total_depth 19 :kneespace 76 :price 3793 :weight 215}
                    {:number "AN9545" :total_width 60 :total_depth 25 :kneespace 40 :price 2709 :weight 200}
                    {:number "AN9546" :total_width 66 :total_depth 25 :kneespace 46 :price 2907 :weight 210}
                    {:number "AN9547" :total_width 72 :total_depth 25 :kneespace 52 :price 2986 :weight 220}
                    {:number "AN9548" :total_width 84 :total_depth 25 :kneespace 64 :price 3350 :weight 230}
                    {:number "AN9549" :total_width 96 :total_depth 25 :kneespace 76 :price 4172 :weight 250}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "030"} {:name "A41"} {:name "033a"}]}

          

            {:line "Nova"
             :category "Credenza"
             :name "Rear Unit"
             :description "One 36\" lateral file pedestal. For 30\" lateral fiile, deduct $75. Specify: pedestal on left or right. Units will be shipped KD"
             :item [{:number "AN9570" :total_width 60 :total_depth 19 :kneespace 22 :price 2763 :weight 245}
                    {:number "AN9571" :total_width 66 :total_depth 19 :kneespace 28 :price 2937 :weight 255}
                    {:number "AN9572" :total_width 72 :total_depth 19 :kneespace 34 :price 3004 :weight 265}
                    {:number "AN9573" :total_width 84 :total_depth 19 :kneespace 46 :price 3347 :weight 275}
                    {:number "AN9574" :total_width 96 :total_depth 19 :kneespace 58 :price 4094 :weight 310}
                    {:number "AN9575" :total_width 60 :total_depth 25 :kneespace 22 :price 3010 :weight 275}
                    {:number "AN9576" :total_width 66 :total_depth 25 :kneespace 28 :price 3208 :weight 285}
                    {:number "AN9577" :total_width 72 :total_depth 25 :kneespace 34 :price 3287 :weight 295}
                    {:number "AN9578" :total_width 84 :total_depth 25 :kneespace 46 :price 3648 :weight 305}
                    {:number "AN9579" :total_width 96 :total_depth 25 :kneespace 58 :price 4473 :weight 350}]
             :option[{:name "A42"} {:name "033a"} {:name "012"} {:name "013"} {:name "020"} {:name "030"}]}

            {:line "Nova"
             :category "Credenza"
             :name "Rear Unit"
             :description "One 36\" lateral file pedestal and one box/box/file pedestal. For 30\" lateral fiile, deduct $75. Specify: pedestal on left or right. Units will be shipped KD"
             :item [{:number "AN9531" :total_width 84 :total_depth 19 :kneespace 27 :price 4346 :weight 340}
                    {:number "AN9532" :total_width 96 :total_depth 19 :kneespace 40 :price 4725 :weight 360}
                    {:number "AN9533" :total_width 120 :total_depth 19 :kneespace 64 :price 5092 :weight 385}
                    {:number "AN9535" :total_width 84 :total_depth 25 :kneespace 27 :price 4779 :weight 390}
                    {:number "AN9536" :total_width 96 :total_depth 25 :kneespace 40 :price 5200 :weight 410}
                    {:number "AN9537" :total_width 120 :total_depth 25 :kneespace 64 :price 5602 :weight 445}]
             :option[{:name "A42"} {:name "033a"} {:name "012"} {:name "013"} {:name "020"} {:name "030"}]}

            {:line "Nova"
             :category "Credenza"
             :name "Rear Unit"
             :description "Two 36\" lateral file pedestals For 30\" lateral fiile, deduct $75. per pedestal. Specify: pedestals on left or right. to be used with 19\" connectors only. All units shipped KD"
             :item [{:number "AN9621" :total_width 96 :total_depth 19 :kneespace 22 :price 5063 :weight 445}
                    {:number "AN9622" :total_width 108 :total_depth 19 :kneespace 34 :price 5237 :weight 475}
                    {:number "AN9623" :total_width 120 :total_depth 19 :kneespace 46 :price 5430 :weight 500}
                    {:number "AN9624" :total_width 144 :total_depth 19 :kneespace 70 :price 5899 :weight 515}
                    {:number "AN9625" :total_width 96 :total_depth 25 :kneespace 22 :price 5568 :weight 465}
                    {:number "AN9626" :total_width 108 :total_depth 25 :kneespace 34 :price 5761 :weight 480}
                    {:number "AN9627" :total_width 120 :total_depth 25 :kneespace 46 :price 5972 :weight 520}
                    {:number "AN9628" :total_width 144 :total_depth 25 :kneespace 70 :price 6489 :weight 555}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "030"} {:name "A42"} {:name "033a"} {:name "010"}]}

            {:line "Nova"
             :category "Credenza"
             :name "Rear Unit"
             :description "One 36\" storage cabinet and one box/box/file pedestal. For 30\" storage cabinet, deduct $35. Specify: pedestal on left or right. Units will be shipped KD"
             :item [{:number "AN9631" :total_width 84 :total_depth 19 :kneespace 27 :price 4226 :weight 280}
                    {:number "AN9632" :total_width 90 :total_depth 19 :kneespace 34 :price 4407 :weight 290}
                    {:number "AN9633" :total_width 96 :total_depth 19 :kneespace 40 :price 4605 :weight 300}
                    {:number "AN9634" :total_width 120 :total_depth 19 :kneespace 64 :price 4972 :weight 330}
                    {:number "AN9635" :total_width 84 :total_depth 25 :kneespace 27 :price 4659 :weight 320}
                    {:number "AN9636" :total_width 90 :total_depth 25 :kneespace 34 :price 4894 :weight 330}
                    {:number "AN9637" :total_width 96 :total_depth 25 :kneespace 40 :price 5080 :weight 345}
                    {:number "AN9638" :total_width 120 :total_depth 25 :kneespace 64 :price 5482 :weight 385}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "030"} {:name "A42"} {:name "033a"} {:name "010"}]}

            {:line "Nova"
             :category "Hutch"
             :name "Open Shelf Corner Unit for Credenzas"
             :description "29.5” High 2 Shelves"
             :item [{:number "AN9166" :total_width 19 :total_depth 19 :price 2153 :weight 70}
                    {:number "AN9167" :total_width 25 :total_depth 25 :price 2285 :weight 85}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "030"} {:name "A42"} {:name "033a"} {:name "010"}]}

            {:line "Nova"
             :category "Part"
             :name "Add-On Pedestals"
             :description "No top. 27.5\" high. A - File/file for credenzas B - Box/box/file for credenzas D - Double lateral file for credenzas"
             :item [{:number "AN9385" :total_width 18.5 :total_depth 18 :price 964 :weight 75}
                    {:number "AN9445" :total_width 30 :total_depth 18 :price 1212 :weight 155}
                    {:number "AN9446" :total_width 36 :total_depth 18 :price 1303 :weight 165}]
             :option[{:name "012"} {:name "A41"} {:name "010"} {:name "100"}]}


            {:line "Nova"
             :category "Hutch"
             :name "Open Tower"
             :description "with adjustable shelves 181/2\" wide | 15\" deep Specify: right or left tower"
             :item [{:number "AN9451" :total_height 42 :shelf 2 :price 830 :weight 55}
                    {:number "AN9452" :total_height 54 :shelf 3 :price 987 :weight 65}]
             :option[{:name "033a"} {:name "070"}]}

            {:line "Nova"
             :category "Hutch"
             :name "Partial Closed Tower"
             :description "with one 14\" high door and open bookshelf 18 1/2\" wide | 15\" deep Specify: right or left tower."
             :item [{:number "AN9453" :total_height 42 :shelf 1 :price 903 :weight 65}
                    {:number "AN9455" :total_height 54 :shelf 2 :price 1084 :weight 75}]
             :option[{:name "011"} {:name "033a"} {:name "070"}]}

            {:line "Nova"
             :category "Hutch"
             :name "Partial Closed Tower"
             :description "with one 28\" high door and adjustable shelf. 18 1/2\" wide | 15\" deep Specify: right or left tower"
             :item [{:number "AN9456" :total_height 42 :shelf 1 :price 951 :weight 75}
                    {:number "AN9457" :total_height 54 :shelf 2 :price 1131 :weight 85}]
             :option[{:name "011"} {:name "033a"}]}

            {:line "Nova"
             :category "Hutch"
             :name "Open Shelf Corner Unit"
             :description "adjustable shelves 15 x 15"
             :item [{:number "AN9168" :total_height 42 :shelf 2 :price 1842 :weight 70}
                    {:number "AN9169" :total_height 54 :shelf 3 :price 1922 :weight 80}]}

            {:line "Nova"
             :category "Hutch"
             :name "Open Tower"
             :description "with adjustable shelves 15\" deep"
             :item [{:number "AN9461" :total_width 30 :total_height 42 :shelf 2 :price 1444 :weight 80}
                    {:number "AN9462" :total_width 30 :total_height 54 :shelf 3 :price 1578 :weight 90}
                    {:number "AN9463" :total_width 36 :total_height 42 :shelf 2 :price 1589 :weight 90}
                    {:number "AN9464" :total_width 36 :total_height 54 :shelf 3 :price 1733 :weight 100}]
             :option[{:name "033a"}]}

            {:line "Nova"
             :category "Hutch"
             :name "Partial Closed Tower"
             :description "with two 14\" high hinged doors 15\" deep"
             :item [{:number "AN9465" :total_width 30 :total_height 42 :shelf 2 :price 1582 :weight 90}
                    {:number "AN9466" :total_width 30 :total_height 54 :shelf 3 :price 1655 :weight 100}
                    {:number "AN9467" :total_width 36 :total_height 42 :shelf 2 :price 1648 :weight 100}
                    {:number "AN9468" :total_width 36 :total_height 54 :shelf 3 :price 1724 :weight 110}]
             :option[{:name "011"} {:name "033a"} {:name "070"}]}

            {:line "Nova"
             :category "Hutch"
             :name "Partial Closed Tower"
             :description "with two 28\" high hinged doors and adj. shelf. 15\" deep"
             :item [{:number "AN9481" :total_width 30 :total_height 42 :shelf 2 :price 1679 :weight 110}
                    {:number "AN9482" :total_width 30 :total_height 54 :shelf 3 :price 1752 :weight 120}
                    {:number "AN9483" :total_width 36 :total_height 42 :shelf 2 :price 1710 :weight 120}
                    {:number "AN9484" :total_width 36 :total_height 54 :shelf 3 :price 1788 :weight 130}]
             :option[{:name "011"} {:name "033a"}]}

            {:line "Nova"
             :category "Hutch"
             :name "Closed Tower"
             :description "with 2 full length hinged doors 15\" deep"
             :item [{:number "AN9485" :total_width 30 :total_height 42 :shelf 2 :price 1806 :weight 120}
                    {:number "AN9486" :total_width 30 :total_height 54 :shelf 3 :price 1938 :weight 130}
                    {:number "AN9487" :total_width 36 :total_height 42 :shelf 2 :price 1950 :weight 130}
                    {:number "AN9488" :total_width 36 :total_height 54 :shelf 3 :price 2095 :weight 140}]
             :option[{:name "011"} {:name "033a"}]}

            {:line "Nova"
             :category "Cabinet"
             :name "Open Overhead"
             :description "18\" high"
             :item [{:number "AN9711" :total_width 15 :total_depth 26 :price 710 :weight 42}
                    {:number "AN9712" :total_width 15 :total_depth 32 :price 722 :weight 47}
                    {:number "AN9713" :total_width 15 :total_depth 38 :price 830 :weight 50}
                    {:number "AN9714" :total_width 15 :total_depth 50 :price 1072 :weight 55}
                    {:number "AN9715" :total_width 15 :total_depth 56 :price 1258 :weight 60}
                    {:number "AN9716" :total_width 15 :total_depth 62 :price 1679 :weight 75}
                    {:number "AN9717" :total_width 15 :total_depth 84 :price 2648 :weight 90}
                    {:number "AN9718" :total_width 15 :total_depth 110 :price 2890 :weight 120}]
             :option[{:name "011"} {:name "070"} {:name "033a"}]}

            {:line "Nova"
             :category "Cabinet"
             :name "Closed Overhead"
             :description "with all hinged doors 18\" high"
             :item [{:number "AN9721" :total_width 15 :total_depth 26 :door 2 :price 891 :weight 57}
                    {:number "AN9722" :total_width 15 :total_depth 32 :door 2 :price 903 :weight 62}
                    {:number "AN9723" :total_width 15 :total_depth 38 :door 2 :price 1011 :weight 65}
                    {:number "AN9724" :total_width 15 :total_depth 50 :door 3 :price 1343 :weight 60}
                    {:number "AN9725" :total_width 15 :total_depth 56 :door 3 :price 1566 :weight 85}
                    {:number "AN9726" :total_width 15 :total_depth 62 :door 4 :price 1836 :weight 105}
                    {:number "AN9727" :total_width 15 :total_depth 84 :door 5 :price 2769 :weight 120}
                    {:number "AN9728" :total_width 15 :total_depth 110 :door 6 :price 3130 :weight 150}]
             :option[{:name "011"} {:name "070"} {:name "033a"}]}

            {:line "Nova"
             :category "Cabinet"
             :name "Open Overhead"
             :description "with adjustable shelves 30\" high"
             :item [{:number "AN9731" :total_width 15 :total_depth 26 :price 924 :weight 72}
                    {:number "AN9732" :total_width 15 :total_depth 32 :price 964 :weight 77}
                    {:number "AN9733" :total_width 15 :total_depth 38 :price 1080 :weight 80}
                    {:number "AN9734" :total_width 15 :total_depth 50 :price 1392 :weight 85}
                    {:number "AN9735" :total_width 15 :total_depth 56 :price 1679 :weight 90}
                    {:number "AN9736" :total_width 15 :total_depth 62 :price 2034 :weight 105}
                    {:number "AN9737" :total_width 15 :total_depth 84 :price 3130 :weight 120}
                    {:number "AN9738" :total_width 15 :total_depth 110 :price 3262 :weight 150}]
             :option[{:name "011"} {:name "070"} {:name "033a"}]}

            {:line "Nova"
             :category "Cabinet"
             :name "Closed Overhead"
             :description "with hinged doors and adjustable shelves 30\" high"
             :item [{:number "AN9741" :total_width 15 :total_depth 26 :door 2 :price 1105 :weight 77}
                    {:number "AN9742" :total_width 15 :total_depth 32 :door 2 :price 1143 :weight 82}
                    {:number "AN9743" :total_width 15 :total_depth 38 :door 2 :price 1260 :weight 85}
                    {:number "AN9744" :total_width 15 :total_depth 50 :door 3 :price 1658 :weight 90}
                    {:number "AN9745" :total_width 15 :total_depth 56 :door 3 :price 1950 :weight 105}
                    {:number "AN9746" :total_width 15 :total_depth 62 :door 4 :price 2396 :weight 125}
                    {:number "AN9747" :total_width 15 :total_depth 84 :door 5 :price 3582 :weight 130}
                    {:number "AN9748" :total_width 15 :total_depth 110 :door 6 :price 3732 :weight 170}]
             :option[{:name "011"} {:name "070"} {:name "033a"}]}

            {:line "Nova"
             :category "Credenza"
             :name "Cabinet Credenza 29\" high."
             :description "1 adjustable shelf behind each pair of doors. Odd single door is on left unless otherwise specified."
             :item [{:number "AN9530" :total_width 35 :total_depth 19 :door 2 :price 1591 :weight 150}
                    {:number "AN9320" :total_width 42 :total_depth 19 :door 2 :price 1913 :weight 165}
                    {:number "AN9340" :total_width 53 :total_depth 19 :door 3 :price 2658 :weight 210}
                    {:number "AN9330" :total_width 70.5 :total_depth 19 :door 4 :price 3190 :weight 260}
                    {:number "AN9331" :total_width 88 :total_depth 19 :door 5 :price 4262 :weight 350}
                    {:number "AN9332" :total_width 96 :total_depth 19 :door 6 :price 4997 :weight 420}
                    {:number "AN9333" :total_width 120 :total_depth 19 :door 8 :price 5779 :weight 550}
                    {:number "AN9110" :total_width 35 :total_depth 25 :door 2 :price 1750 :weight 180}
                    {:number "AN9112" :total_width 42 :total_depth 25 :door 2 :price 2105 :weight 210}
                    {:number "AN9113" :total_width 53 :total_depth 25 :door 3 :price 2924 :weight 240}
                    {:number "AN9114" :total_width 70.5 :total_depth 25 :door 4 :price 3508 :weight 300}
                    {:number "AN9115" :total_width 88 :total_depth 25 :door 5 :price 4688 :weight 390}
                    {:number "AN9116" :total_width 96 :total_depth 25 :door 6 :price 5496 :weight 490}
                    {:number "AN9117" :total_width 120 :total_depth 25 :door 8 :price 6357 :weight 615}]
             :option[{:name "010"} {:name "A42"} {:name "033a"}]}

            {:line "Nova"
             :category "Credenza"
             :name "Cabinet Credenza 36\" high."
             :description "2 adjustable shelves behind each pair of doors. Odd single door is on left unless otherwise specified"
             :item [{:number "AN9560" :total_width 35 :total_depth 19 :door 2 :price 1910 :weight 160}
                    {:number "AN9561" :total_width 42 :total_depth 19 :door 2 :price 2295 :weight 180}
                    {:number "AN9562" :total_width 53 :total_depth 19 :door 3 :price 3190 :weight 235}
                    {:number "AN9563" :total_width 70.5 :total_depth 19 :door 4 :price 3826 :weight 300}
                    {:number "AN9564" :total_width 88 :total_depth 19 :door 5 :price 5113 :weight 370}
                    {:number "AN9565" :total_width 96 :total_depth 19 :door 6 :price 6245 :weight 435}
                    {:number "AN9566" :total_width 120 :total_depth 19 :door 8 :price 7100 :weight 585}
                    {:number "AN9121" :total_width 35 :total_depth 25 :door 2 :price 2101 :weight 200}
                    {:number "AN9122" :total_width 42 :total_depth 25 :door 2 :price 2526 :weight 220}
                    {:number "AN9123" :total_width 53 :total_depth 25 :door 3 :price 3508 :weight 260}
                    {:number "AN9124" :total_width 70.5 :total_depth 25 :door 4 :price 4209 :weight 340}
                    {:number "AN9125" :total_width 88 :total_depth 25 :door 5 :price 5625 :weight 430}
                    {:number "AN9126" :total_width 96 :total_depth 25 :door 6 :price 6870 :weight 530}
                    {:number "AN9127" :total_width 120 :total_depth 25 :door 8 :price 7810 :weight 650}]
             :option[{:name "010"} {:name "A42"} {:name "033a"}]}

            {:line "Nova"
             :category "Credenza"
             :name "Refrigerator"
             :description "2 cubic ft. capacity. Refrigerator can be added to any credenza with doors by adding $1323. to credenza cost."
             :item [{:number "AN9490" :total_width 36 :total_depth 19 :door 2 :price 2914 :weight 200}
                    {:number "AN9493" :total_width 36 :total_depth 25 :door 2 :price 3073 :weight 220}]
             :option[{:name "010"} {:name "A41"}]}

            {:line "Nova"
             :category "Credenza"
             :name "Conference Room Credenza"
             :description "A 6\" utility drawer located behind the two center doors. One adjustable shelf in each center and end section."
             :item [{:number "AN9290" :total_width 35 :total_depth 19 :door 2 :price 2172 :weight 165}
                    {:number "AN9291" :total_width 70.5 :total_depth 19 :door 4 :price 3720 :weight 295}
                    {:number "AN9296" :total_width 96 :total_depth 19 :door 6 :price 5654 :weight 430}
                    {:number "AN9297" :total_width 120 :total_depth 19 :door 8 :price 6442 :weight 570}
                    {:number "AN9251" :total_width 35 :total_depth 25 :door 2 :price 2389 :weight 195}
                    {:number "AN9252" :total_width 70.5 :total_depth 25 :door 4 :price 4092 :weight 320}
                    {:number "AN9253" :total_width 96 :total_depth 25 :door 6 :price 6218 :weight 515}
                    {:number "AN9254" :total_width 120 :total_depth 25 :door 8 :price 7087 :weight 625}]
             :option[{:name "010"} {:name "A42"} {:name "033a"}]}

            {:line "Nova"
             :category "Credenza"
             :name "36\" High Buffet Credenza"
             :description "Doors and drawers"
             :item [{:number "AN9292" :total_width 36.5 :total_depth 19 :door 2 :price 2911 :weight 175}
                    {:number "AN9293" :total_width 72.5 :total_depth 19 :door 4 :price 4361 :weight 330}
                    {:number "AN9294" :total_width 108.5 :total_depth 19 :door 6 :price 7757 :weight 475}
                    {:number "AN9256" :total_width 36.5 :total_depth 25 :door 2 :price 3203 :weight 215}
                    {:number "AN9257" :total_width 72.5 :total_depth 25 :door 4 :price 4798 :weight 360}
                    {:number "AN9258" :total_width 108.5 :total_depth 25 :door 6 :price 8532 :weight 535}]
             :option[{:name "010"} {:name "A42"} {:name "013"} {:name "033a"}]}

            {:line "Nova"
             :category "Credenza"
             :name "Server"
             :description "with hidden casters. Space above doors is 6\" high."
             :item [{:number "AN9491" :total_width 36.5 :total_depth 22 :door 2 :price 2058 :weight 165}
                    {:number "AN9492" :total_width 36.5 :total_depth 25 :door 2 :price 2288 :weight 185}]
             :option[{:name "A22"} {:name "010"} {:name "033a"} {:name "A42"}]}

            {:line "Nova"
             :category "Credenza"
             :name "Combination Credenza"
             :description "2 doors, 2 file and 4 box drawers"
             :item [{:number "AN9030" :total_width 71 :total_depth 19 :price 3717 :weight 325}
                    {:number "AN9031" :total_width 71 :total_depth 25 :price 4162 :weight 375}]
             :option[{:name "010"} {:name "012"} {:name "A42"} {:name "033a"}]}

            {:line "Nova"
             :category "Credenza"
             :name "Combination Credenza"
             :description "2 doors, 1 file and 2 box drawers."
             :item [{:number "AN9350" :total_width 52 :total_depth 19 :price 3321 :weight 275}
                    {:number "AN9132" :total_width 52 :total_depth 25 :price 3719 :weight 300}]
             :option[{:name "010"} {:name "012"} {:name "A42"} {:name "033a"}]}

            {:line "Nova"
             :category "Credenza"
             :name "Combination Credenza"
             :item [{:number "AN9335" :total_width 38.5 :total_depth 19 :price 2563 :weight 175}
                    {:number "AN9336" :total_width 54 :total_depth 19 :price 3411 :weight 270}
                    {:number "AN9337" :total_width 71 :total_depth 19 :price 4283 :weight 350}
                    {:number "AN9133" :total_width 38.5 :total_depth 25 :price 2871 :weight 200}
                    {:number "AN9134" :total_width 54 :total_depth 25 :price 4328 :weight 300}
                    {:number "AN9135" :total_width 71.5 :total_depth 25 :price 4798 :weight 390}]
             :option[{:name "012"} {:name "A42"} {:name "033a"}]}

            {:line "Nova"
             :category "Credenza"
             :name "Writing Credenza"
             :description "4 box and 2 file drawers"
             :item [{:number "AN9360" :total_width 60 :total_depth 19 :kneespace 23 :price 2958 :weight 225}
                    {:number "AN9370" :total_width 71 :total_depth 19 :kneespace 34 :price 3229 :weight 250}
                    {:number "AN9136" :total_width 60 :total_depth 25 :kneespace 23 :price 3314 :weight 240}
                    {:number "AN9137" :total_width 71 :total_depth 25 :kneespace 34 :price 3616 :weight 265}]
             :option[{:name "012"} {:name "A42"} {:name "033a"}]}

            {:line "Nova"
             :category "Credenza"
             :name "Writing Credenza"
             :description "with two 36\"lateral files For 30\" lateral files deduct $75."
             :item [{:number "AN9141" :total_width 96 :total_depth 19 :kneespace 24 :price 4327 :weight 420}
                    {:number "AN9142" :total_width 120 :total_depth 19 :kneespace 48 :price 4936 :weight 475}
                    {:number "AN9143" :total_width 96 :total_depth 25 :kneespace 24 :price 4930 :weight 455}
                    {:number "AN9144" :total_width 120 :total_depth 25 :kneespace 48 :price 5528 :weight 495}]
             :option[{:name "012"} {:name "A42"} {:name "033a"}]}

            {:line "Nova"
             :category "Credenza"
             :name "File Credenza"
             :item [{:number "AN9042" :total_width 38.5 :total_depth 19 :drawer 4 :price 2367 :weight 200}
                    {:number "AN9041" :total_width 58 :total_depth 19 :drawer 6 :price 3812 :weight 290}
                    {:number "AN9040" :total_width 71 :total_depth 19 :drawer 8 :price 4190 :weight 350}
                    {:number "AN9145" :total_width 38.5 :total_depth 25 :drawer 4 :price 2648 :weight 220}
                    {:number "AN9146" :total_width 58 :total_depth 25 :drawer 6 :price 4270 :weight 325}
                    {:number "AN9147" :total_width 71 :total_depth 25 :drawer 8 :price 4693 :weight 390}]
             :option[{:name "012"} {:name "A42"}]}

            {:line "Nova"
             :category "File"
             :name "Lateral Files"
             :description "2 drawers Filing is side to side, adjustable letter and legal."
             :item [{:number "AN9300" :total_width 30.5 :total_depth 19 :price 1766 :weight 175}
                    {:number "AN9310" :total_width 36.5 :total_depth 19 :price 1868 :weight 190}
                    {:number "AN9148" :total_width 30.5 :total_depth 25 :price 1978 :weight 200}
                    {:number "AN9149" :total_width 36.5 :total_depth 25 :price 2091 :weight 245}]
             :option[{:name "012"} {:name "A42"}]}

            {:line "Nova"
             :category "File"
             :name "Lateral File"
             :description "4 drawers Filing is side to side, adjustable letter and legal."
             :item [{:number "AN9311" :total_width 61 :total_depth 19 :price 3391 :weight 350}
                    {:number "AN9312" :total_width 73 :total_depth 19 :price 3603 :weight 375}
                    {:number "AN9151" :total_width 61 :total_depth 25 :price 3798 :weight 395}
                    {:number "AN9152" :total_width 73 :total_depth 25 :price 4033 :weight 425}]
             :option[{:name "012"} {:name "A42"}]}

            {:line "Nova"
             :category "File"
             :name "Lateral File"
             :description "3 drawers; 41\" high Filing is side to side, adjustable letter and legal. Shipped with anti-tilt safety system. All Nova edges are standard."
             :item [{:number "AN9303" :total_width 30.5 :total_depth 19 :price 2660 :weight 225}
                    {:number "AN9304" :total_width 36.5 :total_depth 19 :price 2812 :weight 240}
                    {:number "AN9153" :total_width 30.5 :total_depth 25 :price 2980 :weight 275}
                    {:number "AN9154" :total_width 36.5 :total_depth 25 :price 3150 :weight 290}]
             :option[{:name "A42"} {:name "012"} {:name "033a"}]}

            {:line "Nova"
             :category "File"
             :name "Lateral File"
             :description "4 drawers; 53\" high Shipped with anti-tilt safety system. All Nova edges are standard."
             :item [{:number "AN9155" :total_width 30.5 :total_depth 19 :price 3606 :weight 295}
                    {:number "AN9156" :total_width 36.5 :total_depth 19 :price 3907 :weight 310}
                    {:number "AN9157" :total_width 30.5 :total_depth 25 :price 4040 :weight 345}
                    {:number "AN9158" :total_width 36.5 :total_depth 25 :price 4376 :weight 360}]
             :option[{:name "A42"} {:name "012"} {:name "033a"}]}

            {:line "Nova"
             :category "File"
             :name "Lateral File"
             :description "5 drawers; 68\" high Shipped with anti-tilt safety system. All Nova edges are standard."
             :item [{:number "AN9160" :total_width 30.5 :total_depth 19 :price 4688 :weight 365}
                    {:number "AN9162" :total_width 36.5 :total_depth 19 :price 5010 :weight 380}
                    {:number "AN9163" :total_width 30.5 :total_depth 25 :price 5050 :weight 415}
                    {:number "AN9164" :total_width 36.5 :total_depth 25 :price 5470 :weight 435}]
             :option[{:name "A42"} {:name "012"} {:name "033a"}]}

            {:line "Nova"
             :category "Cabinet"
             :name "Wardrobe"
             :description "Single Door with Shelf and Clothes rod 25\" deep | 18.5\" wide"
             :item [{:number "AN9280" :total_height 72 :shelf 1 :price 2150 :weight 150 }
                    {:number "AN9281" :total_height 84 :shelf 1 :price 2285 :weight 170 }]
             :option[{:name "011"}]}

            {:line "Nova"
             :category "Cabinet"
             :name "Storage/Wardrobe"
             :description "Two doors. 1/3 storage with shelves. 2/3 wardrobe with shelf and clothes rod."
             :item [{:number "AN9591" :total_width 19 :total_depth 30.5 :total_height 72 :shelf 4 :price 2795 :weight 185}
                    {:number "AN9592" :total_width 19 :total_depth 30.5 :total_height 84 :shelf 5 :price 2953 :weight 195}
                    {:number "AN9593" :total_width 19 :total_depth 36.5 :total_height 72 :shelf 4 :price 2958 :weight 195}
                    {:number "AN9594" :total_width 19 :total_depth 36.5 :total_height 84 :shelf 5 :price 3188 :weight 205}
                    {:number "AN9595" :total_width 25 :total_depth 30.5 :total_height 72 :shelf 4 :price 3129 :weight 215}
                    {:number "AN9596" :total_width 25 :total_depth 30.5 :total_height 84 :shelf 5 :price 3306 :weight 230}
                    {:number "AN9597" :total_width 25 :total_depth 36.5 :total_height 72 :shelf 4 :price 3311 :weight 230}
                    {:number "AN9598" :total_width 25 :total_depth 36.5 :total_height 84 :shelf 5 :price 3606 :weight 250}]
             :option[{:name "010"} {:name "A34"} {:name "033a"}]}

            {:line "Nova"
             :category "Cabinet"
             :name "Storage File Cabinet"
             :description "Two doors with adjustable shelves, and two lateral file drawers"
             :item [{:number "AN9505" :total_width 72 :total_depth 19 :total_height 30.5 :shelf 2 :price 3552 :weight 195}
                    {:number "AN9506" :total_width 84 :total_depth 19 :total_height 30.5 :shelf 3 :price 3957 :weight 205}
                    {:number "AN9507" :total_width 72 :total_depth 19 :total_height 36.5 :shelf 2 :price 3696 :weight 205}
                    {:number "AN9508" :total_width 84 :total_depth 19 :total_height 36.5 :shelf 3 :price 4101 :weight 215}
                    {:number "AN9501" :total_width 72 :total_depth 25 :total_height 30.5 :shelf 2 :price 3973 :weight 215}
                    {:number "AN9502" :total_width 84 :total_depth 25 :total_height 30.5 :shelf 3 :price 4431 :weight 225}
                    {:number "AN9503" :total_width 72 :total_depth 25 :total_height 36.5 :shelf 2 :price 4139 :weight 245}
                    {:number "AN9504" :total_width 84 :total_depth 25 :total_height 36.5 :shelf 3 :price 4593 :weight 260}]
             :option[{:name "010"} {:name "012"} {:name "A41"} {:name "033a"}]}

            {:line "Nova"
             :category "Bookcase"
             :name "Open Bookcase with Two Door Credenza"
             :item [{:number "AN9510" :total_width 60 :total_depth 19 :total_height 30.5 :shelf 1 :price 2430 :weight 155}
                    {:number "AN9511" :total_width 72 :total_depth 19 :total_height 30.5 :shelf 2 :price 2733 :weight 165}
                    {:number "AN9512" :total_width 84 :total_depth 19 :total_height 30.5 :shelf 3 :price 2973 :weight 175}
                    {:number "AN9513" :total_width 60 :total_depth 19 :total_height 36.5 :shelf 1 :price 2589 :weight 165}
                    {:number "AN9514" :total_width 72 :total_depth 19 :total_height 36.5 :shelf 2 :price 2880 :weight 175}
                    {:number "AN9515" :total_width 84 :total_depth 19 :total_height 36.5 :shelf 3 :price 3142 :weight 185}
                    {:number "AN9940" :total_width 60 :total_depth 25 :total_height 30.5 :shelf 1 :price 2672 :weight 175}
                    {:number "AN9941" :total_width 72 :total_depth 25 :total_height 30.5 :shelf 2 :price 3007 :weight 185}
                    {:number "AN9942" :total_width 84 :total_depth 25 :total_height 30.5 :shelf 3 :price 3271 :weight 200}
                    {:number "AN9943" :total_width 60 :total_depth 25 :total_height 36.5 :shelf 1 :price 2848 :weight 185}
                    {:number "AN9944" :total_width 72 :total_depth 25 :total_height 26.5 :shelf 2 :price 3167 :weight 200}
                    {:number "AN9945" :total_width 84 :total_depth 25 :total_height 26.5 :shelf 3 :price 3457 :weight 215}]
             :option[{:name "010"} {:name "061"} {:name "A42"} {:name "033a"}]}

            {:line "Nova"
             :category "Bookcase"
             :name "Open Bookcase with Lateral File"
             :item [{:number "AN9520" :total_width 60 :total_depth 19 :total_height 30.5 :shelf 1 :price 2485 :weight 205}
                    {:number "AN9521" :total_width 72 :total_depth 19 :total_height 30.5 :shelf 2 :price 2773 :weight 225}
                    {:number "AN9522" :total_width 84 :total_depth 19 :total_height 30.5 :shelf 3 :price 3031 :weight 235}
                    {:number "AN9523" :total_width 60 :total_depth 19 :total_height 36.5 :shelf 1 :price 2638 :weight 225}
                    {:number "AN9524" :total_width 72 :total_depth 19 :total_height 36.5 :shelf 2 :price 2940 :weight 235}
                    {:number "AN9525" :total_width 84 :total_depth 19 :total_height 36.5 :shelf 3 :price 3190 :weight 245}
                    {:number "AN9950" :total_width 60 :total_depth 25 :total_height 30.5 :shelf 1 :price 2769 :weight 245}
                    {:number "AN9951" :total_width 72 :total_depth 25 :total_height 30.5 :shelf 2 :price 3058 :weight 255}
                    {:number "AN9552" :total_width 84 :total_depth 25 :total_height 30.5 :shelf 3 :price 3365 :weight 270}
                    {:number "AN9953" :total_width 60 :total_depth 25 :total_height 36.5 :shelf 1 :price 2926 :weight 255}
                    {:number "AN9954" :total_width 72 :total_depth 25 :total_height 36.5 :shelf 2 :price 3227 :weight 270}
                    {:number "AN9955" :total_width 84 :total_depth 25 :total_height 36.5 :shelf 3 :price 3516 :weight 290}]
             :option[{:name "012"} {:name "061"} {:name "A42"} {:name "033a"}]}

            {:line "Nova"
             :category "Bookcase"
             :name "Vertical Bookcase with Partial Doors."
             :item [{:number "AN9901" :total_width 60 :total_depth 13 :total_height 30.5 :shelf 2 :price 1727 :weight 125}
                    {:number "AN9902" :total_width 72 :total_depth 13 :total_height 30.5 :shelf 3 :price 1882 :weight 140}
                    {:number "AN9903" :total_width 84 :total_depth 13 :total_height 30.5 :shelf 4 :price 2067 :weight 150}
                    {:number "AN9904" :total_width 96 :total_depth 13 :total_height 30.5 :shelf 5 :price 2149 :weight 170}
                    {:number "AN9911" :total_width 60 :total_depth 13 :total_height 36.5 :shelf 2 :price 1797 :weight 130}
                    {:number "AN9912" :total_width 72 :total_depth 13 :total_height 36.5 :shelf 3 :price 1968 :weight 150}
                    {:number "AN9913" :total_width 84 :total_depth 13 :total_height 36.5 :shelf 4 :price 2156 :weight 170}
                    {:number "AN9914" :total_width 96 :total_depth 13 :total_height 36.5 :shelf 5 :price 2260 :weight 185}
                    {:number "AN9906" :total_width 60 :total_depth 15 :total_height 30.5 :shelf 2 :price 1899 :weight 135}
                    {:number "AN9907" :total_width 72 :total_depth 15 :total_height 30.5 :shelf 3 :price 2069 :weight 150}
                    {:number "AN9908" :total_width 84 :total_depth 15 :total_height 30.5 :shelf 4 :price 2274 :weight 160}
                    {:number "AN9909" :total_width 96 :total_depth 15 :total_height 30.5 :shelf 5 :price 2365 :weight 180}
                    {:number "AN9916" :total_width 60 :total_depth 13 :total_height 36.5 :shelf 2 :price 1977 :weight 140}
                    {:number "AN9917" :total_width 72 :total_depth 13 :total_height 36.5 :shelf 3 :price 2164 :weight 165}
                    {:number "AN9918" :total_width 84 :total_depth 13 :total_height 36.5 :shelf 4 :price 2371 :weight 185}
                    {:number "AN9919" :total_width 96 :total_depth 13 :total_height 36.5 :shelf 5 :price 2486 :weight 200}]
             :option[{:name "010"} {:name "061"} {:name "A41"} {:name "033a"} {:name "A47"}]}

            {:line "Nova"
             :category "Cabinet"
             :name "Wall Hung Cabinet with flipper doors."
             :description "Inside ht.13.5\" Inside depth 12\" Outside ht.18\""
             :item [{:number "AN9700" :total_width 30 :total_depth 15 :door 1 :price 1207 :weight 50}
                    {:number "AN9710" :total_width 35 :total_depth 15 :door 1 :price 1305 :weight 60}
                    {:number "AN9720" :total_width 53 :total_depth 15 :door 2 :price 1745 :weight 70}
                    {:number "AN9730" :total_width 60 :total_depth 15 :door 2 :price 1992 :weight 80}
                    {:number "AN9740" :total_width 70.5 :total_depth 15 :door 2 :price 2065 :weight 95}]
             :option[{:name "011"} {:name "070"}]}

            {:line "Nova"
             :category "Cabinet"
             :name "Wall Hung Cabinet w/ hinged doors"
             :description "Inside height 15\" Inside depth 12\" Outside height 18\""
             :item [{:number "AN9701" :total_width 30 :total_depth 15 :door 2 :price 1174 :weight 50}
                    {:number "AN9702" :total_width 35 :total_depth 15 :door 2 :price 1277 :weight 60}
                    {:number "AN9703" :total_width 53 :total_depth 15 :door 3 :price 1715 :weight 70}
                    {:number "AN9704" :total_width 60 :total_depth 15 :door 4 :price 1938 :weight 80}
                    {:number "AN9705" :total_width 70.5 :total_depth 15 :door 4 :price 2041 :weight 95}]
             :option[{:name "011"} {:name "070"}]}

            {:line "Nova"
             :category "Hutch"
             :name "Flipper Door Hutch for credenzas."
             :description "OA ht. 42\". Inside cabinet ht. 13.5\" Inside cabinet depth 12\". Optional: 54\" OA ht. + 5% When ordering hutches please indicate model number of credenza"
             :item [{:number "AN9600" :total_width 30 :total_depth 15 :door 1 :price 1639 :weight 70}
                    {:number "AN9610" :total_width 35 :total_depth 15 :door 1 :price 1649 :weight 95}
                    {:number "AN9620" :total_width 53 :total_depth 15 :door 2 :price 2865 :weight 150}
                    {:number "AN9630" :total_width 60 :total_depth 15 :door 2 :price 3483 :weight 160}
                    {:number "AN9640" :total_width 70.5 :total_depth 15 :door 2 :price 3514 :weight 175}]
             :option[{:name "011"} {:name "070"} {:name "071"} {:name "072"} {:name "033a"}]}

            {:line "Nova"
             :category "Hutch"
             :name "Hinged Door Hutch for credenzas."
             :description "OA ht. 42\". Inside cabinet ht. 13.5\" Inside cabinet depth 12\". Optional: 54\" OA ht. + 5% When ordering hutches please indicate model number of credenza"
             :item [{:number "AN9605" :total_width 30 :total_depth 15 :door 2 :price 1582 :weight 70}
                    {:number "AN9606" :total_width 35 :total_depth 15 :door 2 :price 1602 :weight 95}
                    {:number "AN9607" :total_width 53 :total_depth 15 :door 3 :price 2775 :weight 150}
                    {:number "AN9608" :total_width 60 :total_depth 15 :door 4 :price 3441 :weight 160}
                    {:number "AN9609" :total_width 70.5 :total_depth 15 :door 4 :price 3489 :weight 175}]
             :option[{:name "011"} {:name "070"} {:name "071"} {:name "072"} {:name "033a"}]}

            {:line "Nova"
             :category "Hutch"
             :name "Hinged Door Hutch for credenzas"
             :description "OA ht.42\". Two doors with two adjustable shelves. Wood shelf in center. For credenzas see p. 182 When ordering hutches please indicate model number of credenza."
             :item [{:number "AN9670" :total_width 70.5 :total_depth 15 :price 3582 :weight 185}]
             :option[{:name "011"} {:name "070"} {:name "071"} {:name "072"} {:name "033a"}]}

            {:line "Nova"
             :category "Hutch"
             :name "Binder Bookcase Hutch for credenzas."
             :description "OA ht. 54\" Optional: 42\" OA ht. Deduct 5% When ordering hutches please indicate model number of credenza"
             :item [{:number "AN9641" :total_width 30 :total_depth 15 :price 1309 :weight 70}
                    {:number "AN9642" :total_width 35 :total_depth 15 :price 1360 :weight 95}
                    {:number "AN9643" :total_width 53 :total_depth 15 :price 2367 :weight 150}
                    {:number "AN9644" :total_width 60 :total_depth 15 :price 3116 :weight 160}
                    {:number "AN9645" :total_width 70.5 :total_depth 15 :price 3247 :weight 175}]
             :option[{:name "033a"} {:name "070"}]}

            {:line "Nova"
             :category "Hutch"
             :name "Organizer"
             :description "for underneath hutch. 4\" high"
             :item [{:number "AN9601" :total_width 34.5 :total_depth 12 :price 460 :weight 30}
                    {:number "AN9602" :total_width 51.5 :total_depth 12 :price 525 :weight 35}
                    {:number "AN9603" :total_width 58.5 :total_depth 12 :price 590 :weight 40}
                    {:number "AN9604" :total_width 69 :total_depth 12 :price 652 :weight 45}]}

            {:line "Nova"
             :category "Bookcase"
             :name "Open Vertical Bookcase"
             :description "13\" OA depth (Tops) 11\" Inside depth"
             :item [{:number "AN9927" :total_height 30 :total_width 30.5 :shelf 1 :price 809 :weight 50}
                    {:number "AN9926" :total_height 36 :total_width 30.5 :shelf 2 :price 905 :weight 65}
                    {:number "AN9925" :total_height 42 :total_width 30.5 :shelf 2 :price 1098 :weight 80}
                    {:number "AN9924" :total_height 48 :total_width 30.5 :shelf 3 :price 1213 :weight 85}
                    {:number "AN9923" :total_height 60 :total_width 30.5 :shelf 3 :price 1349 :weight 100}
                    {:number "AN9922" :total_height 72 :total_width 30.5 :shelf 4 :price 1518 :weight 120}
                    {:number "AN9921" :total_height 84 :total_width 30.5 :shelf 5 :price 1669 :weight 130}
                    {:number "AN9920" :total_height 96 :total_width 30.5 :shelf 6 :price 1771 :weight 150}
                    {:number "AN9937" :total_height 30 :total_width 36.5 :shelf 1 :price 864 :weight 60}
                    {:number "AN9936" :total_height 36 :total_width 36.5 :shelf 2 :price 992 :weight 75}
                    {:number "AN9935" :total_height 42 :total_width 36.5 :shelf 2 :price 1159 :weight 90}
                    {:number "AN9934" :total_height 48 :total_width 36.5 :shelf 3 :price 1252 :weight 95}
                    {:number "AN9933" :total_height 60 :total_width 36.5 :shelf 3 :price 1419 :weight 110}
                    {:number "AN9932" :total_height 72 :total_width 36.5 :shelf 4 :price 1644 :weight 130}
                    {:number "AN9931" :total_height 84 :total_width 36.5 :shelf 5 :price 1825 :weight 140}
                    {:number "AN9930" :total_height 96 :total_width 36.5 :shelf 6 :price 1905 :weight 160}]
             :option[{:name "A41"} {:name "061"} {:name "A32"} {:name "060"} {:name "033a"} {:name "A47"} {:name "A49"}]}

            {:line "Nova"
             :category "Bookcase"
             :name "Open Horizontal Bookcase"
             :description "13\" OA depth. (Tops) 11\" Inside Depth"
             :item [{:number "AN9980" :total_height 30 :total_width 42 :shelf 1 :price 1303 :weight 55}
                    {:number "AN9981" :total_height 48 :total_width 42 :shelf 3 :price 1838 :weight 85}
                    {:number "AN9983" :total_height 30 :total_width 48 :shelf 1 :price 1480 :weight 65}
                    {:number "AN9984" :total_height 48 :total_width 48 :shelf 3 :price 1997 :weight 95}
                    {:number "AN9985" :total_height 30 :total_width 69 :shelf 1 :price 1638 :weight 75}
                    {:number "AN9986" :total_height 48 :total_width 69 :shelf 3 :price 2472 :weight 110}
                    {:number "AN9987" :total_height 30 :total_width 72 :shelf 1 :price 1768 :weight 100}
                    {:number "AN9988" :total_height 48 :total_width 72 :shelf 3 :price 2530 :weight 150}]
             :option[{:name "A42"} {:name "061"} {:name "A32"} {:name "033a"}]}

            {:line "Nova"
             :category "Cabinet"
             :name "CPU Cabinet"
             :description "with keyboard drawer, grommets and black casters. 29.5\" high."
             :item [{:number "AN9285" :total_width 36 :total_depth 25 :price 2012 :weight 125}
                    {:number "AN9286" :total_width 42 :total_depth 25 :price 2103 :weight 135}
                    {:number "AN9287" :total_width 48 :total_depth 25 :price 2196 :weight 150}]}

            {:line "Nova"
             :category "Table"
             :name "Telephone Table"
             :description "with hinged door and one adjustable shelf. Door hinged on left. 29.5\" OA ht. Opening is 4\" high."
             :item [{:number "AN9473" :total_width 18.5 :total_depth 19 :price 1045 :weight 65}
                    {:number "AN9474" :total_width 18.5 :total_depth 25 :price 1177 :weight 70}]
             :option[{:name "033a"} {:name "A17"} {:name "011"}]}

            {:line "Nova"
             :category "Part"
             :name "Pedestal"
             :description "with two box drawers and one file drawer. 29.5\" high."
             :item [{:number "AN9470" :total_width 18.5 :total_depth 19 :price 1160 :weight 85}
                    {:number "AN9480" :total_width 18.5 :total_depth 25 :price 1288 :weight 95}]
             :option[{:name "012"} {:name "A41"} {:name "021"}]}

            {:line "Nova"
             :category "Part"
             :name "Telephone Pedestal"
             :description "Hinged door with one adjustable shelf. Door hinged on left. 29.5\" OA ht."
             :item [{:number "AN9471" :total_width 18.5 :total_depth 19 :price 983 :weight 65}
                    {:number "AN9472" :total_width 18.5 :total_depth 25 :price 1111 :weight 70}]
             :option[{:name "011"} {:name "A41"} {:name "A17"}]}

            {:line "Nova"
             :category "Part"
             :name "Movable Pedestal"
             :description "Box/box/file. Self top. 27.5\" high. For use with credenza shells and table desks"
             :item [{:number "AN9450" :total_width 18.5 :total_depth 18 :price 1122 :weight 75}
                    {:number "AN9460" :total_width 18.5 :total_depth 24 :price 1168 :weight 85}]
             :option[{:name "A41"}]}

            {:line "Nova"
             :category "Part"
             :name "Desk Top"
             :description "Tops have a 2\" builtup hardwood edge with a configurated edge on two long sides"
             :item [{:number "AN9410" :total_width 60 :total_depth 30 :price 688 :weight 60}
                    {:number "AN9411" :total_width 66 :total_depth 30 :price 1073 :weight 65}
                    {:number "AN9412" :total_width 66 :total_depth 36 :price 1194 :weight 75}
                    {:number "AN9413" :total_width 72 :total_depth 36 :price 1427 :weight 85}
                    {:number "AN9414" :total_width 78 :total_depth 36 :price 1595 :weight 90}]
             :option[{:name "033a"}]}

            {:line "Nova"
             :category "Part"
             :name "Bullet End Desk Top"
             :description "Tops have a 2\"  hardwood edge with a configurated edge on two long sides and bullet end"
             :item [{:number "AN9415" :total_width 30 :total_depth 60 :price 1975 :weight 55}
                    {:number "AN9416" :total_width 72 :total_depth 36 :price 2227 :weight 75}
                    {:number "AN9417" :total_width 84 :total_depth 36 :price 2618 :weight 95}
                    {:number "AN9418" :total_width 90 :total_depth 42 :price 2822 :weight 125}
                    {:number "AN9419" :total_width 96 :total_depth 42 :price 3010 :weight 150}]}

            {:line "Nova"
             :category "Part"
             :name "Full Modesty Panel"
             :description "27\" high."
             :item [{:number "AN9580" :total_width 24 :price 209 :weight 15}
                    {:number "AN9581" :total_width 30 :price 250 :weight 20}
                    {:number "AN9582" :total_width 36 :price 329 :weight 25}
                    {:number "AN9583" :total_width 42 :price 381 :weight 30}
                    {:number "AN9584" :total_width 48 :price 460 :weight 35}]
             :option[{:name "033a"}]}

            {:line "Nova"
             :category "Part"
             :name "3/4 Modesty Panel"
             :description "18\" high."
             :item [{:number "AN9585" :total_width 24 :price 193 :weight 12}
                    {:number "AN9586" :total_width 30 :price 228 :weight 15}
                    {:number "AN9587" :total_width 36 :price 295 :weight 20}
                    {:number "AN9588" :total_width 42 :price 343 :weight 25}
                    {:number "AN9589" :total_width 48 :price 416 :weight 30}]
             :option[{:name "033a"}]}

            {:line "Nova"
             :category "Part"
             :name "Full Wrap Around Modesty Panel"
             :description "for bullet desk tops."
             :item [{:number "AN9990" :total_width 72 :total_depth 36 :price 986 :weight 95}
                    {:number "AN9991" :total_width 84 :total_depth 36 :price 1052 :weight 125}
                    {:number "AN9992" :total_width 90 :total_depth 42 :price 1117 :weight 150}]}

            {:line "Nova"
             :category "Part"
             :name "Wood Cylinder Base with 12\" high kinked modesty panel"
             :description "for bullet desk tops only."
             :item [{:number "AN9960" :total_width 72 :total_depth 36 :price 903 :weight 85}
                    {:number "AN9961" :total_width 84 :total_depth 36 :price 957 :weight 115}
                    {:number "AN9962" :total_width 90 :total_depth 42 :price 1011 :weight 140}]}

            {:line "Activa"
             :category "Desk"
             :name "Single Pedestal Desk"
             :description "3/4 pedestal with box/file drawers. 3/4modesty panel. For use alone or with returns on following page. Specify pedestal left or right"
             :item [{:number "AC5500" :total_width 48 :total_depth 24 :kneespace 28 :price 2238 :weight 175}
                    {:number "AC5502" :total_width 60 :total_depth 30 :kneespace 40 :price 2378 :weight 205}
                    {:number "AC5503" :total_width 66 :total_depth 30 :kneespace 46 :price 2426 :weight 225}
                    {:number "AC5504" :total_width 66 :total_depth 36 :kneespace 46 :price 2448 :weight 255}
                    {:number "AC5505" :total_width 72 :total_depth 36 :kneespace 52 :price 2547 :weight 275}]
             :option[{:name "A11"}]}

            {:line "Activa"
             :category "Desk"
             :name "Double Pedestal Desk"
             :description "3/4 pedestals with box/file drawers. 3/4 modesty panel."
            
             :item [{:number "AC5000" :total_width 60 :total_depth 30 :kneespace 23 :price 2742 :weight 265}
                    {:number "AC5001" :total_width 66 :total_depth 30 :kneespace 29 :price 2820 :weight 270}
                    {:number "AC5002" :total_width 66 :total_depth 36 :kneespace 29 :price 2882 :weight 300}
                    {:number "AC5003" :total_width 72 :total_depth 36 :kneespace 35 :price 2916 :weight 325}]
             :option[{:name "A11"}]}

            {:line "Activa"
             :category "Desk"
             :name "Single Pedestal Desk"
             :description "Full pedestal with box/box/file drawers. Full modesty panel. For use alone or with returns on page 196 Specify pedestal left or right."
             :item [{:number "AC5221" :total_width 48 :total_depth 24 :kneespace 27 :price 2448 :weight 185}
                    {:number "AC5222" :total_width 60 :total_depth 30 :kneespace 39 :price 2574 :weight 215}
                    {:number "AC5223" :total_width 66 :total_depth 30 :kneespace 45 :price 2609 :weight 235}
                    {:number "AC5224" :total_width 66 :total_depth 36 :kneespace 45 :price 2654 :weight 265}
                    {:number "AC5225" :total_width 72 :total_depth 36 :kneespace 51 :price 2749 :weight 285}]
             :option[{:name "020"} {:name "013"} {:name "012"} {:name "033a"}]}

            {:line "Activa"
             :category "Desk"
             :name "Double Pedestal Desk"
             :description "Full pedestals with box/box/file drawers. Full modesty panel. letter size pedestal"
             :item [{:number "AC5226" :total_width 60 :total_depth 30 :kneespace 26 :price 3041 :weight 300}
                    {:number "AC5227" :total_width 66 :total_depth 30 :kneespace 26 :price 3116 :weight 330}
                    {:number "AC5228" :total_width 66 :total_depth 36 :kneespace 26 :price 3179 :weight 340}
                    {:number "AC5229" :total_width 72 :total_depth 36 :kneespace 32 :price 3233 :weight 370}]
             :option[{:name "020"} {:name "013"} {:name "012"} {:name "033a"}]}

            {:line "Activa"
             :category "Table"
             :name "Bullet End Table Desk Free Standing"
             :description "Center pencil drawer. 6\" dia. black cylinder. For use alone or with returns on following page. Specify bullet end left or right."
             :item [{:number "AC5060" :total_width 66 :total_depth 30 :kneespace 42 :price 2749 :weight 150}
                    {:number "AC5061" :total_width 72 :total_depth 36 :kneespace 48 :price 2878 :weight 160}
                    {:number "AC5062" :total_width 84 :total_depth 36 :kneespace 60 :price 3073 :weight 180}]
             :option[{:name "013"} {:name "A10"} {:name "A11"} {:name "033a"}]}

            {:line "Activa"
             :category "Desk"
             :name "Expanded Bullet End Table Desk"
             :description "Center pencil drawer. 6\" dia. black cylinder. For use alone or with returns on following page. Specify bullet end left or right."
             :item [{:number "AC5013" :total_width 66 :total_depth 30 :kneespace 42 :price 3166 :weight 150}
                    {:number "AC5014" :total_width 72 :total_depth 36 :kneespace 48 :price 3371 :weight 160}
                    {:number "AC5015" :total_width 84 :total_depth 36 :kneespace 60 :price 3606 :weight 180}]
             :option[{:name "013"} {:name "033a"} {:name "030"}]}

            {:line "Activa"
             :category "Return"
             :name "Bridge Return"
             :description "For connecting single pedestal desks with credenzas to create a U workstation."
             :item [{:number "AC5530" :total_width 42 :total_depth 19.25 :kneespace 42 :price 951 :weight 70}
                    {:number "AC5531" :total_width 48 :total_depth 19.25 :kneespace 48 :price 1050 :weight 80}
                    {:number "AC5532" :total_width 54 :total_depth 19.25 :kneespace 54 :price 1083 :weight 90}
                    {:number "AC5533" :total_width 60 :total_depth 19.25 :kneespace 60 :price 1131 :weight 95}
                    {:number "AC5536" :total_width 42 :total_depth 24 :kneespace 42 :price 1029 :weight 75}
                    {:number "AC5537" :total_width 48 :total_depth 24 :kneespace 48 :price 1122 :weight 85}
                    {:number "AC5538" :total_width 54 :total_depth 24 :kneespace 54 :price 1150 :weight 95}
                    {:number "AC5539" :total_width 60 :total_depth 24 :kneespace 60 :price 1240 :weight 100}]
             :option[{:name "013"} {:name "033a"} {:name "030"}]}

            {:line "Activa"
             :category "Return"
             :name "Return Shell"
             :description "Specify left or right return, and executive or secretarial height"
             :item [{:number "AC5041" :total_width 42 :total_depth 19.25 :kneespace 41 :price 620 :weight 95}
                    {:number "AC5042" :total_width 48 :total_depth 19.25 :kneespace 47 :price 728 :weight 105}
                    {:number "AC5043" :total_width 54 :total_depth 19.25 :kneespace 53 :price 820 :weight 115}
                    {:number "AC5044" :total_width 60 :total_depth 19.25 :kneespace 59 :price 933 :weight 125}
                    {:number "AC5046" :total_width 42 :total_depth 24 :kneespace 41 :price 722 :weight 100}
                    {:number "AC5047" :total_width 48 :total_depth 24 :kneespace 47 :price 837 :weight 130}
                    {:number "AC5048" :total_width 54 :total_depth 24 :kneespace 53 :price 943 :weight 140}
                    {:number "AC5049" :total_width 60 :total_depth 24 :kneespace 59 :price 1073 :weight 150}]
             :option[{:name "013"} {:name "033a"} {:name "030"}]}

            {:line "Activa"
             :category "Return"
             :name "3/4 Pedestal Return with box/file drawers"
             :description "Specify left or right return, and executive or secretarial height"
             :item [{:number "AC5522" :total_width 42 :total_depth 19.25 :kneespace 24 :price 1158 :weight 115}
                    {:number "AC5523" :total_width 48 :total_depth 19.25 :kneespace 30 :price 1196 :weight 120}
                    {:number "AC5524" :total_width 60 :total_depth 19.25 :kneespace 42 :price 1463 :weight 140}]
             :option[{:name "012"} {:name "030"} {:name "033a"}]}

            {:line "Activa"
             :category "Return"
             :name "Return with box/box/file drawers"
             :description "Specify left or right return, and executive or secretarial height."
             :item [{:number "AC5244" :total_width 42 :total_depth 19.25 :kneespace 22 :price 1505 :weight 125}
                    {:number "AC5245" :total_width 48 :total_depth 19.25 :kneespace 28 :price 1566 :weight 140}
                    {:number "AC5246" :total_width 60 :total_depth 19.25 :kneespace 40 :price 1715 :weight 155}
                    {:number "AC5247" :total_width 42 :total_depth 24 :kneespace 22 :price 1745 :weight 135}
                    {:number "AC5248" :total_width 48 :total_depth 24 :kneespace 28 :price 1867 :weight 150}
                    {:number "AC5249" :total_width 60 :total_depth 24 :kneespace 40 :price 2016 :weight 165}]
             :option[{:name "012"} {:name "030"} {:name "033a"}]}

            {:line "Activa"
             :category "Return"
             :name "Return with one box/box/ pedestal and one file/file pedestal"
             :description "Specify left or right return, and executive or secretarial height."
             :item [{:number "AC5250" :total_width 48 :total_depth 19.25 :kneespace 9 :price 2402 :weight 200}
                    {:number "AC5251" :total_width 60 :total_depth 19.25 :kneespace 21 :price 2618 :weight 220}
                    {:number "AC5252" :total_width 48 :total_depth 24 :kneespace 9 :price 2762 :weight 220}
                    {:number "AC5253" :total_width 60 :total_depth 24 :kneespace 21 :price 3010 :weight 240}]
             :option[{:name "012"} {:name "030"} {:name "033a"}]}

            {:line "Activa"
             :category "Return"
             :name "Return with one 36\" lateral file pedestal"
             :description "Specify left or right return, and executive or secretarial height."
             :item [{:number "AC5255" :total_width 48 :total_depth 19.25 :kneespace 10.75 :price 2046 :weight 235}
                    {:number "AC5256" :total_width 60 :total_depth 19.25 :kneespace 22.75 :price 2191 :weight 245}
                    {:number "AC5257" :total_width 48 :total_depth 24 :kneespace 10.75 :price 2173 :weight 245}
                    {:number "AC5258" :total_width 60 :total_depth 24 :kneespace 22.75 :price 2391 :weight 265}]
             :option[{:name "012"} {:name "030"} {:name "033a"}]}

            {:line "Activa"
             :category "Return"
             :name "Rear Unit one box/box/file pedestal"
             :description "Specify pedestal left or right 96\" units shipped KD"
             :item [{:number "AC5130" :total_width 60 :total_depth 18 :kneespace 39 :price 2161 :weight 200}
                    {:number "AC5131" :total_width 66 :total_depth 18 :kneespace 45 :price 2288 :weight 210}
                    {:number "AC5132" :total_width 72 :total_depth 18 :kneespace 51 :price 2430 :weight 220}
                    {:number "AC5133" :total_width 84 :total_depth 18 :kneespace 63 :price 3130 :weight 230}
                    {:number "AC5134" :total_width 96 :total_depth 18 :kneespace 75 :price 3413 :weight 250}
                    {:number "AC5135" :total_width 60 :total_depth 24 :kneespace 39 :price 2369 :weight 225}
                    {:number "AC5136" :total_width 66 :total_depth 24 :kneespace 45 :price 2516 :weight 235}
                    {:number "AC5137" :total_width 72 :total_depth 24 :kneespace 51 :price 2672 :weight 245}
                    {:number "AC5138" :total_width 84 :total_depth 24 :kneespace 63 :price 3443 :weight 255}
                    {:number "AC5139" :total_width 96 :total_depth 24 :kneespace 75 :price 3755 :weight 275}]
             :option[{:name "012"} {:name "030"} {:name "033a"} {:name "010"}]}

            {:line "Activa"
             :category "Return"
             :name "Rear Unit with one 36\" lateral file pedestal"
             :description "Specify pedestal left or right 96\" units shipped KD"
             :item [{:number "AC5270" :total_width 60 :total_depth 18 :kneespace 21 :price 2636 :weight 265}
                    {:number "AC5271" :total_width 66 :total_depth 18 :kneespace 27 :price 2787 :weight 275}
                    {:number "AC5272" :total_width 72 :total_depth 18 :kneespace 33 :price 2883 :weight 285}
                    {:number "AC5273" :total_width 84 :total_depth 18 :kneespace 45 :price 3166 :weight 295}
                    {:number "AC5274" :total_width 96 :total_depth 18 :kneespace 57 :price 3690 :weight 320}
                    {:number "AC5275" :total_width 60 :total_depth 24 :kneespace 21 :price 2874 :weight 275}
                    {:number "AC5276" :total_width 66 :total_depth 24 :kneespace 27 :price 3066 :weight 285}
                    {:number "AC5277" :total_width 72 :total_depth 24 :kneespace 33 :price 3172 :weight 295}
                    {:number "AC5278" :total_width 84 :total_depth 24 :kneespace 45 :price 3483 :weight 320}
                    {:number "AC5279" :total_width 96 :total_depth 24 :kneespace 57 :price 4059 :weight 365}]
             :option[{:name "012"} {:name "030"} {:name "033a"} {:name "010"}]}

            {:line "Activa"
             :category "Return"
             :name "Rear Unit with one box/box/file pedestal and one 36\" lateral file pedestal"
             :description "Specify pedestal left or right 96\" and 120\" units shipped KD 36\" Storage cabinet instead of Lateral file deduct $200"
             :item [{:number "AC5280" :total_width 84 :total_depth 18 :kneespace 27 :price 3694 :weight 350}
                    {:number "AC5281" :total_width 90 :total_depth 18 :kneespace 33 :price 3957 :weight 365}
                    {:number "AC5282" :total_width 96 :total_depth 18 :kneespace 39 :price 4016 :weight 375}
                    {:number "AC5283" :total_width 120 :total_depth 18 :kneespace 63 :price 4330 :weight 420}
                    {:number "AC5285" :total_width 84 :total_depth 24 :kneespace 27 :price 4302 :weight 370}
                    {:number "AC5286" :total_width 90 :total_depth 24 :kneespace 33 :price 4490 :weight 385}
                    {:number "AC5287" :total_width 96 :total_depth 24 :kneespace 39 :price 4681 :weight 400}
                    {:number "AC5288" :total_width 120 :total_depth 24 :kneespace 63 :price 5042 :weight 450}]
             :option[{:name "012"} {:name "030"} {:name "033a"} {:name "010"}]}

            {:line "Activa"
             :category "Return"
             :name "Rear Unit with two 36\" lateral file pedestals"
             :description "Specify pedestal left or right 96\" and 120\" units shipped KD"
             :item [{:number "AC5290" :total_width 90 :total_depth 18 :kneespace 15 :price 4358 :weight 465}
                    {:number "AC5291" :total_width 96 :total_depth 18 :kneespace 21 :price 4557 :weight 475}
                    {:number "AC5292" :total_width 120 :total_depth 18 :kneespace 45 :price 4886 :weight 525}
                    {:number "AC5295" :total_width 90 :total_depth 24 :kneespace 15 :price 4828 :weight 485}
                    {:number "AC5296" :total_width 96 :total_depth 24 :kneespace 21 :price 5012 :weight 500}
                    {:number "AC5297" :total_width 120 :total_depth 24 :kneespace 45 :price 5376 :weight 575}]
             :option[{:name "012"} {:name "030"} {:name "033a"} {:name "010"}]}

            {:line "Activa"
             :category "Credenza"
             :name "Cabinet Credenza"
             :description "Full pedestal. Adjustable shelf"
             :item [{:number "AC5540" :total_width 36 :total_depth 18 :door 2 :price 1484 :weight 170}
                    {:number "AC5541" :total_width 55 :total_depth 18 :door 3 :price 2168 :weight 225}
                    {:number "AC5542" :total_width 72 :total_depth 18 :door 4 :price 2829 :weight 285}
                    {:number "AC5543" :total_width 90 :total_depth 18 :door 5 :price 3769 :weight 350}
                    {:number "AC5544" :total_width 108 :total_depth 18 :door 6 :price 4733 :weight 420}
                    {:number "AC5545" :total_width 36 :total_depth 24 :door 2 :price 1634 :weight 200}
                    {:number "AC5546" :total_width 55 :total_depth 24 :door 3 :price 2384 :weight 250}
                    {:number "AC5547" :total_width 72 :total_depth 24 :door 4 :price 3130 :weight 320}
                    {:number "AC5548" :total_width 90 :total_depth 24 :door 5 :price 4226 :weight 390}
                    {:number "AC5549" :total_width 108 :total_depth 24 :door 6 :price 5646 :weight 450}]
             :option[{:name "010"}]}

            {:line "Activa"
             :category "Credenza"
             :name "36\" High Buffet Credenza"
             :description "Drawers and doors"
             :item [{:number "AC5550" :total_width 38.5 :total_depth 18 :door 2 :drawer 1 :price 2827 :weight 240}
                    {:number "AC5551" :total_width 75.25 :total_depth 18 :door 4 :drawer 2 :price 4088 :weight 300}
                    {:number "AC5554" :total_width 38.5 :total_depth 24 :door 2 :drawer 1 :price 3109 :weight 225}
                    {:number "AC5555" :total_width 75.25 :total_depth 24 :door 4 :drawer 2 :price 4498 :weight 375}]
             :option[{:name "010"} {:name "013"}]}

            {:line "Activa"
             :category "Credenza"
             :name "File Credenza"
             :item [{:number "AC5560" :total_width 40.25 :total_depth 18 :drawer 4 :price 2321 :weight 225}
                    {:number "AC5561" :total_width 59.50 :total_depth 18 :drawer 6 :price 2966 :weight 300}
                    {:number "AC5562" :total_width 78.75 :total_depth 18 :drawer 8 :price 3732 :weight 350}
                    {:number "AC5565" :total_width 40.25 :total_depth 24 :drawer 4 :price 2516 :weight 250}
                    {:number "AC5566" :total_width 59.50 :total_depth 24 :drawer 6 :price 3203 :weight 325}
                    {:number "AC5567" :total_width 78.75 :total_depth 24 :drawer 8 :price 4031 :weight 375}]
             :option[{:name "012"}]}

            {:line "Activa"
             :category "Credenza"
             :name "Combination Credenza"
             :description "Full pedestal. 2 doors, 4 box, and 2 file drawers. Adjustable shelf"
             :item [{:number "AC5570" :total_width 72 :total_depth 18 :price 3326 :weight 350}
                    {:number "AC5571" :total_width 72 :total_depth 24 :price 3660 :weight 375}]
             :option[{:name "010"} {:name "012"}]}

            {:line "Activa"
             :category "Credenza"
             :name "Writing Credenza"
             :description "Full pedestals. 4 box and two file drawers."
             :item [{:number "AC5573" :total_width 60 :total_depth 18 :kneespace 21 :price 2769 :weight 250}
                    {:number "AC5574" :total_width 66 :total_depth 18 :kneespace 27 :price 2859 :weight 275}
                    {:number "AC5575" :total_width 72 :total_depth 18 :kneespace 33 :price 2961 :weight 300}
                    {:number "AC5576" :total_width 60 :total_depth 24 :kneespace 21 :price 3101 :weight 270}
                    {:number "AC5577" :total_width 66 :total_depth 24 :kneespace 27 :price 3144 :weight 300}
                    {:number "AC5578" :total_width 72 :total_depth 24 :kneespace 33 :price 3262 :weight 325}]
             :option[{:name "012"} {:name "030"} {:name "033a"} {:name "010"}]}

            {:line "Activa"
             :category "Credenza"
             :name "Writing Credenza"
             :description "Full pedestals Four 36\" lateral files. 96\" and 120\" units are shipped KD"
             :item [{:number "AC5581" :total_width 96 :total_depth 18 :kneespace 21 :price 3894 :weight 420}
                    {:number "AC5582" :total_width 120 :total_depth 18 :kneespace 45 :price 4442 :weight 475}
                    {:number "AC5584" :total_width 96 :total_depth 24 :kneespace 21 :price 4438 :weight 455}
                    {:number "AC5585" :total_width 120 :total_depth 24 :kneespace 45 :price 4976 :weight 495}]
             :option[{:name "012"} {:name "030"} {:name "033a"} {:name "010"}]}

            {:line "Activa"
             :category "Credenza"
             :name "High Profile Cabinet Credenza"
             :description "3/4 pedestal. Adjustable shelf"
             :item [{:number "AC5069" :total_width 36 :total_depth 18 :door 2 :price 1335 :weight 150}
                    {:number "AC5370" :total_width 55 :total_depth 18 :door 3 :price 1950 :weight 200}
                    {:number "AC5071" :total_width 72 :total_depth 18 :door 4 :price 2546 :weight 250}]
             :option[{:name "012"} {:name "030"} {:name "033a"} {:name "010"}]}

            {:line "Activa"
             :category "Credenza"
             :name "High Profile Combination Credenza"
             :description "3/4 pedestal. 2 doors, 2 box, and 2 file drawers. Adjustable shelf"
             :item [{:number "AC5008" :total_width 72 :total_depth 18 :price 2993 :weight 275}]
             :option[{:name "012"} {:name "030"} {:name "033a"} {:name "010"}]}

            {:line "Activa"
             :category "Credenza"
             :name "High Profile Writing Credenza"
             :description "3/4 pedestal. 2 box and 2 file drawers"
             :item [{:number "AC5307" :total_width 60 :total_depth 18 :kneespace 25 :price 2492 :weight 180}
                    {:number "AC5308" :total_width 66 :total_depth 18 :kneespace 31 :price 2573 :weight 195}
                    {:number "AC5009" :total_width 72 :total_depth 18 :kneespace 37 :price 2665 :weight 215}]
             :option[{:name "012"} {:name "030"} {:name "033a"} {:name "010"}]}

            {:line "Activa"
             :category "File"
             :name "File"
             :description "2 drawers. 29.5\" high"
             :item [{:number "AC5320" :total_width 21 :total_depth 18 :price 1195 :weight 90}
                    {:number "AC5321" :total_width 21 :total_depth 24 :price 1333 :weight 100}]
             :option[{:name "010"} {:name "012"}]}

            {:line "Activa"
             :category "File"
             :name "Lateral File"
             :description "2 drawers. 29.5\" high."
             :item [{:number "AC5325" :total_width 32.5 :total_depth 18 :price 1718 :weight 195}
                    {:number "AC5326" :total_width 38.5 :total_depth 18 :price 1768 :weight 210}
                    {:number "AC5466" :total_width 32.5 :total_depth 24 :price 1890 :weight 230}
                    {:number "AC5467" :total_width 38.5 :total_depth 24 :price 1946 :weight 245}]
             :option[{:name "010"} {:name "012"}]}

            {:line "Activa"
             :category "File"
             :name "Lateral File"
             :description "4 drawers. 29.5\" high"
             :item [{:number "AC5329" :total_width 62.5 :total_depth 18 :price 3353 :weight 360}
                    {:number "AC5330" :total_width 74.5 :total_depth 18 :price 3429 :weight 385}
                    {:number "AC5475" :total_width 62.5 :total_depth 24 :price 3685 :weight 400}
                    {:number "AC5476" :total_width 74.5 :total_depth 24 :price 3912 :weight 450}]
             :option[{:name "010"} {:name "012"}]}

            {:line "Activa"
             :category "Credenza"
             :name "Lateral File / Storage Credenza"
             :description "2 lateral file drawers, 30\" wide on 60\" units; 36\" wide on 72\" units. 2 hinged doors. 29.5\" OA height."
             :item [{:number "AC5462" :total_width 60 :total_depth 18 :price 3323 :weight 310}
                    {:number "AC5463" :total_width 72 :total_depth 18 :price 3492 :weight 335}
                    {:number "AC5464" :total_width 60 :total_depth 24 :price 3676 :weight 350}
                    {:number "AC5465" :total_width 72 :total_depth 24 :price 3840 :weight 400}]
             :option[{:name "010"} {:name "012"}]}

            {:line "Activa"
             :category "File"
             :name "Lateral File"
             :description "3 drawers. 41.5\" high. Built-in anti-tilt mechanism."
             :item [{:number "AC5327" :total_width 32.5 :total_depth 18 :price 2603 :weight 255}
                    {:number "AC5328" :total_width 38.5 :total_depth 18 :price 2693 :weight 275}
                    {:number "AC5468" :total_width 32.5 :total_depth 24 :price 2883 :weight 275}
                    {:number "AC5469" :total_width 38.5 :total_depth 24 :price 3022 :weight 300}]
             :option[{:name "012"}]}

            {:line "Activa"
             :category "File"
             :name "Lateral File"
             :description "4 drawers. 53\" high. Built-in anti-tilt mechanism."
             :item [{:number "AC5470" :total_width 32.5 :total_depth 18 :price 3425 :weight 275}
                    {:number "AC5471" :total_width 38.5 :total_depth 18 :price 3714 :weight 300}
                    {:number "AC5472" :total_width 32.5 :total_depth 24 :price 3837 :weight 300}
                    {:number "AC5473" :total_width 38.5 :total_depth 24 :price 4158 :weight 350}]
             :option[{:name "012"}]}

            {:line "Activa"
             :category "Cabinet"
             :name "Wall Hung Cabinet with flipper doors."
             :description "18\" OA height. 12.5\" inside clearance."
             :item [{:number "AC5350" :total_width 30 :total_depth 14 :door 1 :price 967 :weight 55}
                    {:number "AC5351" :total_width 36 :total_depth 14 :door 1 :price 1050 :weight 60}
                    {:number "AC5358" :total_width 48 :total_depth 14 :door 2 :price 1143 :weight 70}
                    {:number "AC5353" :total_width 55 :total_depth 14 :door 2 :price 1300 :weight 75}
                    {:number "AC5359" :total_width 60 :total_depth 14 :door 2 :price 1385 :weight 80}
                    {:number "AC5356" :total_width 72 :total_depth 14 :door 2 :price 1452 :weight 85}]
             :option[{:name "011"}]}

            {:line "Activa"
             :category "Cabinet"
             :name "Wall Hung Cabinet with hinged doors."
             :description "18\" OA height. 15\" inside clearance."
             :item [{:number "AC5610" :total_width 30 :total_depth 14 :door 2 :price 876 :weight 55}
                    {:number "AC5611" :total_width 36 :total_depth 14 :door 2 :price 959 :weight 60}
                    {:number "AC5618" :total_width 48 :total_depth 14 :door 2 :price 1053 :weight 70}
                    {:number "AC5613" :total_width 55 :total_depth 14 :door 3 :price 1209 :weight 75}
                    {:number "AC5619" :total_width 60 :total_depth 14 :door 4 :price 1294 :weight 80}
                    {:number "AC5616" :total_width 72 :total_depth 14 :door 4 :price 1362 :weight 85}]
             :option[{:name "011"}]}

            {:line "Activa"
             :category "Hutch"
             :name "Flipper Door Hutch for credenzas."
             :description "42\" OA height. 12.5\" inside clearance on cabinet. Optional: 48\" OA ht. +5%"
             :item [{:number "AC5360" :total_width 30 :total_depth 14 :door 1 :price 1519 :weight 130}
                    {:number "AC5361" :total_width 36 :total_depth 14 :door 1 :price 1590 :weight 135}
                    {:number "AC5368" :total_width 48 :total_depth 14 :door 2 :price 2185 :weight 140}
                    {:number "AC5363" :total_width 55 :total_depth 14 :door 2 :price 2518 :weight 145}
                    {:number "AC5369" :total_width 60 :total_depth 14 :door 2 :price 2913 :weight 155}
                    {:number "AC5366" :total_width 72 :total_depth 14 :door 2 :price 3027 :weight 170}]
             :option[{:name "010"} {:name "070"} {:name "071"} {:name "072"}]}

            {:line "Activa"
             :category "Hutch"
             :name "Hinged Door Hutch for credenzas."
             :description "42\" OA height. 15\" inside clearance on cabinet. Optional: 48\" OA ht. +5%"
             :item [{:number "AC5620" :total_width 30 :total_depth 14 :door 2 :price 1496 :weight 130}
                    {:number "AC5621" :total_width 36 :total_depth 14 :door 2 :price 1575 :weight 135}
                    {:number "AC5628" :total_width 48 :total_depth 14 :door 2 :price 2086 :weight 140}
                    {:number "AC5623" :total_width 55 :total_depth 14 :door 3 :price 2483 :weight 145}
                    {:number "AC5629" :total_width 60 :total_depth 14 :door 4 :price 2814 :weight 155}
                    {:number "AC5626" :total_width 72 :total_depth 14 :door 4 :price 2986 :weight 170}]
             :option[{:name "010"} {:name "070"} {:name "071"} {:name "072"}]}

            {:line "Activa"
             :category "Hutch"
             :name "Open Hutch for credenzas."
             :description "Adjustable shelves. 42\" OA height."
             :item [{:number "AC5380" :total_width 30 :total_depth 14 :shelf 2 :price 998 :weight 145}
                    {:number "AC5381" :total_width 36 :total_depth 14 :shelf 2 :price 1071 :weight 150}
                    {:number "AC5388" :total_width 48 :total_depth 14 :shelf 2 :price 1357 :weight 155}
                    {:number "AC5383" :total_width 55 :total_depth 14 :shelf 4 :price 1621 :weight 160}
                    {:number "AC5389" :total_width 60 :total_depth 14 :shelf 4 :price 1901 :weight 170}
                    {:number "AC5386" :total_width 72 :total_depth 14 :shelf 4 :price 2251 :weight 185}]
             :option[{:name "010"} {:name "070"} {:name "071"} {:name "072"}]}

            {:line "Activa"
             :category "Hutch"
             :name "Organizer"
             :description "for underneath hutch. Black only. 4\" high"
             :item [{:number "AC5390" :total_width 30 :total_depth 13 :price 454 :weight 27}
                    {:number "AC5391" :total_width 36 :total_depth 13 :price 460 :weight 30}
                    {:number "AC5392" :total_width 48 :total_depth 13 :price 499 :weight 32}
                    {:number "AC5393" :total_width 55 :total_depth 13 :price 526 :weight 42}
                    {:number "AC5394" :total_width 60 :total_depth 13 :price 602 :weight 45}
                    {:number "AC5395" :total_width 72 :total_depth 13 :price 652 :weight 50}]
             :option[{:name "010"} {:name "070"} {:name "071"} {:name "072"}]}

            {:line "Activa"
             :category "Table"
             :name "Corner Computer Table"
             :description "Working edge at 45 degrees. Keyboard drawer and grommet. For use by itself, or with returns shown on p. 306."
             :item [{:number "AC5040" :total_width 40 :total_depth 40 :price 1871 :weight 225}
                    {:number "AC5450" :total_width 48 :total_depth 48 :price 2528 :weight 275}]}

            {:line "Activa"
             :category "Cabinet"
             :name "Open CPU Cabinet"
             :description "29.5\" OA height. Supplied with keyboard drawer, shelf and grommet. Inside dimensions: 18\" deep unit  7\"W | 16.25\"D | 22.5\"H. 24\" deep unit 7\"W | 22.25\"D | 22.5\"H."
             :item [{:number "AC5051" :total_width 42 :total_depth 18 :price 1789 :weight 125}
                    {:number "AC5052" :total_width 48 :total_depth 18 :price 1908 :weight 150}
                    {:number "AC5053" :total_width 60 :total_depth 18 :price 2027 :weight 175}
                    {:number "AC5453" :total_width 42 :total_depth 24 :price 1966 :weight 140}
                    {:number "AC5454" :total_width 48 :total_depth 24 :price 2099 :weight 175}
                    {:number "AC5455" :total_width 60 :total_depth 24 :price 2230 :weight 200}]}

            {:line "Activa"
             :category "Table"
             :name "Printer Table"
             :description "with paper slot. 29.5\" OA height."
             :item [{:number "AC5036" :total_width 30 :total_depth 24 :price 1158 :weight 70}
                    {:number "AC5037" :total_width 36 :total_depth 24 :price 1217 :weight 80}
                    {:number "AC5038" :total_width 42 :total_depth 24 :price 1246 :weight 90}]
             :option[{:name "030"}]}

            {:line "Activa"
             :category "Table"
             :name "Telephone Pedestal"
             :description "Touch Latch Hinged door with one adjustable shelf. Door hinged on left. 29.5\" high"
             :item [{:number "AC5332" :total_width 19.5 :total_depth 18 :price 862 :weight 70}
                    {:number "AC5459" :total_width 19.5 :total_depth 24 :price 939 :weight 80}]
             :option[{:name "A16"} {:name "010"} {:name "A17"}]}

            {:line "Activa"
             :category "Part"
             :name "Add-On Pedestal"
             :description "No top; for mounting in credenza or desk shell. 28\" high. A - Box/box/file B - File/file"
             :item [{:number "AC5333A" :total_width 17 :total_depth 18 :price 943 :weight 75}
                    {:number "AC5334A" :total_width 17 :total_depth 24 :price 992 :weight 80}
                    {:number "AC5333B" :total_width 17 :total_depth 18 :price 943 :weight 75}
                    {:number "AC5334B" :total_width 17 :total_depth 24 :price 992 :weight 80}]
             :option[{:name "012"} {:name "021"}]}

            {:line "Activa"
             :category "Cabinet"
             :name "Wardrobe / Storage"
             :description "with shelf and rod. 2 doors. 18\" deep units shipped with coat hook instead of clothes rod."
             :item [{:number "AC5336" :total_height 72 :price 2803 :weight 200}
                    {:number "AC5337" :total_height 84 :price 2913 :weight 210}
                    {:number "AC5338" :total_height 72 :price 2928 :weight 220}
                    {:number "AC5339" :total_height 84 :price 3110 :weight 240}
                    {:number "AC5176" :total_height 72 :price 3118 :weight 225}
                    {:number "AC5177" :total_height 84 :price 3262 :weight 250}
                    {:number "AC5178" :total_height 72 :price 3299 :weight 255}
                    {:number "AC5179" :total_height 84 :price 3563 :weight 285}]
             :option[{:name "010"} {:name "A34"}]}

            {:line "Activa"
             :category "Bookcase"
             :name "Open Bookcase with Hinged Door Cabinet Bookcase depth 12\""
             :item [{:number "AC5182" :total_height 60 :total_width 18 :total_depth 30 :shelf 2 :price 2064 :weight 180}
                    {:number "AC5181" :total_height 72 :total_width 18 :total_depth 30 :shelf 3 :price 2306 :weight 190}
                    {:number "AC5180" :total_height 84 :total_width 18 :total_depth 30 :shelf 4 :price 2567 :weight 210}
                    {:number "AC5185" :total_height 60 :total_width 18 :total_depth 36 :shelf 2 :price 2255 :weight 210}
                    {:number "AC5184" :total_height 72 :total_width 18 :total_depth 36 :shelf 3 :price 2494 :weight 220}
                    {:number "AC5183" :total_height 84 :total_width 18 :total_depth 36 :shelf 4 :price 2761 :weight 235}
                    {:number "AC5170" :total_height 60 :total_width 24 :total_depth 30 :shelf 2 :price 2288 :weight 200}
                    {:number "AC5171" :total_height 72 :total_width 24 :total_depth 30 :shelf 3 :price 2648 :weight 210}
                    {:number "AC5172" :total_height 84 :total_width 24 :total_depth 30 :shelf 4 :price 2890 :weight 235}
                    {:number "AC5173" :total_height 60 :total_width 24 :total_depth 36 :shelf 2 :price 2498 :weight 210}
                    {:number "AC5174" :total_height 72 :total_width 24 :total_depth 36 :shelf 3 :price 2769 :weight 225}
                    {:number "AC5175" :total_height 84 :total_width 24 :total_depth 36 :shelf 4 :price 3130 :weight 245}]
             :option[{:name "010"}]}

            {:line "Activa"
             :category "Bookcase"
             :name "Open Bookcase with Lateral File"
             :description "Bookcase depth 12\""
             :item [{:number "AC5085" :total_height 60 :total_width 18 :total_depth 32.5 :shelf 1 :price 2430 :weight 215}
                    {:number "AC5084" :total_height 72 :total_width 18 :total_depth 32.5 :shelf 2 :price 2669 :weight 225}
                    {:number "AC5083" :total_height 84 :total_width 18 :total_depth 32.5 :shelf 3 :price 2932 :weight 235}
                    {:number "AC5088" :total_height 60 :total_width 18 :total_depth 38.5 :shelf 1 :price 2601 :weight 235}
                    {:number "AC5087" :total_height 72 :total_width 18 :total_depth 38.5 :shelf 2 :price 2773 :weight 245}
                    {:number "AC5086" :total_height 84 :total_width 18 :total_depth 38.5 :shelf 3 :price 3020 :weight 275}
                    {:number "AC5160" :total_height 60 :total_width 24 :total_depth 32.5 :shelf 1 :price 2672 :weight 235}
                    {:number "AC5161" :total_height 72 :total_width 24 :total_depth 32.5 :shelf 2 :price 2935 :weight 245}
                    {:number "AC5162" :total_height 84 :total_width 24 :total_depth 32.5 :shelf 3 :price 3224 :weight 260}
                    {:number "AC5163" :total_height 60 :total_width 24 :total_depth 38.5 :shelf 1 :price 2861 :weight 245}
                    {:number "AC5164" :total_height 72 :total_width 24 :total_depth 38.5 :shelf 2 :price 3076 :weight 270}
                    {:number "AC5165" :total_height 84 :total_width 24 :total_depth 38.5 :shelf 3 :price 3431 :weight 300}]
             :option[{:name "012"}]}

            {:line "Activa"
             :category "Bookcase"
             :name "Open Bookcase with Hinged Doors on Bottom."
             :description "12\" deep. Note: Units up to 48\" high have full doors."
             :item [{:number "AC5436" :total_height 30 :total_width 30 :shelf 1 :price 1103 :weight 80}
                    {:number "AC5435" :total_height 36 :total_width 30 :shelf 2 :price 1234 :weight 85}
                    {:number "AC5434" :total_height 42 :total_width 30 :shelf 2 :price 1334 :weight 95}
                    {:number "AC5433" :total_height 48 :total_width 30 :shelf 2 :price 1501 :weight 110}
                    {:number "AC5432" :total_height 60 :total_width 30 :shelf 2 :price 1623 :weight 145}
                    {:number "AC5431" :total_height 72 :total_width 30 :shelf 3 :price 1775 :weight 165}
                    {:number "AC5430" :total_height 84 :total_width 30 :shelf 4 :price 1945 :weight 185}
                    {:number "AC5446" :total_height 30 :total_width 36 :shelf 1 :price 1155 :weight 85}
                    {:number "AC5445" :total_height 36 :total_width 36 :shelf 2 :price 1282 :weight 90}
                    {:number "AC5444" :total_height 42 :total_width 36 :shelf 2 :price 1390 :weight 100}
                    {:number "AC5443" :total_height 48 :total_width 36 :shelf 2 :price 1558 :weight 120}
                    {:number "AC5442" :total_height 60 :total_width 36 :shelf 2 :price 1710 :weight 165}
                    {:number "AC5441" :total_height 72 :total_width 36 :shelf 3 :price 1833 :weight 185}
                    {:number "AC5440" :total_height 84 :total_width 36 :shelf 4 :price 2025 :weight 200}]
             :option[{:name "010"} {:name "061"} {:name "060"} {:name "A49"}]}

            {:line "Activa"
             :category "Bookcase"
             :name "Open Bookcase"
             :description "12\" deep"
             :item [{:number "AC5419" :total_height 30 :total_width 30 :shelf 1 :price 762 :weight 55}
                    {:number "AC5418" :total_height 36 :total_width 30 :shelf 2 :price 835 :weight 70}
                    {:number "AC5417" :total_height 42 :total_width 30 :shelf 2 :price 946 :weight 85}
                    {:number "AC5416" :total_height 48 :total_width 30 :shelf 3 :price 1085 :weight 90}
                    {:number "AC5415" :total_height 60 :total_width 30 :shelf 3 :price 1267 :weight 115}
                    {:number "AC5414" :total_height 72 :total_width 30 :shelf 4 :price 1441 :weight 135}
                    {:number "AC5413" :total_height 84 :total_width 30 :shelf 5 :price 1675 :weight 155}
                    {:number "AC5426" :total_height 30 :total_width 36 :shelf 1 :price 835 :weight 65}
                    {:number "AC5425" :total_height 36 :total_width 36 :shelf 2 :price 946 :weight 80}
                    {:number "AC5424" :total_height 42 :total_width 36 :shelf 2 :price 1033 :weight 95}
                    {:number "AC5423" :total_height 48 :total_width 36 :shelf 3 :price 1267 :weight 100}
                    {:number "AC5422" :total_height 60 :total_width 36 :shelf 3 :price 1351 :weight 125}
                    {:number "AC5421" :total_height 72 :total_width 36 :shelf 4 :price 1566 :weight 145}
                    {:number "AC5420" :total_height 84 :total_width 36 :shelf 5 :price 1860 :weight 165}]
             :option[{:name "010"} {:name "061"} {:name "060"} {:name "A49"}]}

            {:line "Activa"
             :category "Bookcase"
             :name "Open Horizontal Bookcase"
             :description "12\" deep"
             :item [{:number "AC5150" :total_height 30 :total_width 42 :shelf 1 :price 1173 :weight 75}
                    {:number "AC5151" :total_height 48 :total_width 42 :shelf 3 :price 1654 :weight 90}
                    {:number "AC5152" :total_height 30 :total_width 48 :shelf 1 :price 1323 :weight 85}
                    {:number "AC5153" :total_height 48 :total_width 48 :shelf 3 :price 1796 :weight 100}
                    {:number "AC5154" :total_height 30 :total_width 60 :shelf 1 :price 1547 :weight 100}
                    {:number "AC5155" :total_height 48 :total_width 60 :shelf 3 :price 2225 :weight 110}
                    {:number "AC5156" :total_height 30 :total_width 72 :shelf 1 :price 1592 :weight 115}
                    {:number "AC5157" :total_height 48 :total_width 72 :shelf 3 :price 2278 :weight 125}]
             :option[{:name "010"} {:name "061"} {:name "060"} {:name "A49"}]}

            {:line "Proton"
             :category "Desk"
             :name "Double Pedestal Desk Straight Front"
             :description "Box/box/file pedestals. Top overhang on front only; flush sides."
             :item [{:number "AP4000" :total_width 60 :total_depth 30 :kneespace 30 :price 2636 :weight 280}
                    {:number "AP4002" :total_width 66 :total_depth 30 :kneespace 36 :price 2679 :weight 300}
                    {:number "AP4001" :total_width 72 :total_depth 36 :kneespace 42 :price 2878 :weight 320}
                    {:number "AP4003" :total_width 84 :total_depth 36 :kneespace 42 :price 3179 :weight 350}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "030"} {:name "033a"}]}

            {:line "Proton"
             :category "Desk"
             :name "Double Pedestal Desk Shadowbox Front"
             :description "Box/box/file pedestals. Top overhang on front only; flush sides."
             :item [{:number "AP4005" :total_width 60 :total_depth 30 :kneespace 30 :price 2769 :weight 280}
                    {:number "AP4006" :total_width 66 :total_depth 30 :kneespace 36 :price 2824 :weight 300}
                    {:number "AP4007" :total_width 72 :total_depth 36 :kneespace 42 :price 3015 :weight 320}
                    {:number "AP4008" :total_width 84 :total_depth 36 :kneespace 42 :price 3323 :weight 350}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "030"} {:name "033a"}]}

            {:line "Proton"
             :category "Desk"
             :name "Double Pedestal Desk Bowfront Top Straight Front"
             :description "Box/box/file pedestals. Top overhang on front only; flush sides."
             :item [{:number "AP4010" :total_width 66 :total_depth 36 :bow 30 :kneespace 36 :price 2961 :weight 300}
                    {:number "AP4011" :total_width 72 :total_depth 42 :bow 36 :kneespace 42 :price 3166 :weight 320}
                    {:number "AP4012" :total_width 84 :total_depth 42 :bow 36 :kneespace 42 :price 3467 :weight 350}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "030"} {:name "033a"}]}

            {:line "Proton"
             :category "Desk"
             :name "Double Pedestal Desk Bowfront Top Shadowbox Front"
             :description "Box/box/file pedestals. Top overhang on front only; flush sides."
             :item [{:number "AP4013" :total_width 66 :total_depth 36 :bow 30 :kneespace 36 :price 3046 :weight 300}
                    {:number "AP4014" :total_width 72 :total_depth 42 :bow 36 :kneespace 42 :price 3250 :weight 320}
                    {:number "AP4015" :total_width 84 :total_depth 42 :bow 36 :kneespace 42 :price 3588 :weight 350}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "030"} {:name "033a"}]}

            {:line "Proton"
             :category "Desk"
             :name "Single Pedestal Desk Straight Front"
             :description "Box/box/file pedestal. Top overhang on front only; flush sides. 48\" & 54\" not for returns."
             :item [{:number "AP4060" :total_width 48 :total_depth 24 :kneespace 31 :price 1914 :weight 160}
                    {:number "AP4061" :total_width 54 :total_depth 30 :kneespace 37 :price 1956 :weight 180}
                    {:number "AP4062" :total_width 60 :total_depth 30 :kneespace 26 :price 2107 :weight 190}
                    {:number "AP4063" :total_width 66 :total_depth 30 :kneespace 32 :price 2276 :weight 200}
                    {:number "AP4064" :total_width 66 :total_depth 36 :kneespace 32 :price 2347 :weight 210}
                    {:number "AP4065" :total_width 72 :total_depth 36 :kneespace 38 :price 2444 :weight 220}
                    {:number "AP4066" :total_width 78 :total_depth 36 :kneespace 44 :price 2745 :weight 240}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "030"} {:name "033a"}]}

            {:line "Proton"
             :category "Desk"
             :name "Single Pedestal Desk Shadowbox Front"
             :description "Box/box/file pedestal. Top overhang on front only; flush sides."
             :item [{:number "AP4050" :total_width 60 :total_depth 30 :kneespace 26 :price 2263 :weight 190}
                    {:number "AP4051" :total_width 66 :total_depth 30 :kneespace 32 :price 2408 :weight 200}
                    {:number "AP4052" :total_width 66 :total_depth 36 :kneespace 32 :price 2474 :weight 210}
                    {:number "AP4053" :total_width 72 :total_depth 36 :kneespace 38 :price 2582 :weight 220}
                    {:number "AP4054" :total_width 78 :total_depth 36 :kneespace 44 :price 2878 :weight 240}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "030"} {:name "033a"}]}

            {:line "Proton"
             :category "Desk"
             :name "Single Pedestal Desk Bowfront Top Straight Front"
             :description "Box/box/file pedestal. Pull-out dictation shelf. Top overhang on front only; flush sides."
             :item [{:number "AP4025" :total_width 66 :total_depth 36 :bow 30 :kneespace 32 :price 2577 :weight 210}
                    {:number "AP4026" :total_width 66 :total_depth 42 :bow 36 :kneespace 32 :price 2648 :weight 220}
                    {:number "AP4027" :total_width 72 :total_depth 42 :bow 36 :kneespace 38 :price 2745 :weight 230}
                    {:number "AP4028" :total_width 78 :total_depth 42 :bow 36 :kneespace 44 :price 3046 :weight 250}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "030"} {:name "033a"}]}

            {:line "Proton"
             :category "Desk"
             :name "Single Pedestal Desk Bowfront Top Shadowbox Front"
             :description "Box/box/file pedestal. Pull-out dictation shelf. Top overhang on front only; flush sides"
             :item [{:number "AP4020" :total_width 66 :total_depth 36 :bow 30 :kneespace 32 :price 2636 :weight 210}
                    {:number "AP4021" :total_width 66 :total_depth 42 :bow 36 :kneespace 32 :price 2703 :weight 220}
                    {:number "AP4022" :total_width 72 :total_depth 42 :bow 36 :kneespace 38 :price 2977 :weight 230}
                    {:number "AP4023" :total_width 78 :total_depth 42 :bow 36 :kneespace 44 :price 3106 :weight 250}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "030"} {:name "033a"}]}

            {:line "Proton"
             :category "Desk"
             :name "Single Pedestal Cube Desk Shadowbox Front"
             :description "Box/box/file pedestal. No overhang; flush sides"
             :item [{:number "AP4030" :total_width 60 :total_depth 30 :kneespace 26 :price 2474 :weight 190}
                    {:number "AP4031" :total_width 66 :total_depth 30 :kneespace 32 :price 2618 :weight 200}
                    {:number "AP4032" :total_width 66 :total_depth 36 :kneespace 32 :price 2685 :weight 210}
                    {:number "AP4033" :total_width 72 :total_depth 36 :kneespace 38 :price 2793 :weight 220}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "030"} {:name "033a"}]}

            {:line "Proton"
             :category "Desk"
             :name "Double Ped. Cube Desk Shadowbox Front"
             :description "Box/box/file pedestals. No overhang; flush sides."
             :item [{:number "AP4035" :total_width 60 :total_depth 30 :kneespace 30 :price 2980 :weight 280}
                    {:number "AP4036" :total_width 66 :total_depth 30 :kneespace 36 :price 3034 :weight 300}
                    {:number "AP4037" :total_width 66 :total_depth 36 :kneespace 36 :price 3149 :weight 320}
                    {:number "AP4038" :total_width 72 :total_depth 36 :kneespace 42 :price 3227 :weight 350}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "030"} {:name "033a"}]}

            {:line "Proton"
             :category "Desk"
             :name "Bullet End Table Desk"
             :description "6\" dia. black cylinder. For use with any return shown on page 325 Specify bullet end left or right."
             :item [{:number "AP4070" :total_width 66 :total_depth 36 :kneespace 26 :price 2194 :weight 145}
                    {:number "AP4071" :total_width 72 :total_depth 36 :kneespace 32 :price 2217 :weight 155}
                    {:number "AP4072" :total_width 84 :total_depth 36 :kneespace 44 :price 2464 :weight 175}]
             :option[{:name "013"} {:name "020"} {:name "030"} {:name "033a"} {:name "A23"}]}

            {:line "Proton"
             :category "Desk"
             :name "Expanded Bullet End Table Desk"
             :description "Self supported. 6\" dia. black cylinder. For use with any return shown on page 325 Specify bullet end left or right"
             :item [{:number "AP4073" :total_width 66 :total_depth 36 :kneespace 26 :price 2374 :weight 150}
                    {:number "AP4074" :total_width 72 :total_depth 36 :kneespace 32 :price 2398 :weight 160}
                    {:number "AP4075" :total_width 84 :total_depth 36 :kneespace 44 :price 2645 :weight 180}]
             :option[{:name "013"} {:name "020"} {:name "030"} {:name "033a"} {:name "A23"}]}

            {:line "Proton"
             :category "Bridge"
             :name "Credenza/Bridge Top"
             :description "Tops have 1.25\" hardwood edge with a configurated edge on one long side"
             :item [{:number "AP4080" :total_width 42 :total_depth 19 :price 482 :weight 18}
                    {:number "AP4081" :total_width 48 :total_depth 19 :price 541 :weight 28}
                    {:number "AP4082" :total_width 60 :total_depth 19 :price 650 :weight 38}
                    {:number "AP4085" :total_width 42 :total_depth 25 :price 541 :weight 21}
                    {:number "AP4086" :total_width 48 :total_depth 25 :price 602 :weight 32}
                    {:number "AP4087" :total_width 60 :total_depth 25 :price 722 :weight 44}]
             :option[{:name "030"} {:name "031"} {:name "032"} {:name "033a"}]}

            {:line "Proton"
             :category "Return"
             :name "Bridge Return"
             :description "For connecting a single pedestal desk with a credenza to create a U workstation."
             :item [{:number "AP4090" :total_width 42 :total_depth 19 :price 915 :weight 65}
                    {:number "AP4091" :total_width 48 :total_depth 19 :price 964 :weight 75}
                    {:number "AP4092" :total_width 54 :total_depth 19 :price 1011 :weight 85}
                    {:number "AP4093" :total_width 60 :total_depth 19 :price 1072 :weight 90}
                    {:number "AP4095" :total_width 42 :total_depth 25 :price 976 :weight 70}
                    {:number "AP4096" :total_width 48 :total_depth 25 :price 1032 :weight 85}
                    {:number "AP4097" :total_width 54 :total_depth 25 :price 1072 :weight 90}
                    {:number "AP4098" :total_width 60 :total_depth 25 :price 1131 :weight 95}]
             :option[{:name "030"} {:name "031"} {:name "032"} {:name "033a"}]}

            {:line "Proton"
             :category "Return"
             :name "Return with box/box/file pedestal"
             :description "No modesty panel. Specify left or right return,and executive or secretarial height."
             :item [{:number "AP4112" :total_width 42 :total_depth 19 :kneespace 26 :price 1385 :weight 100}
                    {:number "AP4110" :total_width 48 :total_depth 19 :kneespace 32 :price 1505 :weight 105}
                    {:number "AP4111" :total_width 60 :total_depth 19 :kneespace 44 :price 1613 :weight 115}
                    {:number "AP4115" :total_width 42 :total_depth 25 :kneespace 26 :price 1679 :weight 110}
                    {:number "AP4113" :total_width 48 :total_depth 25 :kneespace 32 :price 1800 :weight 118}
                    {:number "AP4114" :total_width 60 :total_depth 25 :kneespace 44 :price 1914 :weight 130}]
             :option[{:name "012"} {:name "030"} {:name "031"} {:name "032"} {:name "033a"} {:name "A44"}]}

            {:line "Proton"
             :category "Return"
             :name "Return with 36\" lateral file"
             :description "No modesty panel. Specify left or right return,and executive or secretarial height. For 30\" wide lateral file deduct $158. list."
             :item [{:number "AP4116" :total_width 48 :total_depth 19 :kneespace 11 :price 1985 :weight 200}
                    {:number "AP4117" :total_width 60 :total_depth 19 :kneespace 21 :price 2107 :weight 210}
                    {:number "AP4118" :total_width 48 :total_depth 25 :kneespace 11 :price 2058 :weight 205}
                    {:number "AP4119" :total_width 60 :total_depth 25 :kneespace 21 :price 2185 :weight 215}]
             :option[{:name "012"} {:name "033a"} {:name "A44"}]}

            {:line "Proton"
             :category "Table"
             :name "Computer Corner Table"
             :description "with keyboard drawer and grommet. 4120 has 30\" wide angled work edge and 19\" ends. 4121 has 36\" wide angled work edge and 25\" ends."
             :item [{:number "AP4120" :total_width 40 :total_depth 40 :price 1806 :weight 190}
                    {:number "AP4121" :total_width 48 :total_depth 48 :price 2402 :weight 215}]}

            {:line "Proton"
             :category "Credenza"
             :name "Cabinet Credenza 29\" high"
             :description "1 adjustable shelf behind each pair of doors. 3 door units - Single door is on left unless otherwise specified. 105\" wide unit shipped KD"
             :item [{:number "AP4210" :total_width 35 :total_depth 19 :door 2 :price 1385 :weight 135}
                    {:number "AP4211" :total_width 42 :total_depth 19 :door 2 :price 1566 :weight 150}
                    {:number "AP4212" :total_width 53 :total_depth 19 :door 3 :price 2168 :weight 190}
                    {:number "AP4213" :total_width 70.5 :total_depth 19 :door 4 :price 2408 :weight 240}
                    {:number "AP4214" :total_width 105 :total_depth 19 :door 6 :price 4334 :weight 400}
                    {:number "AP4215" :total_width 35 :total_depth 25 :door 2 :price 1553 :weight 160}
                    {:number "AP4216" :total_width 42 :total_depth 25 :door 2 :price 1867 :weight 180}
                    {:number "AP4217" :total_width 53 :total_depth 25 :door 3 :price 2528 :weight 220}
                    {:number "AP4218" :total_width 70.5 :total_depth 25 :door 4 :price 2709 :weight 280}
                    {:number "AP4219" :total_width 105 :total_depth 25 :door 6 :price 4936 :weight 425}]
             :option[{:name "010"}]}

            {:line "Proton"
             :category "Credenza"
             :name "Combination Credenza"
             :description "2 doors, 2 file and 4 box drawers"
             :item [{:number "AP4205" :total_width 60 :total_depth 19 :door 2 :price 2769 :weight 275}
                    {:number "AP4200" :total_width 70.5 :total_depth 19 :door 2 :price 2949 :weight 300}
                    {:number "AP4206" :total_width 60 :total_depth 25 :door 2 :price 3070 :weight 300}
                    {:number "AP4201" :total_width 70.5 :total_depth 25 :door 2 :price 3250 :weight 325}]
             :option[{:name "010"} {:name "012"}]}

            {:line "Proton"
             :category "Credenza"
             :name "Writing Credenza"
             :description "4 box and 2 file drawers"
             :item [{:number "AP4220" :total_width 60 :total_depth 19 :price 2288 :weight 185}
                    {:number "AP4221" :total_width 70.5 :total_depth 19 :price 2589 :weight 195}
                    {:number "AP4223" :total_width 60 :total_depth 25 :price 2450 :weight 200}
                    {:number "AP4224" :total_width 70.5 :total_depth 25 :price 2643 :weight 225}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "030"} {:name "033a"} {:name "A44"}]}

            {:line "Proton"
             :category "File"
             :name "Lateral File"
             :description "4 drawers. Filing is side to side, adjustable letter and legal"
             :item [{:number "AP4226" :total_width 61 :total_depth 19 :price 2745 :weight 340}
                    {:number "AP4227" :total_width 73 :total_depth 19 :price 2980 :weight 365}
                    {:number "AP4228" :total_width 61 :total_depth 25 :price 3070 :weight 375}
                    {:number "AP4229" :total_width 73 :total_depth 25 :price 3124 :weight 400}]
             :option[{:name "012"}]}

            {:line "Proton"
             :category "File"
             :name "Lateral File"
             :description "2 drawers. Filing is side to side, adjustable letter and legal. Shipped with anti-tilt safety system."
             :item [{:number "AP4230" :total_width 30.5 :total_depth 19 :price 1559 :weight 170}
                    {:number "AP4231" :total_width 36.5 :total_depth 19 :price 1679 :weight 185}
                    {:number "AP4232" :total_width 30.5 :total_depth 25 :price 1745 :weight 205}
                    {:number "AP4233" :total_width 36.5 :total_depth 25 :price 1854 :weight 225}]
             :option[{:name "012"}]}

            {:line "Proton"
             :category "Return"
             :name "Rear Unit"
             :description "One box, box, file pedestal. Specify pedestal on left or right. If part of a U unit request “connector” plate."
             :item [{:number "AP4130" :total_width 60 :total_depth 19 :kneespace 42 :price 1926 :weight 165}
                    {:number "AP4131" :total_width 66 :total_depth 19 :kneespace 48 :price 1985 :weight 175}
                    {:number "AP4132" :total_width 72 :total_depth 19 :kneespace 54 :price 2129 :weight 185}
                    {:number "AP4134" :total_width 60 :total_depth 25 :kneespace 42 :price 2041 :weight 190}
                    {:number "AP4135" :total_width 66 :total_depth 25 :kneespace 48 :price 2089 :weight 200}
                    {:number "AP4136" :total_width 72 :total_depth 25 :kneespace 54 :price 2241 :weight 210}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "030"} {:name "031"} {:name "032"} {:name "033a"}]}

            {:line "Proton"
             :category "Return"
             :name "Rear Unit"
             :description "One box, box, file pedestal. and one file, file pedestal. Specify pedestal on left or right. If part of a U unit request “connector” plate"
             :item [{:number "AP4140" :total_width 60 :total_depth 19 :kneespace 26 :price 2528 :weight 225}
                    {:number "AP4141" :total_width 66 :total_depth 19 :kneespace 32 :price 2643 :weight 235}
                    {:number "AP4142" :total_width 72 :total_depth 19 :kneespace 38 :price 2709 :weight 245}
                    {:number "AP4144" :total_width 60 :total_depth 25 :kneespace 26 :price 2782 :weight 235}
                    {:number "AP4145" :total_width 66 :total_depth 25 :kneespace 32 :price 2883 :weight 245}
                    {:number "AP4146" :total_width 72 :total_depth 25 :kneespace 38 :price 2980 :weight 255}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "030"} {:name "031"} {:name "032"} {:name "033a"}]}

            {:line "Proton"
             :category "Return"
             :name "Rear Unit"
             :description "One 36\" lateral file pedestal. For 30\" lateral files deduct $150. Specify pedestal on left or right. If part of a U unit request “connector” plate"
             :item [{:number "AP4150" :total_width 60 :total_depth 19 :kneespace 22 :price 2523 :weight 235}
                    {:number "AP4151" :total_width 66 :total_depth 19 :kneespace 28 :price 2636 :weight 245}
                    {:number "AP4152" :total_width 72 :total_depth 19 :kneespace 34 :price 2703 :weight 255}
                    {:number "AP4154" :total_width 60 :total_depth 25 :kneespace 22 :price 2775 :weight 265}
                    {:number "AP4155" :total_width 66 :total_depth 25 :kneespace 28 :price 2878 :weight 275}
                    {:number "AP4156" :total_width 72 :total_depth 25 :kneespace 34 :price 2993 :weight 285}]
             :option[{:name "012"} {:name "013"} {:name "020"} {:name "030"} {:name "031"} {:name "032"} {:name "033a"}]}

            {:line "Proton"
             :category "Cabinet"
             :name "Wall Hung Cabinet with hinged doors"
             :description "Inside ht.13.5\" Inside depth 12\" Outside ht.18\" For flipper doors add $75. list per door"
             :item [{:number "AP4300" :total_width 30 :total_depth 15 :door 2 :price 1071 :weight 50}
                    {:number "AP4301" :total_width 35 :total_depth 15 :door 2 :price 1181 :weight 60}
                    {:number "AP4302" :total_width 53 :total_depth 15 :door 3 :price 1580 :weight 70}
                    {:number "AP4303" :total_width 60 :total_depth 15 :door 4 :price 1803 :weight 80}
                    {:number "AP4304" :total_width 70.5 :total_depth 15 :door 4 :price 1867 :weight 95}]
             :option[{:name "010"}]}

            {:line "Proton"
             :category "Credenza"
             :name "Hinged Door Hutch for credenzas"
             :description "OA ht. 42\". Inside cabinet ht. 13.5\" Inside cabinet depth 12\". Optional: 54\" OA ht. + 15% When ordering Hutches please indicate model number of credenza. For flipper doors add $75. list per door"
             :item [{:number "AP4310" :total_width 30 :total_depth 15 :door 2 :price 1319 :weight 70}
                    {:number "AP4311" :total_width 35 :total_depth 15 :door 2 :price 1360 :weight 95}
                    {:number "AP4312" :total_width 53 :total_depth 15 :door 3 :price 2168 :weight 150}
                    {:number "AP4313" :total_width 60 :total_depth 15 :door 4 :price 2408 :weight 160}
                    {:number "AP4314" :total_width 70.5 :total_depth 15 :door 4 :price 2643 :weight 175}]
             :option[{:name "010"} {:name "070"} {:name "072"}]}

            {:line "Proton"
             :category "Credenza"
             :name "Binder/Bookcase Hutch for credenzas"
             :description "OA ht. 54\" Optional: 42\" OA ht. Deduct 10% When ordering Hutches please indicate model number of credenza"
             :item [{:number "AP4320" :total_width 30 :total_depth 15 :price 1186 :weight 70}
                    {:number "AP4321" :total_width 35 :total_depth 15 :price 1251 :weight 95}
                    {:number "AP4322" :total_width 53 :total_depth 15 :price 2095 :weight 150}
                    {:number "AP4323" :total_width 60 :total_depth 15 :price 2760 :weight 160}
                    {:number "AP4324" :total_width 70.5 :total_depth 15 :price 2884 :weight 175}]}

            {:line "Proton"
             :category "Cabinet"
             :name "CPU Cabinet"
             :description "29.5\" high with keyboard drawer, and grommet."
             :item [{:number "AP4345" :total_width 36 :total_depth 25 :price 1906 :weight 120}
                    {:number "AP4346" :total_width 42 :total_depth 25 :price 1993 :weight 125}
                    {:number "AP4347" :total_width 48 :total_depth 25 :price 2080 :weight 130}]
             :option[{:name "100"}]}

            {:line "Proton"
             :category "Cabinet"
             :name "Storage/Wardrobe"
             :description "Two doors. 1/3 storage w/shelves. 2/3 wardrobe w/shelf & coat carrier 19\" deep units  shipped with coat hook instead of clothes rod."
             :item [{:number "AP4240" :total_height 72 :total_width 30.5 :total_depth 19 :shelf 4 :price 2662 :weight 185}
                    {:number "AP4241" :total_height 84 :total_width 30.5 :total_depth 19 :shelf 5 :price 2811 :weight 195}
                    {:number "AP4242" :total_height 72 :total_width 36.5 :total_depth 19 :shelf 4 :price 2818 :weight 215}
                    {:number "AP4243" :total_height 84 :total_width 36.5 :total_depth 19 :shelf 5 :price 3036 :weight 235}
                    {:number "AP4245" :total_height 72 :total_width 30.5 :total_depth 25 :shelf 4 :price 3105 :weight 215}
                    {:number "AP4244" :total_height 84 :total_width 30.5 :total_depth 25 :shelf 5 :price 3233 :weight 225}
                    {:number "AP4247" :total_height 72 :total_width 36.5 :total_depth 25 :shelf 4 :price 3269 :weight 245}
                    {:number "AP4248" :total_height 84 :total_width 36.5 :total_depth 25 :shelf 5 :price 3486 :weight 265}]
             :option[{:name "010"} {:name "A34"}]}

            {:line "Proton"
             :category "File"
             :name "Storage w/Lateral Files"
             :item [{:number "AP4250" :total_height 60 :total_width 30.5 :total_depth 19 :shelf 2 :price 3151 :weight 195}
                    {:number "AP4251" :total_height 72 :total_width 30.5 :total_depth 19 :shelf 3 :price 3384 :weight 205}
                    {:number "AP4252" :total_height 84 :total_width 30.5 :total_depth 19 :shelf 4 :price 3768 :weight 240}
                    {:number "AP4255" :total_height 60 :total_width 36.5 :total_depth 19 :shelf 2 :price 3263 :weight 210}
                    {:number "AP4256" :total_height 72 :total_width 36.5 :total_depth 19 :shelf 3 :price 3520 :weight 230}
                    {:number "AP4257" :total_height 84 :total_width 36.5 :total_depth 19 :shelf 4 :price 3907 :weight 255}
                    {:number "AP4260" :total_height 60 :total_width 30.5 :total_depth 25 :shelf 2 :price 3528 :weight 225}
                    {:number "AP4261" :total_height 72 :total_width 30.5 :total_depth 25 :shelf 3 :price 3789 :weight 235}
                    {:number "AP4262" :total_height 84 :total_width 30.5 :total_depth 25 :shelf 4 :price 4214 :weight 270}
                    {:number "AP4265" :total_height 60 :total_width 36.5 :total_depth 25 :shelf 2 :price 3612 :weight 240}
                    {:number "AP4266" :total_height 72 :total_width 36.5 :total_depth 25 :shelf 3 :price 3944 :weight 270}
                    {:number "AP4267" :total_height 84 :total_width 36.5 :total_depth 25 :shelf 4 :price 4376 :weight 285}]
             :option[{:name "010"} {:name "012"}]}

            {:line "Proton"
             :category "Bookcase"
             :name "Open Bookcase"
             :item [{:number "AP4270" :total_height 30 :total_width 30.5 :shelf 1 :price 755 :weight 50}
                    {:number "AP4271" :total_height 36 :total_width 30.5 :shelf 2 :price 843 :weight 65}
                    {:number "AP4272" :total_height 42 :total_width 30.5 :shelf 2 :price 1024 :weight 80}
                    {:number "AP4273" :total_height 48 :total_width 30.5 :shelf 3 :price 1132 :weight 85}
                    {:number "AP4274" :total_height 60 :total_width 30.5 :shelf 3 :price 1265 :weight 100}
                    {:number "AP4275" :total_height 72 :total_width 30.5 :shelf 4 :price 1418 :weight 120}
                    {:number "AP4276" :total_height 84 :total_width 30.5 :shelf 5 :price 1558 :weight 130}
                    {:number "AP4277" :total_height 96 :total_width 30.5 :shelf 6 :price 1654 :weight 150}
                    {:number "AP4280" :total_height 30 :total_width 36.5 :shelf 1 :price 807 :weight 60}
                    {:number "AP4281" :total_height 36 :total_width 36.5 :shelf 2 :price 926 :weight 75}
                    {:number "AP4282" :total_height 42 :total_width 36.5 :shelf 2 :price 1080 :weight 90}
                    {:number "AP4283" :total_height 48 :total_width 36.5 :shelf 3 :price 1169 :weight 95}
                    {:number "AP4284" :total_height 60 :total_width 36.5 :shelf 3 :price 1325 :weight 110}
                    {:number "AP4285" :total_height 72 :total_width 36.5 :shelf 4 :price 1535 :weight 130}
                    {:number "AP4286" :total_height 84 :total_width 36.5 :shelf 5 :price 1704 :weight 140}
                    {:number "AP4287" :total_height 96 :total_width 36.5 :shelf 6 :price 1777 :weight 160}]
             :option[{:name "060"} {:name "061"} {:name "A32"} {:name "A49"}]}])
