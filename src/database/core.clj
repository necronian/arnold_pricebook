(ns database.core
  (:use korma.core
        [korma.db :only (defdb)])
  (:require [database.schema :as schema]))

(defdb db schema/db-spec)

(defentity users)

(defn create-user [user]
  (insert users
          (values user)))

(defn update-user [id first-name last-name email]
  (update users
  (set-fields {:first_name first-name
               :last_name last-name
               :email email})
  (where {:id id})))

(defn get-user [id]
  (first (select users
                 (where {:id id})
                 (limit 1))))

(def ^:private spec-data-columns
  [:leg :door :base :adjustableshelf :drawer :shelf :kneespace
   :leaf :basesection :topsection :seat])

(def ^:private size-data-columns
  [:weight :total_depth :total_width :bow_minimum_depth :shelf_width
   :top_diamater :penninsula_depth :total_height])

(def ^:private price-data-columns
  [:price])

(defentity tables
  (table :information_schema.tables))


(defentity owner)
(defentity line)
(defentity category)
(defentity product)

(defentity option)

(defentity variant)

(defentity style)
(defentity price)

(defentity item)
(defentity size)
(defentity spec)

(defentity product_option)
(defentity variant_option)
(defentity context_templates)

(defentity product_view)
(defentity option_view)

(defentity item_view)
(defentity style_view
  (has-many item_view {:fk :style}))

(defmacro column->id
  "For tables such as owner, category, etc.
  we often just want to get the row id for a specific unique name."
  [table column value]
  `(-> ~table select*
       (where {~column (str ~value)})
       (fields :id)
       (limit 1)
       select first :id))

(defmacro id->name
  "When we have a row id and we want to get the name of the item it represents"
  [table id]
  `(-> ~table select*
       (where {:id ~id})
       (limit 1)
       (fields :name)
       select first :name))

(defmacro insert->id
  "Insert data into the database and return the id of the inserted data"
  [table coll]
  `(-> ~table insert*
       (values ~coll)
       insert :id))

(defn ownerid [s]
  (column->id owner :name s))

(defn lineid [s]
  (column->id line :name s))

(defn categoryid [s]
  (column->id category :name s))

(defn optionid [s]
  (column->id option :number s))

(defn- insert-data
  "Filter the map to remove any keys that are not in the passed vector.
  If no matching keys are found return a map with all
  the columns as keys with null values"
  [columns coll]
  (let [data (->> (map (fn [k] {k (k coll)}) columns)
                  (into {})
                  (filter (comp not nil? val))
                  (into {}))]
    (if (empty? data) (zipmap columns (repeat nil)) data)))

(defn insert-price
  "Insert a single price row into the database"
  [coll]
  (insert->id price (insert-data price-data-columns coll)))

(defn insert-size
  "Insert a single size row into the database"
  [coll]
  (insert->id size (insert-data size-data-columns coll)))

(defn insert-spec
  "Insert a single spec row into the database"
  [coll]
  (insert->id spec (insert-data spec-data-columns coll)))

(defn insert-item
  "Insert a single item into the database."
  [coll]
  (insert->id item
              {:size (insert-size coll)
               :spec (insert-spec coll)
               :style (:style-id coll)
               :part (:part coll)}))

(defmulti insert-style #(if (= 1 (count (:item %))) :single :multiple))

(defmethod insert-style :single
  [coll]
  (let [i (first (:item coll))
        style-id (insert->id style {:number (:number coll)
                                    :variant (:variant-id coll)
                                    :price (insert-price i)})]
    (insert-item (merge {:style-id style-id} i))))

(defmethod insert-style :multiple
  [coll]
  (let [style-id (insert->id style {:number (:number coll)
                                    :variant (:variant-id coll)
                                    :price (insert-price coll)})]
    (doseq [i (:item coll)] (insert-item (merge {:style-id style-id} i)))))

(defn- private-insert-variant
  [coll]
  (let [variant-id (insert->id variant {:name (:name coll)
                                        :description (:description coll)
                                        :product (:product-id coll)})]
    
    (doseq [i (map #(optionid (:name %))
                   (:option coll))]
      (insert variant_option (values {:option i :variant variant-id})))
    
    (doseq [v (:style coll)]
      (insert-style (merge {:variant-id variant-id} v)))))

(defmulti insert-variant #(cond
                           (:item %) :item
                           (:style %) :style
                           (:variant %) :variant))

(defmethod insert-variant :item
  [coll]
  (private-insert-variant {:product-id (:product-id coll)
                           :style (into '[] (map (fn [i] {:number (:number i)
                                                          :item [i]})
                                                 (:item coll)))}))

(defmethod insert-variant :style
  [coll]
  (private-insert-variant {:product-id (:product-id coll)
                           :style (into '[] (:style coll))}))

(defmethod insert-variant :variant
  [coll]
  (doseq [v (:variant coll)]
    (private-insert-variant (merge (dissoc v :item)
                                   {:product-id (:product-id coll)
                                    :style (into '[] (map (fn [i]
                                                            {:number (:number i)
                                                             :item [i]})
                                                          (:item v)))}))))

(defn insert-product
  [coll]
  (let [product-id (insert->id product
                               {:owner (ownerid (:owner coll))
                                :line (lineid (:line coll))
                                :category (categoryid (:category coll))
                                :name (:name coll)
                                :description (:description coll)})
        options (map #(optionid (:name %))
                     (:option coll))]
    
    (doseq [i options]
      (insert product_option (values {:option i :product product-id})))

    (insert-variant (merge {:product-id product-id} coll))))

(defn products
  [oid lid cid]
  (select product (where {:line lid :category cid :owner oid})))

(defn product_styles [pid]
  (select style_view (where {:product_id pid})))

(defn get-products
  [& {:keys [:owner :category :line] lim :limit}]
  (let [itm (into {} (filter second {:owner (ownerid owner)
                                     :category (categoryid category)
                                     :line (lineid line)}))]
    (cond
     (= 1 lim)        (-> product_view select* (where itm) (limit lim) select first)
     (not (nil? lim)) (-> product_view select* (where itm) (limit lim) select)
     :default         (-> product_view select* (where itm) select))))

(defn product->variants
  [id]
  (-> variant select*
      (where {:product id})
      select))

(defn productid->options
  "Given a productid return a list of option rows associated with the product"
  [pid]
  (map #(-> option_view select* 
            (where {:id (:option %)})
            (limit 1) select first)
       (-> product_option select*
           (where {:product pid})
           select)))

(defn name->template 
  [s]
  (-> context_templates select*
      (where {:name s})
      (limit 1)
      select first :template))

(defn productid->variant
  [pid]
  (-> variant select*
      (where {:product pid})
      select))

(defn variantid->options
  "Given a variantid return a list of option rows associated with the variant"
  [vid]
  (map #(-> option_view select* 
            (where {:id (:option %)})
            (limit 1) select first)
       (-> variant_option select*
           (where {:variant vid})
           select)))

(defn get-variant
  [vid]
  (-> variant
      select*
      (where {:id vid})
      (limit 1) select first))

(defn variantid->styles
  [vid]
  (-> style_view select*
      (with item_view)
      (where {:variant vid})
      select))

(defn variantid->items
    "Given a variantid return a list of style rows associated with the product"
    [vid]
    (-> style_view select*
        (where {:variant_id vid})
        select))
