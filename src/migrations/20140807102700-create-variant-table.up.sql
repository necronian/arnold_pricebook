
CREATE TABLE variant
(
  id serial PRIMARY KEY,
  product integer REFERENCES product (id),
  name text,
  description text,
  image text
);
