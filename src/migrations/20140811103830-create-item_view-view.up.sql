
CREATE VIEW item_view AS
SELECT item.id,
       item.part,
       item.style,
       size.total_width,
       size.total_height,
       size.total_depth,
       size.penninsula_depth,
       size.bow_minimum_depth,
       size.shelf_width,
       size.top_diamater,
       size.weight,
       spec.kneespace,
       spec.door,
       spec.drawer,
       spec.shelf,
       spec.adjustableshelf,
       spec.base,
       spec.leg,
       spec.seat,
       spec.topsection,
       spec.basesection,
       spec.leaf
FROM item, size, spec
WHERE item.size = size.id AND
      item.spec = spec.id;
