
INSERT INTO context_templates(name,template)
    VALUES ('header','\enableregime     [utf-8]         % choose input encoding
                                % (in LuaTeX and XeTeX, UTF-8 is on by default, thus not needed)
\mainlanguage   [en]            % language mode: changes typesetting rules, quote signs etc.
\setupbodyfontenvironment[default][em=italic] % ConTeXts default \em is slanted, but italic is better

\setuppapersize[letter]

\setuplayout[backspace=10mm, % Set the Left Margin to 10mm
             topspace=10mm,  % Set the Top Margin to 10mm
             height=middle,  % Make the top and bottom margins equal
             width=middle,   % Make the Left and right margins equsl
             header=10mm,     % We dont need headers
             footer=5mm,     % Or footers
             grid=yes]       % Layout using grids, makes things pretty

\usetypescript[palatino]    % Pretty Fonts
\setupbodyfont[palatino,10pt]

\setuppagenumbering[location={footer,inleft},alternative=doublesided] % Page numbers in lower left and right of opposing pages

%\showframe % Show the Page layout frames for debugging

\setupcolors[state=start]'),
           ('table-header','\setupTABLE[frame=off,option=stretch,split=repeat]

\setupTABLE[header][leftframe=off,rightframe=off,topframe=on,bottomframe=on,background=color,backgroundcolor=white,rulethickness=2pt]

\setupTABLE[row][odd][background=color,backgroundcolor=gray]
\setupTABLE[row][each][align={center,lohi}]
\setupTABLE[c][1][width=50mm,background=color,backgroundcolor=gray]
\setupTABLE[c][2][width=40mm,align={flushleft,lohi},background=color,backgroundcolor=white]
\setupTABLE[c][3,4,5][leftframe=on]
\setupTABLE[c][6][leftframe=on,rightframe=on]
\setupTABLE[c][7][background=color,backgroundcolor=gray,leftframe=on]
\setupTABLE[c][8][background=color,backgroundcolor=gray,align=flushleft]'),
           ('footer','\stoptext');
