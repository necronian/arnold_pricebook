
CREATE VIEW style_view AS
SELECT style.id,
       style.number,
       style.variant,
       ( SELECT price.price
         FROM price
         WHERE style.price = price.id ) AS price,
       ( SELECT variant.product
         FROM variant
         WHERE variant.id = style.variant) AS product_id,
       ( SELECT count(*) AS count
         FROM item
         WHERE item.style = style.id) AS total_items
FROM style;
