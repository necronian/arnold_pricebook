
CREATE TABLE price 
(
   id serial PRIMARY KEY,
   price numeric (10, 2)
);
