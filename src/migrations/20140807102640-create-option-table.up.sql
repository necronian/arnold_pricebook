
CREATE TABLE option
(
  id serial PRIMARY KEY,
  "number" text NOT NULL,
  description text,
  amount numeric(8,2) NOT NULL,
  percentage boolean NOT NULL,
  negative boolean NOT NULL,
  each boolean NOT NULL,
  per text
);
