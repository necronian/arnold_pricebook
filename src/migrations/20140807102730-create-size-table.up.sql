
CREATE TABLE size
(
   id serial PRIMARY KEY,
   total_width numeric (10, 6),
   total_height numeric (10, 6),
   total_depth numeric (10, 6),
   penninsula_depth numeric (10, 6),
   bow_minimum_depth numeric (10, 6),
   shelf_width numeric (10, 6),
   top_diamater numeric (10, 6),
   weight integer
);
