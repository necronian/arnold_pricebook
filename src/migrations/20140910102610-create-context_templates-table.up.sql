
CREATE TABLE context_templates
(
  id serial PRIMARY KEY,
  name text NOT NULL,
  template text NOT NULL
);
