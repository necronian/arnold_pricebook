
CREATE TABLE item
(
  id serial PRIMARY KEY,
  part text,
  style integer REFERENCES style (id),
  size integer NOT NULL REFERENCES size (id),
  spec integer NOT NULL REFERENCES spec (id)
);
