
CREATE TABLE variant_option
(
   variant integer REFERENCES variant (id),
   option integer REFERENCES option (id),
   PRIMARY KEY (variant, option),
   UNIQUE (variant, option)
);
