
CREATE TABLE spec
(
   id serial PRIMARY KEY,
   kneespace numeric (10, 6),
   door integer,
   drawer integer,
   shelf integer,
   adjustableshelf integer,
   base integer,
   leg integer,
   seat integer,
   topsection integer,
   basesection integer,
   leaf integer
);
