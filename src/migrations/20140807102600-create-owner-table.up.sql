
CREATE TABLE owner
(
  id serial PRIMARY KEY,
  name text NOT NULL UNIQUE
);
