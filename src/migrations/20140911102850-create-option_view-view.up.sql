
CREATE MATERIALIZED VIEW option_view AS 
    SELECT id, number AS option_number, CONCAT(description, ' ',
       CASE WHEN per != '' THEN CONCAT(formatted, ' ', TRIM(both ' ' from per))
            WHEN each = true THEN CONCAT(formatted, ' each')
            ELSE formatted
       END) AS option_description
FROM (SELECT *,
       CONCAT((CASE WHEN negative = true THEN '-' 
                    ELSE '+' END),
              '$',
              TRIM(leading ' ' from TO_CHAR(CEIL(amount),'999999')),
              (CASE WHEN percentage = true THEN '%' END)) AS formatted
FROM option) AS subselect
    WITH DATA;
