
CREATE TABLE product_option
(
  product integer REFERENCES product (id),
  option integer REFERENCES option (id),
  PRIMARY KEY (product, option),
  UNIQUE (product, option)
);
