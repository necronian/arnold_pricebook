
CREATE TABLE product
(
  id serial PRIMARY KEY,
  name text,
  description text,
  image text,
  line integer NOT NULL REFERENCES line (id),
  category integer NOT NULL REFERENCES category (id),
  owner integer NOT NULL REFERENCES owner (id)
);
