
CREATE TABLE style
(
  id serial PRIMARY KEY,
  number text UNIQUE NOT NULL,
  price integer REFERENCES price (id),
  variant integer NOT NULL REFERENCES variant (id)
);
