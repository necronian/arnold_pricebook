
CREATE MATERIALIZED VIEW product_view AS 
SELECT product.id,
       product.name,
       product.description,
       product.image,
       product.line,
       product.category,
       product.owner,
       ( SELECT count(*) AS count
         FROM variant
         WHERE variant.product = product.id ) AS total_variants,
       ( SELECT count(*) AS count
         FROM product_option
         WHERE product.id = product_option.product) AS total_options,
       ( SELECT count(*) AS count
         FROM variant_option,
              variant
         WHERE product.id = variant.product AND 
                            variant_option.variant = variant.id ) AS total_variant_options,
    ( SELECT count(*) AS count
      FROM style,
           variant
      WHERE product.id = variant.product AND
                         variant.id = style.variant ) AS total_styles,
    ( SELECT count(*) AS count
      FROM item,
           style,
           variant
      WHERE product.id = variant.product AND
                         variant.id = style.variant AND 
                         item.style = style.id ) AS total_items,
    CASE WHEN ( SELECT count(*) AS count
                FROM variant
                WHERE variant.product = product.id) = 1 THEN ( SELECT id 
                                                               FROM variant 
                                                               WHERE variant.product = product.id )
          ELSE null
     END AS variant_id
FROM product
WITH DATA;
