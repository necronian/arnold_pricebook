(ns arnold-pricebook.utils)

(defn option-description
  "Given an option database row return a description used in the
  pricebook options column."
  [coll]
  (when (seq coll)
    (str (:description coll)
         " "
         (if (= 1 (:negative coll)) "-" "+")
         ;(if (= 1 (:negative coll)) "Subtract (-)" "Add (+)")
         " \\$"
         (:amount coll)
         (when (= 1 (:each coll)) " each")
         (when (:per coll) (str " per " (:per coll))))))

(defn size-description
  "Given an item database row return a formatted size result that can
  be used in the price book size column"
  [seq]
  (let [width (remove-trailing-zeros (:width seq))
        depth (remove-trailing-zeros (:depth seq))
        penninsula (remove-trailing-zeros (:penninsuladepth seq))
        bow (remove-trailing-zeros (:bowminimum seq))
        height (remove-trailing-zeros (:height seq))]
    (str (when width (str width "\""))
         (when depth (str " x " depth))
         (when penninsula (str   "/" penninsula "\""))
         (when bow (str   "/" bow "\""))
         (when (and depth (not penninsula) (not bow)) "\"")
         (when height (str " x " height "\"")))))

(defn has-column
  "If the list of maps has data for the keyword then that column will
  exists in the pricebook"
  [col seq]
  (when (not (empty? (filter (comp not nil?) seq))) true))

(defn pad-map
  "A function to pad a list with empty maps in order to make it the
  same length as another list"
  [seq n]
  (if (> (count seq) n)
    seq
    (concat seq (take (- n (count seq)) (repeat {})))))

(comment
(defn one-variant
  "For products with only one variant return the pricelist rows for the product"
  [seq]
  (let [items (:item (first (:variant seq)))
        options (:option seq)
        number-rows (max (count options) (count items))
        padded-items (pad-map items number-rows)
        padded-options (pad-map options number-rows)
        columns (map (fn [item option]
                       [:TR
                        [:TD (:style item)]
                        [:TD (size-description item)]
                        [:TD (when (:price item) (str "\\$" (:price item)))]
                        [:TD (:weight item)]
                        [:TD (:optionname option)]
                        [:TD (option-description option)]])
                     padded-items padded-options)
        front-matter (into [:TR {:topframe "on"}
                            [:TD {:nr number-rows}
                             (when (:image seq)
                               (str "\\externalfigure[" (md5 (:image seq)) ".svg][width=48mm]"))
                             ]
                            [:TD {:nr number-rows}
                             (ctx/bold (:name seq)) "\\\\"
                             (:description seq)]]
                           (rest (first columns)))]
    [:TABLE [:TABLEhead [:TR [:TH] [:TH] [:TH "Style"] [:TH "Size \\\\ (W x D x H)"]
                         [:TH "List \\\\ Price"] [:TH "Weight"] [:TH {:nc 2} "Options"]]]
     (into `[:TABLEbody [~@ front-matter]] (rest columns))]))

(defn multiple-variants
  "TODO: WRITE THIS!"
  [seq]
  (let [variants (:variant seq)
        options (:option seq)
        total-rows (reduce + (map (comp count :item) variants))
        number-rows (max (count options) total-rows)
        padded-options (pad-map options number-rows)
        heading [:TABLEhead [:TR [:TH] [:TH] [:TH "Style"] [:TH "Size \\\\ (W x D x H)"]
                             [:TH "List \\\\ Price"] [:TH "Weight"] [:TH {:nc 2} "Options"]]]
        columns (mapcat (fn [variant]
                          (let [items (:item variant)]
                            (into [[:TR {:topframe "on"}
                                    [:TD {:topframe "off" :nr (count items)}
                                     (when (:image seq)
                                       (str "\\externalfigure[" (md5 (:image variant))
                                            ".svg][width=48mm]"))]
                                    [:TD {:nr (count items)}
                                     (str (ctx/bold (:name variant))
                                          "\\\\"
                                          (:description variant))]
                                    [:TD (:style (first items))]
                                    [:TD (size-description (first items))]
                                    [:TD (when (:price (first items))
                                           (str "\\$" (:price (first items))))]
                                    [:TD (:weight (first items))]]]
                                  (rest (map (fn [item]
                                               [:TR
                                                [:TD (:style item)]
                                                [:TD (size-description item)]
                                                [:TD (when (:price item)
                                                       (str "\\$" (:price item)))]
                                                [:TD (:weight item)]])
                                             items)))))
                        variants)
        front-matter [:TR {:topframe "on"}
                      [:TD]
                      [:TD {:nc 5} (ctx/bold (:name seq)) "\\\\" (:description seq)]
                      [:TD]]]
    (into [:TABLE] (into [heading] [(into [:TABLEbody front-matter]
                                          (map #(into %1 [[:TD {:topframe "off"}
                                                           (:optionname %2)]
                                                          [:TD {:topframe "off"}
                                                           (option-description %2)]])
                                               columns padded-options))]))))

  (defn product-rows
  "Given a product number return a vector of rows for each associated
  item."
  [n]
  (let [prod (-> whole-product (where {:id n}) (limit 1) select first)]
    (if (<= (count (:variant prod)) 1)
      (one-variant prod)
      (multiple-variants prod))))
  
  (defn build-template
    []
    (compile "resources/out"
             (ctx/context (apply conj
                                 (map product-rows
                                      (map :id (-> (select* product) (fields :id) select)))))))
  
  (defn compile
  "Compile the context file with the supplied data"
  [filename data]
  (let [out-data (str (slurp "resources/templates/header.tex") "\n" 
                      (slurp "resources/templates/table-setup.tex") "\n"
                      data "\n\\stoptext")]
    (spit (str filename ".tex") out-data))
  (let-programs [ct "C:/context/tex/texmf-win64/bin/context.exe"]
                (ct (.getAbsolutePath (clojure.java.io/file (str filename ".tex")))
                    "--autopdf"
                    (str "--result=" (.getAbsolutePath (clojure.java.io/file
                                                        (str filename ".svg")))))))

  (defn save-images-pdf
    []
    (doseq [im (select product (fields :image) (where {:image [not= nil]}))]
      (spit (str "resources/temp-images/" (md5 (:image im)) ".svg") (:image im)))
    (doseq [im (select variant (fields :image) (where {:image [not= nil]}))]
      (spit (str "resources/temp-images/" (md5 (:image im)) ".svg") (:image im)))))
