(ns arnold_pricebook.mapit
  (:require [database.core :as db]))

(defn- single-variant? [coll]  (= (:total_variants coll) 1))
(defn- single-items? [coll]    (= (:total_items coll) (:total_styles coll)))
(defn- variant-options? [coll] (= (:total_options coll) 0))

(defn- rows-dispatch
  [coll]
  (cond
   (and (single-variant? coll)
        (single-items? coll))          :single_variant
   (and (single-variant? coll)
        (not (single-items? coll)))    :single_variant_items
   (and (not (single-variant? coll))
        (single-items? coll)
        (not (variant-options? coll))) :multiple_variant_product_options
   (and (not (single-variant? coll))
        (single-items? coll)
        (variant-options? coll))       :multiple_variant_variant_options
   :default                            :multiple_variant_items))

(defn- style-map
  [vid]
  (let [query (db/variantid->styles vid)
        styles (map #(-> % 
                         (clojure.set/rename-keys {:item_view :items})
                         (dissoc :id)
                         (dissoc :variant)
                         (dissoc :product_id))
                    query)]
    (mapv #(merge (dissoc % :items)
                  {:items (mapv (fn [i] (dissoc i :id))
                                (:items %))}) styles)))

(defn- variant-map
  [vid]
  (let [variant (db/get-variant vid)
        styles (style-map vid)
        options (mapv #(dissoc % :id) (db/variantid->options vid))]
    {:description (:description variant)
     :name (:name variant)
     :image (:image variant)
     :styles styles
     :options options}))

(defn product-map
  [coll]
  (let [options (mapv #(dissoc % :id) (db/productid->options (:id coll)))
        variants (db/productid->variant (:id coll))]
    {:name (:name coll)
     :description (:description coll)
     :image (:image coll)
     :dispatch (rows-dispatch coll)
     :variants (mapv #(variant-map (:id %)) variants)
     :options options}))
