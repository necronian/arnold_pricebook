(ns arnold_pricebook.core
  (:use [korma.core]
        [korma.db])
  (:require [database.core :as db]
            [pandect.core :refer [md5]]
            [arnold_pricebook.mapit :as pm]
            [contup.core :as ctx]))

(defn- key-used?
  "Take a key and a collection if the collection contains non-nil values
  for the key then return the key, else return nil"
  [key coll]
  (when (seq (filter key (map #(select-keys % [key]) coll))) key))

(defn- remove-trailing-zeros
  "If the given bigdec is a whole integer return the integer."
  [n]
  (when n (if (= (bigdec (int n)) n) (int n) n)))

(defn- size-heading
  "Given an item database row return a formatted size result that can
  be used in the price book size column"
  [coll]
  (let [width (key-used? :total_width coll)
        depth (key-used? :total_depth coll)
        height (key-used? :total_height coll)]
    (str (when width (str  "W"))
         (when depth (str " x " "D"))
         (when height (str " x " "H")))))

(defn- size-description
  "Given an item database row return a formatted size result that can
  be used in the price book size column"
  [coll]
  (let [width (remove-trailing-zeros (:total_width coll))
        depth (remove-trailing-zeros (:total_depth coll))
        penninsula (remove-trailing-zeros (:penninsula_depth coll))
        bow (remove-trailing-zeros (:bow_minimum_depth coll))
        height (remove-trailing-zeros (:total_height coll))]
    (str (when width (str width "\""))
         (when depth (str " x " depth))
         (when penninsula (str "/" penninsula "\""))
         (when bow (str   "/" bow "\""))
         (when (and depth (not penninsula) (not bow)) "\"")
         (when height (str " x " height "\"")))))

(def columns [{:header (fn [_] "") :keys [:image]
               :span 1 :rows :onlyfirst :optional false
               :transform (fn [coll] [[:TD {:nr (:total_rows coll)}
                                       (when (:image coll)
                                         [:externalfigure
                                          (str "[" (md5 (str (:image coll)))
                                               ".svg" "][width=48mm]")])]])}
              {:header (fn [_] "") :keys [:name :description]
               :span 1 :rows :onlyfirst :optional false
               :transform (fn [coll] [[:TD {:nr (:total_rows coll)}
                                       (when (and (:name coll))
                                             [:bf (:name coll)])
                                       (when (and (:name coll) 
                                                  (:description coll))
                                         " \n ")
                                       (when (and (:description coll))
                                         (:description coll))]])}
              {:header (fn [_] "Style") :keys [:number]
               :span 1 :rows :all :optional false
               :transform (fn [coll] [[:TD (:number coll)]])}
              {:header (fn [coll] (size-heading coll))
               :keys [:total_width :total_depth :penninsula_depth
                      :bow_minimum_depth :total_height]
               :span 1 :rows :all :optional false
               :transform (fn [coll] [[:TD (size-description coll)]])}
              {:header (fn [_] "KneeSpace") :keys [:kneespace]
               :span 1 :rows :all :optional true
               :transform (fn [coll] [[:TD (remove-trailing-zeros
                                            (:kneespace coll))
                                       (when (:kneespace coll) "\"")]])}
              {:header (fn [_] "Drawers") :keys [:drawer]
               :span 1 :rows :all :optional true
               :transform (fn [coll] [[:TD (:drawer coll)]])}
              {:header (fn [_] "Shelves") :keys [:shelf]
               :span 1 :rows :all :optional true
               :transform (fn [coll] [[:TD (:drawer coll)]])}
              {:header (fn [_] "Price") :keys [:price]
               :span 1 :rows :all :optional false
               :transform (fn [coll] [[:TD (:price coll)]])}
              {:header (fn [_] "Weight") :keys [:weight]
               :span 1 :rows :all :optional false
               :transform (fn [coll] [[:TD (:weight coll)]])}
              {:header (fn [_] "Options")
               :keys [:option_number :option_description]
               :span 2 :rows :all :optional false
               :transform (fn [coll] [[:TD (:option_number coll)]
                                      [:TD (:option_description coll)]])}])

(def product-columns
  (-> columns 
      (update-in [1] (fn [_] {:header (fn [_] "") :keys [:part]
                              :span 1 :rows :all :optional false
                              :transform (fn [coll] [[:TD (:part coll)]])}))
      (update-in [2] update-in [:rows] (fn [_] :first))
      (update-in [6] update-in [:rows] (fn [_] :first))
      (update-in [7] update-in [:rows] (fn [_] :first))))

(defn- pad-map
  "A function to pad a list with empty maps in order to make it the
  same length as another list."
  [n coll]
  (if (> (count coll) n)
    coll
    (concat coll (take (- n (count coll)) (repeat {})))))

(defn- build-row-heading
  "Given a columns definition and a collection of data rows create the context
  table heading."
  [columns items]
  [:TABLEhead 
   (into [:TR]
         (map #(vector :TH {:nc (:span %1)} %2) columns
              (map #((:header %) items) columns)))])

(defn- column-map
  [columns total row current]
  (let [data (merge {:total_rows total :current_row current} row)]
    (map #(cond
           (and (= :first (:rows %)) (= 0 current)) ((:transform %) data)
           (and (= :onlyfirst (:rows %)) (= 0 current)) ((:transform %) data)
           (and (= :onlyfirst (:rows %)) (not (= 0 current))) nil
           (= :all (:rows %)) ((:transform %) data)
           :default [[:TD]])
         columns)))

(defn- build-row
  "Given a row of data and the columns definition output a context table row"
  [columns total row current]
  (into [:TR] (apply concat (column-map columns total row current))))

(defn- build-rowset
  [columns coll]
  (map #(build-row columns (count coll) %1 %2) coll (range (count coll))))

(defn- keys->used?
  [coll items]
  (->> (:keys coll)
       (map #(key-used? % items))
       flatten
       distinct
       (every? nil?)
       not))

(defn- items->columns
  [columns items]
  (->> columns
       (map #(if (:optional %)
               (when (keys->used? % items) %)
               %))
       (filterv (comp not nil?))))

(defn- first-only-keys
  [columns]
  (flatten (map :keys (filter #(= :first (:rows %)) columns))))

(defn- transform-keys
  [columns coll]
  (let [ks (first-only-keys columns)]
    (into [(first coll)] (map #(apply dissoc % ks) (rest coll)))))

(defn- merge-styles
  "For each item in a style merge the item data with the style data."
  [coll]
  (let [variant (dissoc coll :items)
        items (:items coll)]
    (case (count items)
      1 (merge variant (first items))
        (mapv #(merge variant %) items))))

(defn- merge-variants
  "For each variant build the style rows"
  [coll]
  (let [variants (mapv #(mapv merge-styles (:styles %))
                       (:variants coll))]
    (case (count variants)
      1 (first variants)
        variants)))

(defn- partition-list
  [v coll]
  (loop [i [(take (first v) coll)]
         r (drop (first v) coll)
         n (rest v)]
    (if (empty? r)
      i
      (recur (into i [(take (first n) r)])
             (drop (first n) r)
             (rest n)))))

(defn- add-empty-maps
  [n coll]
  (concat coll [(repeat n {})]))

(defn- remove-columns
  "Return only the colums that have keys which appear in the supplied
  collection."
  [columns coll]
  (filter (comp not nil?)
          (map #(when (or (not (:optional %))
                          (keys->used? % coll)) %)
               columns)))

(defn- build-item-row
  "Given a row of data and the columns definition output a context table row"
  [columns row]
  (into [:TR] (mapcat #((:transform %) row) columns)))

(defn- add-heading-seperator
  "Add a bold line to some of the columns in the first item in the collection"
  [coll & {:keys [skip less] :or {skip 2 less 2}}]
  (let [heading-row (first coll)]
    (-> [(-> (take skip heading-row)
              (concat (map #(-> (if (map? (second %))
                                  [:TD (merge (second %) {:topframe "on"
                                                          :rulethickness "2pt"})]
                                  [:TD {:topframe "on"
                                        :rulethickness "2pt"}
                                   (second %)])
                                (into (rest (rest %))))
                           (drop-last less (drop skip heading-row))))
              (concat (take-last less heading-row))
              vec)]
        (into (rest coll)))))

(defn- ->tablebody
  [coll]
  (-> [:TABLEbody]
      (into coll)))

(defn- build-table
  [heading body]
  [:TABLE
   heading
   (->tablebody body)])

(defn- options-dispatch
  [i o]
  (let [ops (distinct (map vector? i))]
    (cond
     (nil? (seq (filter (comp not nil? seq) o))) :nil
     (= 1 (count ops)) (if (first ops) :vectors :maps)
     :default :both)))

(defmulti merge-options options-dispatch)

(defmethod merge-options :vectors
  [i o]
  (let [number-options (count o)
        number-items (map count i)
        rows (max number-options (reduce + number-items))]
    (if (> (reduce + number-items) number-options)
      (map (fn [i o] (map #(merge %1 %2) i o))
           i
           (partition-list number-items (pad-map rows o)))
      (map (fn [i o] (map #(merge %1 %2) i o))
           (add-empty-maps (- rows (reduce + number-items)) i)
           (partition-list (concat number-items
                                   (- rows (reduce + number-items)))
                           (pad-map rows o))))))

(defmethod merge-options :maps
  [i o]
  (let [number-options (count o)
        number-items (count i)
        rows (max number-options number-items)]
    (cond
     (= number-options number-items) (map merge i o)
     (> number-items number-options) (map merge i (pad-map rows o))
     (< number-items number-options) (map merge  (pad-map rows i) o)
     :default nil)))

(defmethod merge-options :nil
  [i o]
  i)

(defn- parent-keys
  [coll]
  (select-keys coll [:image :name :description]))

(defn- transform-collection
  [coll]
  (let [styles (merge-variants coll)
        used-columns (remove-columns columns (flatten styles))
        heading (build-row-heading used-columns (flatten styles))
        ;;item-option-rows (merge-options styles (:options coll))
        item-option-rows (map merge-options 
                              (merge-options styles (:options coll))
                              (map :options (:variants coll)))
        used-product-columns (remove-columns product-columns
                                             (flatten styles))
        variants (map #(merge (parent-keys %2) {:styles %1})
                      item-option-rows
                      (:variants coll))]
    
    {:heading heading
     :item-option-rows item-option-rows
     :variants variants
     :product-heading (build-row used-columns 1 coll 0)
     :product-row-builder (partial build-rowset used-product-columns)
     :row-builder (partial build-rowset used-columns)}))

(defmulti product->table :dispatch)

(defmethod product->table :single_variant
  [coll]
  (let [{:keys [heading item-option-rows row-builder]}
        (transform-collection coll)
        context-rows (map (partial merge (parent-keys coll))
                          item-option-rows)]

    (build-table heading (row-builder context-rows))))

(defmethod product->table :single_variant_items
  [coll]
  (let [{:keys [heading item-option-rows product-heading
                product-row-builder]}
        (transform-collection coll)

        context-rows (map product-row-builder item-option-rows)]

    (->> context-rows
         (mapcat add-heading-seperator)
         (cons product-heading)
         (build-table heading))))

(defmethod product->table :multiple_variant_product_options
  [coll]
  (let [{:keys [product-heading heading variants row-builder]}
        (transform-collection coll)

        context-rows (map #(row-builder (map (fn [i] (merge i %)) (:styles %)))
                          variants)]

    (->> context-rows
         (mapcat add-heading-seperator)
         (cons product-heading)
         (build-table heading))))

(defmethod product->table :multiple_variant_variant_options
  [coll]
  (let [{:keys [product-heading heading variants row-builder]}
        (transform-collection coll)

        context-rows (map #(row-builder (map (fn [i] (merge i %)) (:styles %)))
                          variants)]
    
    (->> context-rows
         (mapcat #(add-heading-seperator % :less 0))
         (cons product-heading)
         (build-table heading))))

(defmethod product->table :multiple_variant_items
  [coll]
  :multiple_variant_items)

(defn test-data []
  (product->table (pm/product-map
                   (first
                    (db/get-products :owner "Arnold Desk"
                                     :category "Solar"
                                     :category "Desk")))))

(defn test-it
  []
  (product->table (pm/product-map (nth (db/get-products :limit 10) 8))))

(defn test-again
  []
  (product->table (pm/product-map (nth (db/get-products :limit 4) 3))))

(defn test-more
  []
  (product->table (pm/product-map (nth (db/get-products :limit 7) 6))))

(defn build-it
  [coll]
  (let [header (db/name->template "header")
        table (db/name->template "table-header")
        footer (db/name->template "footer")]
    (ctx/context->pdf! (into (into [header] coll) [footer]))))
