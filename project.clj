(defproject arnold_pricebook "0.1.0-SNAPSHOT"
  :description "A program to facilitate creating new Arnold Group price lists."
  :url "http://arnolddesks.com"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 ;Database Access
                 [org.clojure/java.jdbc "0.3.3"] ;Specify in order to make
                                                 ;korma and migratus play
                                                 ;nice
                 [korma "0.3.3"]
                 [postgresql/postgresql "9.1-901-1.jdbc4"]
                 [org.xerial/sqlite-jdbc "3.7.15-M1"]
                 [migratus "0.7.0"]
                 [pandect "0.3.4"]
                 [liberator "0.12.0"]
                 [cheshire "5.3.1"]
                 [fresnel "0.2.2"]
                 [contup "0.1.0-SNAPSHOT"]]
  :plugins [[migratus-lein "0.1.0"]
            [codox "0.6.7"]
            [cider/cider-nrepl "0.8.0-SNAPSHOT"]]
  :migratus {:store :database
             :migration-dir "migrations"
             :db {:classname "com.postgresql.jdbc.Driver"
                  :subprotocol "postgresql"
                  :subname "//localhost/arnold_pricebook"
                  :user "postgres"
                  :password "keet2004"}})
